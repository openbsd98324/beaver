# beaver


![](beaver.png)


 
## Build 

Requires gtk, intltool, and pkg-config.



````
 apt-get install  clang gcc   intltool   libgtk2.0-dev  
````


````
sh compile.sh 
````

 

File	Packages
/usr/include/gtk-2.0/gtk/gtk.h	libgtk2.0-dev
/usr/include/gtk-3.0/gtk/gtk.h	libgtk-3-dev


## Screenshots

Linux: 

![](media/beaver.png)


 
