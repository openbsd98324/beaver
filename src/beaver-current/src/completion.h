
/*
** Beaver's an Early AdVanced EditoR
** (C) 1999-2000 Marc Bevand, Damien Terrier and Emmanuel Turquin, 2008 Tobias Heinzen, Double 12
**
** editor.h
**
** Author<s>:   Marc Bevand (aka "After") <bevand_m@epita.fr>
**              Emmanuel Turquin (aka "Ender") <turqui_e@epita.fr>
**              Michael Terry <mterry@fastmail.fm>
**
** Description: Header for auto-completion stuff
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef __COMPLETION_H__
#define __COMPLETION_H__

/* PROTOTYPES */
gint auto_completion (GtkTextView * Editor);

#endif							/* !__COMPLETION_H__ */
