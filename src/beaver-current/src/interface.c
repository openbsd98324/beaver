
/*
** Beaver's an Early AdVanced EditoR
** (C) 1999-2000 Marc Bevand, Damien Terrier and Emmanuel Turquin, 2008 Tobias Heinzen, Double 12
**
** interface.c
**
** Author<s>:     Emmanuel Turquin (aka "Ender") <turqui_e@epita.fr>
**                Michael Terry <mterry@fastmail.fm>
**				  Double 12
**				  Tobias Heinzen
**
** Latest update: Fri Feb 21 02:22:58 2003
** Description:   Beaver interface source
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/** @file interface.c
 * main interface components
 */

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include "../config.h"
#include "struct.h"
#include "plugin.h"
#include "prefs.h"
#include "msgbar.h"
#include "interface.h"
#include "filesops.h"
#include "editor.h"
#include "conf.h"
#include "undoredo.h"
#include "search.h"
#include "completion.h"
#include "api/beaver.h"
#include "languages.h"

#include "main.h"	/* included for UNTITLED macro -> TODO: move to prefs.h */

/* global variables */
GtkWidget *MainWindow = NULL;	/* the main window */
GtkWidget *MainNotebook = NULL;	/* the notebook (where all the editors go into) */
GtkUIManager *MenuManager = NULL;	/* a gtk ui manager, that handles creation and
									 * managment of the beaver UI */
t_settings Settings;			/* global settings of beaver */
GArray *FileProperties = NULL;	/* array of all open files and their properties
								 * (eg. format, read-only etc..) */
gint NewFilesCnt = 0;			/* how many files are new? */
gint OpenedFilesCnt = -1;		/* how many files are open? */

/* static global variables */
static GtkWidget *bar_menu = NULL;	/* the menubar */
static GtkWidget *bar_toolbar = NULL;	/* the toolbar */
static GtkPrintSettings *settings = NULL;	/*settings for print operation */
static GtkWidget *file_info_dialog = NULL;	/* file info dialog */
static GtkWidget *file_info_label = NULL;	/* file info label */
static GHashTable *opened_file_names = NULL;	/* holds information about already opened files */

/* the different text types */
static gchar *text_formats[3] = { "UNIX",
	"MAC",
	"DOS"
};

/* prototypes */
static inline gchar *get_text (GtkTextBuffer * Buffer);
static inline void notebook_add_page (const gchar * filename);
static void close_file (void);
static void save_file_as (void);

/*****************************************************************
 * Menu Handlers
 *****************************************************************/

/**
 * 
 */
static void new_file (void)
{
	print_msg ("New File ...");
	notebook_add_page (NULL);
}

/**
 * 
 */
static void open_file (void)
{
	/* get the current page */
	gint current_page = gtk_notebook_get_current_page (GTK_NOTEBOOK (MainNotebook));

	gchar* dialog_dir;
	
	if (current_page != -1) /* if there is a page opened */
	{
		/* try to use the dir of the file currently opened*/
		gchar* current_dir = g_path_get_dirname( FPROPS (current_page, Name) );

		if (!g_strcmp0(current_dir,"."))  /* if current_dir == "." */
		{
			/* there isn't a valid directory component, use home dir for file selection dialog */
			dialog_dir =  g_strdup(g_get_home_dir());
		}
		else
		{
			/* the directory is ok, use it for the file selection dialog */
			dialog_dir = g_path_get_dirname( FPROPS (current_page, Name) );
		}
		g_free(current_dir); 
	}
	else
	{
		/* use the home dir for the file selection dialog */
		dialog_dir = g_strdup(g_get_home_dir());
	}
	
	/* open a file selection dialog */
	gchar *filename =
		beaver_box_file_selection ("Open File", GTK_FILE_CHOOSER_ACTION_OPEN,
								  dialog_dir);
	g_free (dialog_dir);
	if (filename == NULL)
		return;
		
	/* open up the file */
	print_msg ("Open File ...");
	notebook_add_page (filename);

	/* free up space */
	g_free (filename);
	
}

/**
 * 
 */
static void save_file (void)
{
	if (OpenedFilesCnt >= 0)
	{
		/* get current page */
		gint current_page =
			gtk_notebook_get_current_page (GTK_NOTEBOOK (MainNotebook));

		/* if the file was not modified then it's useless to save */
		if (!gtk_text_buffer_get_modified (FPROPS (current_page, Buffer)))
		{
			return;
		}

		/* try to open file for writing */
		FILE *fp = NULL;

		/* if the file is a new ("Untitled") file, it it's in readonly
		 * mode, or if it is not writeable, then try to call save_file_as */
		if (g_strrstr (FPROPS (current_page, Name), UNTITLED) != NULL ||
			!(fp = fopen (FPROPS (current_page, Name), "w"))
			|| FPROPS (current_page, ReadOnly) != 0)
		{
			save_file_as ();
			return;
		}

		/* get the text and length from the widget and write
		 * it into the file */
		GtkWidget *textview = FPROPS (current_page, Text);
		gchar *text =
			get_text (gtk_text_view_get_buffer (GTK_TEXT_VIEW (textview)));
		gint length = strlen (text);	//gtk_text_buffer_get_char_count (gtk_text_view_get_buffer (GTK_TEXT_VIEW(textview)));

		fwrite (text, length, 1, fp);
		fclose (fp);

		g_free (text);

		/* mark file as unmodified */
		gtk_text_buffer_set_modified (FPROPS (current_page, Buffer), FALSE);
	}
}

/**
 * 
 */
static void save_file_as (void)
{
	if (OpenedFilesCnt >= 0)
	{
		/* get the page before the current page */
		gint last_page = gtk_notebook_get_current_page (GTK_NOTEBOOK (MainNotebook)) -1;
		
		gchar* dialog_dir;
		
		if (last_page != -1) /* if there is a page opened before the current page */
		{
			/* try to use the dir of the file before the current page*/
			gchar* last_dir = g_path_get_dirname( FPROPS (last_page, Name) );

			if (!g_strcmp0(last_dir,"."))  /* if current_dir == "." */
			{
				/* there isn't a valid directory component, use home dir for file selection dialog */
				dialog_dir =  g_strdup(g_get_home_dir());
			}
			else
			{
				/* the directory is ok, use it for the file selection dialog */
				dialog_dir = g_path_get_dirname( FPROPS (last_page, Name) );
			}
			g_free(last_dir); 
		}
		else
		{
			/* use the home dir for the file selection dialog */
			dialog_dir = g_strdup(g_get_home_dir());
		}
		
		/* open a file selection dialog */
		gchar *filename =
			beaver_box_file_selection ("Save File As ...",
									   GTK_FILE_CHOOSER_ACTION_SAVE,
									   dialog_dir);
		g_free(dialog_dir);
									 
		if (filename == NULL)
			return;

		FILE *fp = fopen (filename, "w");

		/* file is probably not writeable */
		if (!fp)
		{
			gchar *estring =
				g_strdup_printf ("File \"%s\" is not writeable!", filename);
			beaver_box_error (estring);
			g_free (estring);
			return;
		}

		/* get the text and length from the widget and write
		 * it into the file */
		gint current_page =
			gtk_notebook_get_current_page (GTK_NOTEBOOK (MainNotebook));
		GtkWidget *textview = FPROPS (current_page, Text);
		gchar *text =
			get_text (gtk_text_view_get_buffer (GTK_TEXT_VIEW (textview)));
		gint length = strlen (text);	//gtk_text_buffer_get_char_count (gtk_text_view_get_buffer (GTK_TEXT_VIEW(textview)));

		fwrite (text, length, 1, fp);
		fclose (fp);

		/* if the file was previously an new file, then we open up
		 * a new tab and close the old one. otherwise just open up 
		 * a new tab */
		notebook_add_page (filename);
		if (g_strrstr (FPROPS (current_page, Name), UNTITLED) != NULL)
		{
			gint save =
				gtk_notebook_get_current_page (GTK_NOTEBOOK (MainNotebook));
			gtk_text_buffer_set_modified (FPROPS (current_page, Buffer), FALSE);
			gtk_notebook_set_current_page (GTK_NOTEBOOK (MainNotebook),
										   current_page);
			close_file ();
			gtk_notebook_set_current_page (GTK_NOTEBOOK (MainNotebook), save);
		}

		/* free up space */
		g_free (filename);
		g_free (text);
	}
}

/**
 * 
 */
static void save_all (void)
{
	/* only save all files if there are files to save open */
	if (OpenedFilesCnt >= 0)
	{
		gint i;
		for (i = 0; i < OpenedFilesCnt + 1; i++)
		{
			/* choose the correct page on the notebook */
			gtk_notebook_set_current_page (GTK_NOTEBOOK (MainNotebook), i);

			/* save that file */
			save_file ();
		}
	}
}

/**
 * 
 */
static void begin_print (GtkPrintOperation * operation,
						 GtkPrintContext * context, gpointer user_data)
{
	/* get the document to print */
	PrintDocument *doc = user_data;

	/* set how many pages there are to print */
	doc->n_pages =
		1 + gtk_text_buffer_get_line_count (doc->textbuffer) / LINES_PER_PAGE;
	gtk_print_operation_set_n_pages (operation, doc->n_pages);
}

/**
 * 
 */
static void draw_page (GtkPrintOperation * operation, GtkPrintContext * context,
					   gint page_nr, gpointer user_data)
{
	cairo_t *cr;
	PangoLayout *layout;
	PrintDocument *doc = user_data;

	/* get some contexes */
	cr = gtk_print_context_get_cairo_context (context);
	layout = gtk_print_context_create_pango_layout (context);

	/* get width and height of the print plane */
	gint width = gtk_print_context_get_width (context);
	gint height = gtk_print_context_get_height (context);

	/* draw a rectangle all around the text */
	cairo_set_line_width (cr, 0.7);
	cairo_new_sub_path (cr);
	cairo_rectangle (cr, 0, 0, width, height);
	cairo_close_path (cr);
	cairo_stroke (cr);

	/* draw a small rectangle on top of the page (header) */
	cairo_new_sub_path (cr);
	cairo_rectangle (cr, 0, 0, width, 17);
	cairo_close_path (cr);
	cairo_set_source_rgb (cr, 0.9, 0.9, 0.9);
	cairo_fill_preserve (cr);
	cairo_set_source_rgba (cr, 0, 0, 0, 1);
	cairo_stroke (cr);

	/* draw the header. the header contains of the filename and the page
	 * number. the filename is truncated to the basename and is centered
	 * in the header. it also get's printed bold and with a different
	 * font face. */
	cairo_text_extents_t extents;
	cairo_select_font_face (cr, "Sans", CAIRO_FONT_SLANT_NORMAL,
							CAIRO_FONT_WEIGHT_BOLD);
	cairo_set_font_size (cr, 12.0);
	cairo_text_extents (cr, doc->name, &extents);

	double x = (width / 2) - (extents.width / 2 + extents.x_bearing);

	cairo_move_to (cr, x, 12.0);
	cairo_show_text (cr, doc->name);
	cairo_stroke (cr);

	/* now draw the page number on the right handside */
	gchar *page = g_strdup_printf ("Page %d/%d", page_nr + 1, doc->n_pages);
	cairo_select_font_face (cr, "Sans", CAIRO_FONT_SLANT_NORMAL,
							CAIRO_FONT_WEIGHT_NORMAL);
	cairo_set_font_size (cr, 12.0);
	cairo_text_extents (cr, doc->name, &extents);

	x = width - extents.width - 3.0;

	cairo_move_to (cr, x, 12.0);
	cairo_show_text (cr, page);
	cairo_stroke (cr);

	g_free (page);

	/* count the number of characters until 75 lines are full. */
	gint chars = 0;
	gint lines = 0;
	gint length = 0;
	gchar *text = doc->content + doc->chars_printed;
	while (*text && lines < LINES_PER_PAGE)
	{
		if (*text == '\n')
		{
			cairo_select_font_face (cr, "Courier New", CAIRO_FONT_SLANT_NORMAL,
									CAIRO_FONT_WEIGHT_NORMAL);
			cairo_set_font_size (cr, 12.0);
			cairo_move_to (cr, 3.0, (lines + 2) * 15.0);
			*text = '\0';
			cairo_show_text (cr, text - length);
			*text = '\n';
			cairo_stroke (cr);

			length = -1;
			lines++;
		}

		length++;
		text++;
		chars++;
	}

	/* increase the number of printed chars */
	doc->chars_printed += chars;

	/* render the page */
	pango_layout_set_alignment (layout, PANGO_ALIGN_LEFT);
	pango_cairo_show_layout (cr, layout);

	/* free up space */
	g_object_unref (layout);

}

/**
 * Prints the selected tab.
 */
static void print_buffer (void)
{
	/* only print if there's something to print */
	if (OpenedFilesCnt >= 0)
	{
		/* get the page to print */
		gint current_page =
			gtk_notebook_get_current_page (GTK_NOTEBOOK (MainNotebook));

		/* if there are no characters in the text, then there's no
		 * sense in printing */
		if (!gtk_text_buffer_get_char_count (FPROPS (current_page, Buffer)))
		{
			print_msg ("Nothing to print ...");
			return;
		}

		/* setup print instructions */
		PrintDocument *doc = g_new0 (PrintDocument, 1);
		GtkWidget *textview = FPROPS (current_page, Text);
		doc->textbuffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (textview));
		doc->content =
			get_text (gtk_text_view_get_buffer (GTK_TEXT_VIEW (textview)));
		doc->name = FPROPS (current_page, BaseName);

		/* create new GTKPrintOperation */
		GtkPrintOperation *print;
		GtkPrintOperationResult res;
		print = gtk_print_operation_new ();

		/* get the printer settings */
		if (settings != NULL)
			gtk_print_operation_set_print_settings (print, settings);

		/* register the handlers */
		g_signal_connect (print, "begin_print", G_CALLBACK (begin_print), doc);
		g_signal_connect (print, "draw_page", G_CALLBACK (draw_page), doc);

		/* show the print dialog */
		gtk_print_operation_set_show_progress (print, TRUE);
		res =
			gtk_print_operation_run (print,
									 GTK_PRINT_OPERATION_ACTION_PRINT_DIALOG,
									 GTK_WINDOW (MainWindow), NULL);

		if (res == GTK_PRINT_OPERATION_RESULT_APPLY)
		{
			if (settings != NULL)
				g_object_unref (settings);

			settings =
				g_object_ref (gtk_print_operation_get_print_settings (print));
		}

		/* free up space */
		g_object_unref (print);
	}
}

/**
 * Display a dialog box with informations about the file.
 */
static void file_info_clicked (void)
{
	/* if no file is opened, then there's no need in
	 * viewing this box
	 */
	if (!(OpenedFilesCnt >= 0))
	{
		return;
	}

	/* if the file info dialog was not initialized beforehand
	 * do it now */
	if (file_info_dialog == NULL)
	{
		/* create new dialog */
		file_info_dialog = gtk_dialog_new ();
		gtk_window_set_modal (GTK_WINDOW (file_info_dialog), TRUE);
		gtk_window_set_transient_for (GTK_WINDOW (file_info_dialog),
									  GTK_WINDOW (gtk_widget_get_toplevel
												  (GTK_WIDGET (MainNotebook))));
		gtk_window_set_policy (GTK_WINDOW (file_info_dialog), FALSE, FALSE,
							   FALSE);
		gtk_window_set_title (GTK_WINDOW (file_info_dialog),
							  APP_NAME " - File Properties");

		/* first of all we need an ok button */
		GtkWidget *button =
			gtk_dialog_add_button (GTK_DIALOG (file_info_dialog), GTK_STOCK_OK,
								   1);

		/* the left label, shows all informations that
		 * we want to provide. */
		GtkWidget *label = gtk_label_new ("Base Name:\n"
										  "Full Name:\n"
										  "File Size:\n" "Language:\n"
										  "Format:\n" "\n" "New File:\n"
										  "Modified:\n" "Readonly:\n");
		file_info_label = gtk_label_new ("");

		/* we have a horizontal box that divides the dialog.
		 * in the left box there's a label with what we want
		 * to provide (as info) and in the right label is the
		 * actual information.
		 */
		GtkWidget *hbox = gtk_hbox_new (FALSE, 2);
		gtk_container_set_border_width (GTK_CONTAINER (hbox), 5);
		gtk_container_add (GTK_CONTAINER (GTK_DIALOG (file_info_dialog)->vbox),
						   hbox);
		gtk_container_add (GTK_CONTAINER (hbox), label);
		gtk_container_add (GTK_CONTAINER (hbox), file_info_label);

		/* some signals */
		g_signal_connect (G_OBJECT (file_info_dialog), "delete-event",
						  GTK_SIGNAL_FUNC (gtk_widget_hide_on_delete),
						  file_info_dialog);
		g_signal_connect (G_OBJECT (file_info_dialog), "close",
						  GTK_SIGNAL_FUNC (gtk_widget_hide), file_info_dialog);
		g_signal_connect (G_OBJECT (file_info_dialog), "response",
						  GTK_SIGNAL_FUNC (gtk_widget_hide), file_info_dialog);
		g_signal_connect (G_OBJECT (button), "clicked",
						  GTK_SIGNAL_FUNC (gtk_widget_hide), file_info_dialog);
	}

	/* get some infos */
	gint current_page =
		gtk_notebook_get_current_page (GTK_NOTEBOOK (MainNotebook));
	gboolean new_file = FALSE;
	if (stat (FPROPS (current_page, Name), &FPROPS (current_page, Stats)) == -1)
		new_file = TRUE;

	/* get the name of the language */
	gchar *language = "-";
	if (FPROPS (current_page, WidgetInfo.Lg) != -1)
	{
		language = Prefs.L[FPROPS (current_page, WidgetInfo.Lg)].Description;
	}

	/* put in the information */
	gchar *label_text =
		g_strdup_printf ("%s\n%s\n%d Bytes\n%s\n%s\n\n%s\n%s\n%s\n",
						 FPROPS (current_page, BaseName), FPROPS (current_page,
																  Name),
						 new_file ? 0 : (gint) FPROPS (current_page,
													   Stats).st_size,
						 language, text_formats[beaver_text_format ()],
						 new_file ? "Yes" : "No",
						 gtk_text_buffer_get_modified (FPROPS
													   (current_page,
														Buffer)) ? "Yes" : "No",
						 FPROPS (current_page, ReadOnly) ? "Yes" : "No");
	gtk_label_set_text (GTK_LABEL (file_info_label), label_text);
	g_free (label_text);

	/* show the dialog */
	gtk_widget_show_all (file_info_dialog);
}

/**
 * 
 */
static void close_file (void)
{
	/* check if you can close anything. if not then
	 * we're done already */
	if (OpenedFilesCnt >= 0)
	{
		/* get the current page */
		gint current_page =
			gtk_notebook_get_current_page (GTK_NOTEBOOK (MainNotebook));

		/* check if tab needs to be saved */
		if (gtk_text_buffer_get_modified (FPROPS (current_page, Buffer)))
		{
			/* ask question and save if necessary */
			gchar *qstring =
				g_strdup_printf ("Save changes in \"%s\"?",
								 FPROPS (current_page, Name));
			gint question = beaver_box_question (qstring);
			if (question == 1)
			{
				save_file ();
			}
			g_free (qstring);

			free_undoredo();
		}

		/* remove page from notebook */
		gtk_notebook_remove_page (GTK_NOTEBOOK (MainNotebook), current_page);

		/* free up space in the file information array */
		g_hash_table_remove (opened_file_names, FPROPS (current_page, Name));
		g_free (FPROPS (current_page, Name));
		g_free (FPROPS (current_page, BaseName));
		g_free (FPROPS (current_page, Type));

		gtk_widget_destroyed (FPROPS (current_page, Text),
							  &FPROPS (current_page, Text));

		/* remove tab from array */
		g_array_remove_index (FileProperties, current_page);

		/* annotate the closing of the tab, print a message
		 * and refresh the interface */
		OpenedFilesCnt--;
		if (OpenedFilesCnt < 0)
			NewFilesCnt = 0;
		print_msg ("File close ...");
		refresh_interface ();
	}
}

/**
 * @param   userdata    data provided at signal connecting time
 */
static void close_file_from_tab (GtkButton * button, gpointer userdata)
{
	/* search for the given filename the tab number */
	gchar *filename = userdata;
	gint i = 0;
	for (; i < OpenedFilesCnt; i++)
	{
		if (g_ascii_strcasecmp (filename, FPROPS (i, Name)) == 0)
		{
			g_free (filename);
			break;
		}
	}

	/* select clicked tab */
	gtk_notebook_set_current_page (GTK_NOTEBOOK (MainNotebook), i);
	close_file ();
}

/**
 * 
 */
static void close_all (void)
{
	/* if files are open, then check if we need to save them? */
	if (OpenedFilesCnt >= 0)
	{
		gint i;
		while ((i = OpenedFilesCnt) >= 0)
		{
			/* choose the correct page on the notebook */
			gtk_notebook_set_current_page (GTK_NOTEBOOK (MainNotebook), i);

			/* close file */
			close_file ();
		}
	}
}

/**
 * 
 */
static void quit (void)
{
	/* close all files */
	close_all ();

	g_object_unref(MenuManager);
	g_array_free(FileProperties, TRUE);

	/* no files open or require saving, so we can
	 * quit the application */
	gtk_main_quit ();
}

/**
 * 
 */
static void undo_clicked (void)
{
	if (OpenedFilesCnt >= 0)
	{
		proceed_undo ();
		refresh_interface();
	}
}

/**
 * 
 */
static void redo_clicked (void)
{
	if (OpenedFilesCnt >= 0)
	{
		proceed_redo ();
		refresh_interface();
	}
}

/**
 * 
 */
static void cut_clicked ()
{
	gint CurrentPage;
	CurrentPage = gtk_notebook_get_current_page (GTK_NOTEBOOK (MainNotebook));

	if ((OpenedFilesCnt >= 0) && !FPROPS (CurrentPage, ReadOnly))
	{
		/* check wheter we're allowed to edit 
		 * this file */
		if (FPROPS (CurrentPage, WidgetInfo.Editable) == FALSE)
		{
			return;
		}
	}
	GtkClipboard *clipboard = gtk_clipboard_get (GDK_SELECTION_CLIPBOARD);
	gtk_text_buffer_cut_clipboard (gtk_text_view_get_buffer
								   (GTK_TEXT_VIEW (FPROPS (CurrentPage, Text))),
								   clipboard, TRUE);
	print_msg ("Selection cut to Clipboard...");
}

/**
 * 
 */
static void copy_clicked ()
{
	gint CurrentPage;
	CurrentPage = gtk_notebook_get_current_page (GTK_NOTEBOOK (MainNotebook));

	if (OpenedFilesCnt >= 0)
	{
		/* check wheter we're allowed to edit 
		 * this file */
		if (FPROPS (CurrentPage, WidgetInfo.Editable) == FALSE)
		{
			return;
		}

		GtkClipboard *clipboard = gtk_clipboard_get (GDK_SELECTION_CLIPBOARD);
		gtk_text_buffer_copy_clipboard (gtk_text_view_get_buffer
										(GTK_TEXT_VIEW
										 (FPROPS (CurrentPage, Text))),
										clipboard);
		print_msg ("Selection copied to Clipboard...");
	}
}

/**
 * 
 */
static void paste_clicked ()
{
	gint CurrentPage;
	CurrentPage = gtk_notebook_get_current_page (GTK_NOTEBOOK (MainNotebook));

	if ((OpenedFilesCnt >= 0) && !FPROPS (CurrentPage, ReadOnly))
	{
		/* check wheter we're allowed to edit 
		 * this file */
		if (FPROPS (CurrentPage, WidgetInfo.Editable) == FALSE)
		{
			return;
		}

		GtkClipboard *clipboard = gtk_clipboard_get (GDK_SELECTION_CLIPBOARD);
		gtk_text_buffer_paste_clipboard (gtk_text_view_get_buffer
										 (GTK_TEXT_VIEW
										  (FPROPS (CurrentPage, Text))),
										 clipboard, NULL, TRUE);
		print_msg ("Text pasted from Clipboard...");
	}
}

/**
 * 
 */
static void select_all_clicked ()
{
	gint CurrentPage;
	CurrentPage = gtk_notebook_get_current_page (GTK_NOTEBOOK (MainNotebook));

	GtkTextIter start, end;
	GtkTextBuffer *Buffer;
	Buffer =
		gtk_text_view_get_buffer (GTK_TEXT_VIEW (FPROPS (CurrentPage, Text)));
	gtk_text_buffer_get_bounds (Buffer, &start, &end);
	gtk_text_buffer_place_cursor (Buffer, &start);
	gtk_text_buffer_move_mark_by_name (Buffer, "selection_bound", &end);
	print_msg ("All Text selected...");
}

/**
 * 
 */
static void complete_clicked (void)
{
	if (OpenedFilesCnt >= 0)
	{
		gint current_page =
			gtk_notebook_get_current_page (GTK_NOTEBOOK (MainNotebook));
		auto_completion (GTK_TEXT_VIEW (FPROPS (current_page, Text)));
	}
}

/**
 * 
 */
static void toggle_readonly (void)
{
	if (OpenedFilesCnt >= 0)
	{
		gint current_page =
			gtk_notebook_get_current_page (GTK_NOTEBOOK (MainNotebook));
		gint readonly = FPROPS (current_page, ReadOnly);
		gboolean writeable = TRUE;

		if (readonly == 0)
		{
			writeable = FALSE;
			FPROPS (current_page, ReadOnly) = 1;
		}
		else if (readonly == 1)
		{
			writeable = TRUE;
			FPROPS (current_page, ReadOnly) = 0;
		}
		else
		{
			print_msg ("File is write-protected!");
			return;
		}

		gtk_text_view_set_editable (GTK_TEXT_VIEW (FPROPS (current_page, Text)),
									writeable);

		refresh_interface ();
	}
}

/**
 * 
 */
static void find_clicked (void)
{
	if (OpenedFilesCnt >= 0)
	{
		search (FALSE);
	}
}

/**
 * 
 */
static void replace_clicked (void)
{
	if (OpenedFilesCnt >= 0)
	{
		search (TRUE);
	}
}

/**
 * 
 */
static void goto_line_clicked (void)
{
	if (OpenedFilesCnt >= 0)
	{
		goto_line ();
	}
}

/**
 * 
 */
static void prefs_clicked (void)
{
	display_prefs (&Settings);
}

/**
 * 
 */
static void help_clicked (void)
{
	beaver_box_message ("There's currently no help available!");
}

/**
 * 
 */
static void about (void)
{
	/* the authors */
	const char *AUTHORS[] = {
		"Double 12",
		"Higor Eurípedes",
		"Trevor Brown",
		"James Buren",
		"Andrea Florio",
		"Tobias Heinzen (retired)",
		"Damien Terrier (retired)",
		"Marc Bevand (retired)",
		"Emmanuel Turquin (retired)",
		"Michael Terry (retired)",
		"Leslie Polzer (retired)",
		NULL
	};

	/* the artists */
	const char *ARTISTS[] = {
		"Vinay Pawar (application icon)",
		"Tigert (about logo)",
		NULL
	};

	/* the comments */
	gchar *COMMENTS =
		g_strconcat (APP_MOTTO, "\n", "Compiled: ", __DATE__, " ", __TIME__,
					 NULL);

	/* load logo */
	GdkPixbuf *logo = gdk_pixbuf_new_from_file (PIXMAP_DIR "/about.xpm", NULL);

	/* create new about window, with tabs ^^ */
	GtkWidget *AboutWindow = g_object_new (GTK_TYPE_ABOUT_DIALOG,
										   "program-name", APP_NAME,
										   "version", VERSION_NUMBER,
										   "copyright",
										   "(C) 1999-2010 Beaver developers",
										   "comments", COMMENTS,
										   "authors", AUTHORS,
										   "artists", ARTISTS,
										   "translator-credits", TRANSLATORS,
										   "website", APP_URL,
										   "logo", logo,
										   NULL);
	g_object_unref (logo);
	g_free (COMMENTS);

	g_signal_connect_swapped (G_OBJECT (AboutWindow), "response",
							  (GtkSignalFunc) gtk_widget_destroy,
							  G_OBJECT (AboutWindow));

	/* show the window */
	gtk_widget_show_all (AboutWindow);
	print_msg ("Display About window...");
}

/**
 * 
 */
static void toolbar_clicked (void)
{
	/* get the widget of the check item in the menu bar */
	GtkWidget *menu_toolbar =
		gtk_ui_manager_get_widget (MenuManager,
								   "/ui/MainMenu/ViewMenu/Toolbar");

	/* get wheter or not the checkbox is active */
	gboolean toggle =
		gtk_check_menu_item_get_active (GTK_CHECK_MENU_ITEM (menu_toolbar));

	/* show or hide the toolbar, depending wheter the checkbox
	 * was active or not */
	if (toggle)
	{
		gtk_widget_show (bar_toolbar);
		print_msg ("Show Toolbar ...");
	}
	else
	{
		gtk_widget_hide (bar_toolbar);
		print_msg ("Hide Toolbar ...");
	}

	/* save back choice into settings */
	TOOLBAR_DISPLAY = toggle;
	set_bool_conf ("/ToolBar/Display", toggle);
}

/**
 * 
 */
static void statusbar_clicked (void)
{
	/* get the widget of the check item in the menu bar */
	GtkWidget *menu_statusbar =
		gtk_ui_manager_get_widget (MenuManager,
								   "/ui/MainMenu/ViewMenu/Statusbar");

	/* get wheter or not the checkbox is active */
	gboolean toggle =
		gtk_check_menu_item_get_active (GTK_CHECK_MENU_ITEM (menu_statusbar));

	/* show or hide the toolbar, depending wheter the checkbox
	 * was active or not */
	if (toggle)
	{
		show_msgbar ();
		print_msg ("Show Statusbar ...");
	}
	else
	{
		hide_msgbar ();
	}

	/* save back choice into settings */
	MSGBAR_DISPLAY = toggle;
	set_bool_conf ("/MsgBar/Display", toggle);
}


static void wordwrap_clicked (void)
{
};

/*****************************************************************/

/**
 * the actions of the menu entries.
 */
static const GtkActionEntry menu_entries[] = {
	/* name, stockid, label */
	{"FileMenuAction", NULL, "_File"},
	{"EditMenuAction", NULL, "_Edit"},
	{"ViewMenuAction", NULL, "_View"},
	{"ToolsMenuAction", NULL, "_Tools"},
	{"LanguageMenuAction", NULL, "_Language"},
	{"HelpMenuAction", NULL, "_Help"},

	// File menu
	{"NewAction", GTK_STOCK_NEW,	/* name, stock id */
	 "_New", "<control>N",	/* label, accelerator */
	 "Start a new document",	/* tooltip */
	 G_CALLBACK (new_file)},	/* callback function */

	{"OpenAction", GTK_STOCK_OPEN,
	 "_Open", "<control>O",
	 "Open a document",
	 G_CALLBACK (open_file)},

	{"SaveAction", GTK_STOCK_SAVE,
	 "_Save", "<control>S",
	 "Save the current document",
	 G_CALLBACK (save_file)},

	{"SaveAsAction", GTK_STOCK_SAVE_AS,
	 "Save _As", "<shift><control>S",
	 "Save the current document under a new name",
	 G_CALLBACK (save_file_as)},

	{"SaveAllAction", GTK_STOCK_SAVE,
	 "Save A_ll", NULL,
	 "Save all documents",
	 G_CALLBACK (save_all)},

	{"PrintAction", GTK_STOCK_PRINT,
	 "_Print", "<control>P",
	 "Print the current document",
	 G_CALLBACK (print_buffer)},

	{"PropertiesAction", GTK_STOCK_PROPERTIES,
	 "_Properties", NULL,
	 "Show the properties of the current document",
	 G_CALLBACK (file_info_clicked)},

	{"CloseAction", GTK_STOCK_CLOSE,
	 "_Close", "<control>W",
	 "Close the current document",
	 G_CALLBACK (close_file)},

	{"CloseAllAction", GTK_STOCK_CLOSE,
	 "Clo_se All", NULL,
	 "Close all documents",
	 G_CALLBACK (close_all)},

	{"QuitAction", GTK_STOCK_QUIT,
	 "_Quit", "<control>Q",
	 "Quit Beaver",
	 G_CALLBACK (quit)},

	// Edit menu
	{"UndoAction", GTK_STOCK_UNDO,
	 "_Undo", "<control>Z",
	 "Undo the last action",
	 G_CALLBACK (undo_clicked)},

	{"RedoAction", GTK_STOCK_REDO,
	 "_Redo", "<shift><control>Z",
	 "Redo the last action",
	 G_CALLBACK (redo_clicked)},

	{"CutAction", GTK_STOCK_CUT,
	 "Cu_t", "<control>X",
	 "Cut the current selection",
	 G_CALLBACK (cut_clicked)},

	{"CopyAction", GTK_STOCK_COPY,
	 "_Copy", "<control>C",
	 "Copy the current selection",
	 G_CALLBACK (copy_clicked)},

	{"PasteAction", GTK_STOCK_PASTE,
	 "_Paste", "<control>V",
	 "Paste the copied or cutted text here",
	 G_CALLBACK (paste_clicked)},

	{"SelectAllAction", NULL,
	 "Select _All", "<control>A",
	 "Select everything in the whole document",
	 G_CALLBACK (select_all_clicked)},

	{"CompleteAction", NULL,
	 "Co_mplete", "<control>space",
	 "Complete the current command or code tag",
	 G_CALLBACK (complete_clicked)},

	{"ToggleReadonlyAction", NULL,
	 "Toggle Rea_donly", NULL,
	 "Toggle if this document is writable or not",
	 G_CALLBACK (toggle_readonly)},

	{"FindAction", GTK_STOCK_FIND,
	 "_Find", "<control>F",
	 "Find a certain phrase in this file",
	 G_CALLBACK (find_clicked)},

	{"ReplaceAction", GTK_STOCK_FIND_AND_REPLACE,
	 "R_eplace", "<control>R",
	 "Find a certain phrase in this file and replace it with another one",
	 G_CALLBACK (replace_clicked)},

	{"GoToLineAction", GTK_STOCK_JUMP_TO,
	 "_Go to Line", "<control>L",
	 "Change the cursor to a given line number",
	 G_CALLBACK (goto_line_clicked)},

	{"PreferencesAction", GTK_STOCK_PREFERENCES,
	 "Prefere_nces", NULL,
	 "Change the preferences",
	 G_CALLBACK (prefs_clicked)},

	// View menu
	{"DocTabsAction", NULL,
	 "_Doc Tabs", NULL,
	 "Choose the placement of the document tabs",
	 NULL},

	{"ScrollbarAction", NULL,
	 "Sc_rollbar", NULL,
	 "Choose the placement of the scrollbar",
	 NULL},

	// Help menu
	{"ContentsAction", GTK_STOCK_HELP,
	 "_Contents", "F1",
	 "Show the help contents",
	 G_CALLBACK (help_clicked)},

	{"AboutAction", GTK_STOCK_ABOUT,
	 "_About", NULL,
	 "About this program",
	 G_CALLBACK (about)},
};

/**
 * the actions for all menu elements that have check boxes
 */
static const GtkToggleActionEntry toggle_entries[] = {
	{"ToolbarAction", NULL,
	 "_Toolbar", NULL,
	 "Show/do not show the toolbar",
	 G_CALLBACK (toolbar_clicked),},

	{"StatusbarAction", NULL,
	 "_Statusbar", NULL,
	 "Show/do not show the statusbar",
	 G_CALLBACK (statusbar_clicked),},

	{"WordWrapAction", NULL,
	 "_Word-wrap", NULL,
	 "Word-wrap on/off",
	 G_CALLBACK (wordwrap_clicked)},
};

/* number of entries */
static const guint n_menu_entries = G_N_ELEMENTS (menu_entries);
static const guint n_toggle_entries = G_N_ELEMENTS (toggle_entries);

/**
 * @param   Text buffer to get the text from
 * @return  returns the text of the given buffer. pointer must be freed after
 *          use. 
 */
static inline gchar *get_text (GtkTextBuffer * Buffer)
{
	GtkTextIter start, end;
	gchar *str;

	gtk_text_buffer_get_bounds (Buffer, &start, &end);
	str = gtk_text_buffer_get_slice (Buffer, &start, &end, FALSE);

	return str;
}

/**
 * refreshes the label of the notebook tab.
 * 
 * @param   tab     the number of the tab to refresh the label off
 */
static void refresh_notebook_label (gint tab)
{
	/* nothing to update */
	if (OpenedFilesCnt < 0)
		return;

	/* the label */
	gchar *label = NULL;

	/* get the name of the current file opened */
	gchar *basename = FPROPS (tab, BaseName);
	if (!basename)
	{
		basename = "";
	}

	/* add a "RO" if the file is read only */
	if (FPROPS (tab, ReadOnly))
	{
		label = g_strconcat ("RO ", basename, NULL);
	}
	else
	{
		label = g_strdup (basename);
	}

	/* add a "*" if the file was edited */
	if (gtk_text_buffer_get_modified (FPROPS (tab, Buffer)))
	{
		gchar *new_label = g_strdup (label);
		g_free (label);
		label = g_strconcat ("*", new_label, NULL);
		g_free (new_label);
	}

	/* set label */
	GtkWidget *child = gtk_notebook_get_nth_page (GTK_NOTEBOOK (MainNotebook), tab);
	if (child != NULL)
	{
		GtkWidget *tablab =
			gtk_notebook_get_tab_label (GTK_NOTEBOOK (MainNotebook), child);
		GList *childs = gtk_container_get_children (GTK_CONTAINER (tablab));
		gtk_label_set_text (GTK_LABEL (childs->data), label);
		g_list_free(childs);
	}

	/* free up space */
	g_free (label);
}

/**
 * refreshes the scrollbar position of a given tab
 * 
 * @param	tab		the number of the tab to refresh the scrollbar of
 */
static void refresh_notebook_scrollbar (gint tab)
{
	/* get the scrolled window of the tab for setting the
	 * scrollbar of */
	GtkScrolledWindow *scroll =
		GTK_SCROLLED_WINDOW (GTK_WIDGET (FPROPS (tab, Text))->parent);

	/* now set the scrollbar position */
	switch (SCROLLBAR_POSITION)
	{
	case 1:	/* LEFT */
		gtk_scrolled_window_set_placement (scroll, GTK_CORNER_TOP_RIGHT);
		break;
	case 2:	/* RIGHT */
	default:
		gtk_scrolled_window_set_placement (scroll, GTK_CORNER_TOP_LEFT);
		break;
	}
}

/**
 * refreshes the text buffer (sets font&color scheme as well as some
 * other options)
 * 
 * @param	tab	the number of the tab to refresh the text buffer of
 */
static void refresh_notebook_text (gint tab)
{
	if (FPROPS (tab, Text) != NULL)
	{
		/* setting the new font */
		PangoFontDescription *desc = pango_font_description_from_string (FONT);
		gtk_widget_modify_font (FPROPS (tab, Text), desc);
		pango_font_description_free (desc);

		/* setting the new color */
		GdkColor color;
		color.red = SELECTED_BG_RED;
		color.green = SELECTED_BG_GREEN;
		color.blue = SELECTED_BG_BLUE;
		gtk_widget_modify_base (FPROPS (tab, Text), GTK_STATE_SELECTED, &color);
		color.red = SELECTED_FG_RED;
		color.green = SELECTED_FG_GREEN;
		color.blue = SELECTED_FG_BLUE;
		gtk_widget_modify_text (FPROPS (tab, Text), GTK_STATE_SELECTED, &color);
		color.red = BG_RED;
		color.green = BG_GREEN;
		color.blue = BG_BLUE;
		gtk_widget_modify_base (FPROPS (tab, Text), GTK_STATE_NORMAL, &color);
		color.red = FG_RED;
		color.green = FG_GREEN;
		color.blue = FG_BLUE;
		gtk_widget_modify_text (FPROPS (tab, Text), GTK_STATE_NORMAL, &color);

		/* setting the word wrap */
		if (!TOGGLE_WORDWRAP)
		{
			gtk_text_view_set_wrap_mode (GTK_TEXT_VIEW (FPROPS (tab, Text)),
										 GTK_WRAP_NONE);
		}
		else
		{
			gtk_text_view_set_wrap_mode (GTK_TEXT_VIEW (FPROPS (tab, Text)),
										 GTK_WRAP_WORD_CHAR);
		}
	}
}

/**
 * Sets the sensitivity of a UI item
 * 
 * @param   path    the path of the ui item in the xml
 * @param   toggle  wheter it is sensitive or not
 */
static void ui_item_set_sensitive (const gchar * path, gboolean toggle)
{
	GtkWidget *widget = gtk_ui_manager_get_widget (MenuManager, path);
	gtk_widget_set_sensitive (widget, toggle);
}

/**
 * Handles the key pressing event on the current GtkTextView
 * 
 * @param view       the active GtkTextView
 * @param event      the GdkEventKey that stores the current event data
 * @param user_data  Nothing.
 *
 * @return discards or not the normal behavior of the key
 */
static gboolean key_press (GtkWidget * view, GdkEventKey * event,
						   gpointer user_data)
{
	/* stops, if it isnt a key press */
	if (GDK_KEY_PRESS != event->type)
	{
		return FALSE;
	}

	gboolean is_shift, is_alt, is_control;
	is_shift = (event->state & GDK_SHIFT_MASK) != 0;
	is_alt = (event->state & GDK_MOD1_MASK) != 0;
	is_control = (event->state & GDK_CONTROL_MASK) != 0;

	/* if just the home key was pressed */
	if (GDK_Home == event->keyval && !(is_alt || is_control))
	{
		GtkTextIter iter;
		GtkTextMark *mark;
		GtkTextBuffer *buffer;
		gunichar ch = 0;
		gint old_pos, distance;

		buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (view));

		/* get the cursor iter and store the current position */
		mark = gtk_text_buffer_get_insert (buffer);
		gtk_text_buffer_get_iter_at_mark (buffer, &iter, mark);
		old_pos = distance = gtk_text_iter_get_line_index (&iter);
		gtk_text_iter_set_line_index (&iter, 0);

		/* stop when we find a non-whitespace character or the end of the line */
		while (g_unichar_isspace ((ch = gtk_text_iter_get_char (&iter)))
			   && !gtk_text_iter_ends_line (&iter))
		{
			gtk_text_iter_forward_char (&iter);
			distance--;
		}

		/* if we came back to the same position, allow the normal effect of the home key */
		if (!distance)
			return FALSE;

		/* if shift is pressed, select the text... */
		if (is_shift)
		{
			GtkTextIter end;

			if (!gtk_text_buffer_get_has_selection (buffer))
			{
				/* current cursor pos is the end of the selection */
				gtk_text_buffer_get_iter_at_mark (buffer, &end, mark);
			}
			else
			{
				/* get the end of the current selection */
				gtk_text_buffer_get_selection_bounds (buffer, NULL, &end);
			}

			/* select ;) */
			gtk_text_buffer_select_range (buffer, &iter, &end);
		}
		else
		{	/* else, just move the cursor */
			gtk_text_buffer_place_cursor (buffer, &iter);
			gtk_text_view_place_cursor_onscreen (GTK_TEXT_VIEW (view));
		}

		/* discards the normal behaviour */
		return TRUE;
	}

	return FALSE;
}

/**
 * Returns the width of a given character in pixels.
 * 
 * @param view a GtkTextView to extract the information
 * @param ch   the character
 * 
 */
static inline gint get_char_width (GtkTextView * view, const gchar * ch)
{
	PangoLayout *layout;
	gint width = 0;

	layout = gtk_widget_create_pango_layout (GTK_WIDGET (view), ch);

	if (layout != NULL)
	{
		pango_layout_get_pixel_size (layout, &width, NULL);
		g_object_unref (G_OBJECT (layout));

		return width;
	}

	return 0;
}

/**
 * This function is called when the widget is refreshed on the screen (expose event)
 */
static gboolean expose_event (GtkWidget * view, GdkEventExpose * event,
							  gpointer user_data)
{
	/* only draw if the user wants to */
	if (!MARKER)
	{
		return FALSE;
	}

	GdkRectangle visible_rect;
	GdkRectangle redraw_rect;

	cairo_t *cr;
	double x;

	gtk_text_view_get_visible_rect (GTK_TEXT_VIEW (view), &visible_rect);

	gtk_text_view_buffer_to_window_coords (GTK_TEXT_VIEW (view),
										   GTK_TEXT_WINDOW_TEXT, visible_rect.x,
										   visible_rect.y, &redraw_rect.x,
										   &redraw_rect.y);

	redraw_rect.width = visible_rect.width;
	redraw_rect.height = visible_rect.height;

	cr = gdk_cairo_create (gtk_text_view_get_window
						   (GTK_TEXT_VIEW (view), GTK_TEXT_WINDOW_TEXT));

	/* set clip region */
	cairo_rectangle (cr, event->area.x, event->area.y, event->area.width,
					 event->area.height);
	cairo_clip (cr);

	/* the x position of the right margin */
	x = visible_rect.x + redraw_rect.x + 0.5 +
		gtk_text_view_get_left_margin (GTK_TEXT_VIEW (view)) +
		(get_char_width (GTK_TEXT_VIEW (view), "_") *
		 gtk_text_view_get_right_margin (GTK_TEXT_VIEW (view)));

	/* change the line width */
	cairo_set_line_width (cr, 0.7);

	cairo_move_to (cr, x, redraw_rect.y);
	cairo_line_to (cr, x, redraw_rect.y + redraw_rect.height);

	cairo_set_source_rgba (cr, 0.0, 0.0, 0.0, 0.3);

	/* draw the line */
	cairo_stroke (cr);
	cairo_destroy(cr);

	return FALSE;
}

/**
 * 
 * @param   filename    the file to open or NULL to create
 *                      a new file. opens a new textview
 *                      in the main notebook.
 */
static inline void notebook_add_page (const gchar * filename)
{
	/* we first check if the file has already been opened. if this
	 * is the case, then we set the current notebook tab to this
	 * one */
	if (filename != NULL
		&& g_hash_table_lookup (opened_file_names, filename) == (gpointer) 1)
	{

		/* now we iterate through the tabs and set the one current,
		 * which has the same filename. */
		gint i = 0;
		for (i = 0; i < OpenedFilesCnt; i++)
		{
			if (g_ascii_strcasecmp (filename, FPROPS (i, Name)) == 0)
			{
				gtk_notebook_set_current_page (GTK_NOTEBOOK (MainNotebook), i);
				break;
			}
		}

		return;
	}

	/* the new file needs file properties, that we can
	 * access later on. so we create a new properties 
	 * object and increase the file counter */
	t_fprops *properties = g_malloc0 (sizeof (t_fprops));
	g_array_insert_val (FileProperties, OpenedFilesCnt + 1, properties);
	/* HACK!!! */
	FPROPS (OpenedFilesCnt + 1, BaseName) = NULL;
	FPROPS (OpenedFilesCnt + 1, WidgetInfo.current_action) = NULL;

	/* we have opened a new file */
	OpenedFilesCnt++;

	/* create a new scroll window and textview for the new
	 * notebook page */
	GtkWidget *scrolled_window = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_window),
									GTK_POLICY_AUTOMATIC, GTK_POLICY_ALWAYS);

	FPROPS (OpenedFilesCnt, Text) = gtk_text_view_new ();
	FPROPS (OpenedFilesCnt, Buffer) =
		gtk_text_view_get_buffer (GTK_TEXT_VIEW
								  (FPROPS (OpenedFilesCnt, Text)));

	gtk_container_add (GTK_CONTAINER (scrolled_window),
					   FPROPS (OpenedFilesCnt, Text));

	/* changing font to selected font in the preferences */
	PangoFontDescription *desc = pango_font_description_from_string (FONT);
	gtk_widget_modify_font (FPROPS (OpenedFilesCnt, Text), desc);
	pango_font_description_free (desc);

	/* changing selection color, background and forground color
	 * of the texfield according to the preferences */
	GdkColor color;
	color.red = SELECTED_BG_RED;
	color.green = SELECTED_BG_GREEN;
	color.blue = SELECTED_BG_BLUE;
	gtk_widget_modify_base (FPROPS (OpenedFilesCnt, Text), GTK_STATE_SELECTED,
							&color);
	color.red = SELECTED_FG_RED;
	color.green = SELECTED_FG_GREEN;
	color.blue = SELECTED_FG_BLUE;
	gtk_widget_modify_text (FPROPS (OpenedFilesCnt, Text), GTK_STATE_SELECTED,
							&color);
	color.red = BG_RED;
	color.green = BG_GREEN;
	color.blue = BG_BLUE;
	gtk_widget_modify_base (FPROPS (OpenedFilesCnt, Text), GTK_STATE_NORMAL,
							&color);
	color.red = FG_RED;
	color.green = FG_GREEN;
	color.blue = FG_BLUE;
	gtk_widget_modify_text (FPROPS (OpenedFilesCnt, Text), GTK_STATE_NORMAL,
							&color);

	/* append new notebook page */
	GtkWidget *content = scrolled_window;
	GtkWidget *tab_label = gtk_label_new ("");

	GtkRequisition size;
	GtkWidget *close_image =
		gtk_image_new_from_stock (GTK_STOCK_CLOSE, GTK_ICON_SIZE_MENU);
	gtk_widget_size_request (close_image, &size);

	GtkWidget *tab_button = gtk_button_new ();
	gtk_widget_set_size_request (tab_button, size.width, size.height);
	gtk_button_set_relief (GTK_BUTTON (tab_button), GTK_RELIEF_NONE);
	gtk_button_set_focus_on_click (GTK_BUTTON (tab_button), FALSE);
	gtk_container_add (GTK_CONTAINER (tab_button), close_image);

	/* make the buttons on the tab small */
	GtkRcStyle *rcstyle = gtk_rc_style_new ();
	rcstyle->xthickness = rcstyle->ythickness = 0;
	gtk_widget_modify_style (tab_button, rcstyle);
	gtk_rc_style_unref (rcstyle);

	GtkWidget *align = gtk_alignment_new (1.0, 0.0, 0.0, 0.0);
	gtk_container_add (GTK_CONTAINER (align), tab_button);

	GtkWidget *box = gtk_hbox_new (FALSE, 2);
	gtk_box_pack_start (GTK_BOX (box), tab_label, TRUE, TRUE, 4);
	gtk_box_pack_start (GTK_BOX (box), align, FALSE, FALSE, 0);
	gtk_widget_show_all (box);

	gtk_notebook_append_page (GTK_NOTEBOOK (MainNotebook), content, box);

	/* make sure the notebook is shown */
	gtk_widget_show_all (MainNotebook);

	/* make new text active */
	gtk_notebook_set_current_page (GTK_NOTEBOOK (MainNotebook), OpenedFilesCnt);

	/* if no filename was given, then this means, that a new
	 * file has to be created */
	if (!filename)
	{
		gchar *string = g_strdup_printf (UNTITLED " %d", ++NewFilesCnt);
		init_file_properties (string, OpenedFilesCnt);

		open_file_in_editor (GTK_WIDGET (FPROPS (OpenedFilesCnt, Text)), NULL,
							 OpenedFilesCnt);

		g_hash_table_insert (opened_file_names, (gpointer) g_strdup (string),
							 (gpointer) 1);
		g_signal_connect (G_OBJECT (tab_button), "clicked",
						  G_CALLBACK (close_file_from_tab),
						  (gpointer) g_strdup (string));

		g_free (string);
	}
	else
	{
		init_file_properties (filename, OpenedFilesCnt);

		open_file_in_editor (GTK_WIDGET (FPROPS (OpenedFilesCnt, Text)),
							 filename, OpenedFilesCnt);

		g_hash_table_insert (opened_file_names, (gpointer) g_strdup (filename),
							 (gpointer) 1);
		g_signal_connect (G_OBJECT (tab_button), "clicked",
						  G_CALLBACK (close_file_from_tab),
						  (gpointer) g_strdup (filename));
	}

	/* attach some signals for the 80-char marker line
	 * as well as for the intelligent home-key behaviour */
	gtk_text_view_set_right_margin (GTK_TEXT_VIEW
									(FPROPS (OpenedFilesCnt, Text)), 80);
	g_signal_connect_after (FPROPS (OpenedFilesCnt, Text), "expose-event",
							G_CALLBACK (expose_event), NULL);
	g_signal_connect (FPROPS (OpenedFilesCnt, Text), "key-press-event",
					  G_CALLBACK (key_press), NULL);

	/* refresh the interface */
	refresh_interface ();
}

/**
 * Refreshes some parts of the interface. this function is called
 * when tabs are changed or new files are created
 */
void refresh_interface (void)
{
	/* the current page that is selected */
	gint current_page =
		gtk_notebook_get_current_page (GTK_NOTEBOOK (MainNotebook));

	/*** REFRESH LABELS OF NOTEBOOK_TABS ***/
	gint i = 0;
	for (; i < OpenedFilesCnt + 1; i++)
	{
		refresh_notebook_label (i);
		refresh_notebook_scrollbar (i);
		refresh_notebook_text (i);
	}

	/*** REFRESH TITLE OF WINDOW ***/
	/* determine which title to set, first check if there are some
	 * file opened */
	gchar *Title;
	if (current_page == -1)
	{
		Title = g_strconcat (APP_NAME, NULL);
	}
	else
	{
		/* get the name of the current file opened */
		gchar *basename = FPROPS (current_page, BaseName);

		/* make sure that a filename really exists */
		if (!basename)
		{
			Title = g_strconcat (APP_NAME, NULL);
		}
		else
		{
			Title = g_strconcat (APP_NAME, " - ", basename, NULL);
		}
	}

	/* make sure that nothing went wrong with strconcat */
	if (Title != NULL)
	{
		/* set title */
		gtk_window_set_title (GTK_WINDOW (MainWindow), Title);

		/* free the title */
		g_free (Title);
	}

	/*** MISC ***/

	/* put the tab position to user choosen position */
	switch (TAB_POSITION)
	{
	case 0:	/* TOP */
	default:
		gtk_notebook_set_tab_pos (GTK_NOTEBOOK (MainNotebook), GTK_POS_TOP);
		break;
	case 1:	/* BOTTOM */
		gtk_notebook_set_tab_pos (GTK_NOTEBOOK (MainNotebook), GTK_POS_BOTTOM);
		break;
	case 2:	/* LEFT */
		gtk_notebook_set_tab_pos (GTK_NOTEBOOK (MainNotebook), GTK_POS_LEFT);
		break;
	case 3:	/* RIGHT */
		gtk_notebook_set_tab_pos (GTK_NOTEBOOK (MainNotebook), GTK_POS_RIGHT);
		break;
	}

	/* set the toolbar style */
	switch (TOOLBAR_STYLE)
	{
	case 0:	/* ICONS */
		gtk_toolbar_set_style (GTK_TOOLBAR (bar_toolbar), GTK_TOOLBAR_ICONS);
		break;
	case 1:	/* TEXT */
		gtk_toolbar_set_style (GTK_TOOLBAR (bar_toolbar), GTK_TOOLBAR_TEXT);
		break;
	case 2:	/* BOTH */
	default:
		gtk_toolbar_set_style (GTK_TOOLBAR (bar_toolbar), GTK_TOOLBAR_BOTH);
		break;
	}

	/* set the size of the toolbar */
	switch (TOOLBAR_SIZE)
	{
	case 0:	/* SMALL */
		gtk_toolbar_set_icon_size (GTK_TOOLBAR (bar_toolbar),
								   GTK_ICON_SIZE_MENU);
		break;
	case 1:	/* LARGE */
	default:
		gtk_toolbar_set_icon_size (GTK_TOOLBAR (bar_toolbar),
								   GTK_ICON_SIZE_LARGE_TOOLBAR);
		break;
	}

	/*** MENU SENSITIVITY ***/

	/* deactivate/activate some buttons if there are
	 * files open or not */
	gboolean opened = (OpenedFilesCnt >= 0);

	ui_item_set_sensitive ("/ui/MainMenu/FileMenu/Save", opened);
	ui_item_set_sensitive ("/ui/MainMenu/FileMenu/SaveAs", opened);
	ui_item_set_sensitive ("/ui/MainMenu/FileMenu/SaveAll", opened);
	ui_item_set_sensitive ("/ui/MainMenu/FileMenu/Print", opened);
	ui_item_set_sensitive ("/ui/MainMenu/FileMenu/Properties", opened);
	ui_item_set_sensitive ("/ui/MainMenu/FileMenu/Close", opened);
	ui_item_set_sensitive ("/ui/MainMenu/FileMenu/CloseAll", opened);
	ui_item_set_sensitive ("/ui/MainMenu/EditMenu/Undo", opened);
	ui_item_set_sensitive ("/ui/MainMenu/EditMenu/Redo", opened);
	ui_item_set_sensitive ("/ui/MainMenu/EditMenu/Cut", opened);
	ui_item_set_sensitive ("/ui/MainMenu/EditMenu/Copy", opened);
	ui_item_set_sensitive ("/ui/MainMenu/EditMenu/Paste", opened);
	ui_item_set_sensitive ("/ui/MainMenu/EditMenu/SelectAll", opened);
	ui_item_set_sensitive ("/ui/MainMenu/EditMenu/Complete", opened);
	ui_item_set_sensitive ("/ui/MainMenu/EditMenu/ToggleReadonly", opened);
	ui_item_set_sensitive ("/ui/MainMenu/EditMenu/Find", opened);
	ui_item_set_sensitive ("/ui/MainMenu/EditMenu/Replace", opened);
	ui_item_set_sensitive ("/ui/MainMenu/EditMenu/GoToLine", opened);

	ui_item_set_sensitive ("/ui/MainToolbar/Close", opened);
	ui_item_set_sensitive ("/ui/MainToolbar/Save", opened);
	ui_item_set_sensitive ("/ui/MainToolbar/Print", opened);
	ui_item_set_sensitive ("/ui/MainToolbar/Undo", opened);
	ui_item_set_sensitive ("/ui/MainToolbar/Redo", opened);
	ui_item_set_sensitive ("/ui/MainToolbar/Cut", opened);
	ui_item_set_sensitive ("/ui/MainToolbar/Copy", opened);
	ui_item_set_sensitive ("/ui/MainToolbar/Paste", opened);
	ui_item_set_sensitive ("/ui/MainToolbar/Find", opened);
	ui_item_set_sensitive ("/ui/MainToolbar/Replace", opened);
	ui_item_set_sensitive ("/ui/MainToolbar/GoToLine", opened);

	/* from here on we need a valid tab */
	if (current_page == -1)
		return;

	/* note if saveable */
	if (gtk_text_buffer_get_modified (FPROPS (current_page, Buffer)))
	{
		ui_item_set_sensitive ("/ui/MainMenu/FileMenu/Save", TRUE);
		ui_item_set_sensitive ("/ui/MainToolbar/Save", TRUE);
	}
	else
	{
		ui_item_set_sensitive ("/ui/MainMenu/FileMenu/Save", FALSE);
		ui_item_set_sensitive ("/ui/MainToolbar/Save", FALSE);
	}

	/* note if redo is possible */
	gboolean redo = redo_is_possible ();
	ui_item_set_sensitive ("/ui/MainMenu/EditMenu/Redo", redo);
	ui_item_set_sensitive ("/ui/MainToolbar/Redo", redo);

	/* note if undo is possible */
	gboolean undo = undo_is_possible ();
	ui_item_set_sensitive ("/ui/MainMenu/EditMenu/Undo", undo);
	ui_item_set_sensitive ("/ui/MainToolbar/Undo", undo);

	/* if in readonly mode then remove paste in menu */
	if (FPROPS (current_page, ReadOnly) != 0)
	{
		ui_item_set_sensitive ("/ui/MainMenu/EditMenu/Paste", FALSE);
		ui_item_set_sensitive ("/ui/MainToolbar/Paste", FALSE);
	}
	else
	{
		ui_item_set_sensitive ("/ui/MainMenu/EditMenu/Paste", TRUE);
		ui_item_set_sensitive ("/ui/MainToolbar/Paste", TRUE);
	}

	calculate_internal_tab_width (GTK_TEXT_VIEW (FPROPS (current_page, Text)));
}


void calculate_internal_tab_width (GtkTextView * view)
{
	PangoTabArray *tab_array;
	gint width;
	gchar *str = g_strnfill (get_int_conf ("General/Editor/TabWidth"),' ');

	width = get_char_width (view, str);
	g_free(str);

	tab_array = pango_tab_array_new (1, TRUE);
	pango_tab_array_set_tab (tab_array, 0, PANGO_TAB_LEFT, width);

	gtk_text_view_set_tabs (view, tab_array);

	pango_tab_array_free (tab_array);
}

/**
 * starts the main interface (editor) of beaver. gets called from main.c
 * 
 * @param   argc    how many arguments
 * @param   argv    list of arguments
 */
void interface (gint argc, gchar * argv[])
{
	GError *error = NULL;
	GtkWidget *vbox = NULL;
	GtkActionGroup *action_group = NULL;

	/* initialize gtk */
	gtk_set_locale ();
	gtk_init (&argc, &argv);

	/* set the beaver logo as the default logo for
	 * all windows to be opened */
	gtk_window_set_default_icon_from_file (ICON_DIR "/beaver.png", NULL);

	/* initialize settings */
	Settings = init_settings ();

	/* array that holds all information about opened
	 * files. init it here */
	FileProperties = g_array_new (TRUE, FALSE, sizeof (t_fprops));

	/* create a the main window */
	MainWindow = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_window_set_resizable (GTK_WINDOW (MainWindow), TRUE);
	gtk_window_set_default_size (GTK_WINDOW (MainWindow), MAIN_WINDOW_WIDTH,
								 MAIN_WINDOW_HEIGHT);
	gtk_window_resize (GTK_WINDOW (MainWindow), MAIN_WINDOW_WIDTH,
					   MAIN_WINDOW_HEIGHT);

	/* create new ui manager */
	MenuManager = gtk_ui_manager_new ();

	/* attach actions to the menues */
	action_group = gtk_action_group_new ("MenuActions");
	gtk_ui_manager_insert_action_group (MenuManager, action_group, 0);

	gtk_action_group_add_actions (action_group, menu_entries, n_menu_entries,
								  NULL);
	gtk_action_group_add_toggle_actions (action_group, toggle_entries,
										 n_toggle_entries, NULL);

	gtk_window_add_accel_group (GTK_WINDOW (MainWindow),
								gtk_ui_manager_get_accel_group (MenuManager));

	/* load ui description from xml file */
	gtk_ui_manager_add_ui_from_file (MenuManager, RESOURCE_DIR "/standard.xml",
									 &error);
	if (error)
	{
		/* error occured: display error message and quit */
		g_message ("building menus failed: %s", error->message);
		g_error_free (error);
		return;
	}

	/* the main window consists of 3 vertical boxes. in the first box
	 * there is the menu, in the second box is the toolbar and in the
	 * third box there is the main notebook with all the open files. */
	vbox = gtk_vbox_new (FALSE, 0);
	gtk_container_add (GTK_CONTAINER (MainWindow), vbox);

	/* display menu bar */
	bar_menu = gtk_ui_manager_get_widget (MenuManager, "ui/MainMenu");
	gtk_box_pack_start (GTK_BOX (vbox), bar_menu, FALSE, FALSE, 0);

	/* display toolbar */
	bar_toolbar = gtk_ui_manager_get_widget (MenuManager, "ui/MainToolbar");
	gtk_box_pack_start (GTK_BOX (vbox), bar_toolbar, FALSE, FALSE, 0);

	/* create a new notebook and display it */
	MainNotebook = gtk_notebook_new ();
	gtk_notebook_set_scrollable (GTK_NOTEBOOK (MainNotebook), TRUE);
	gtk_box_pack_start (GTK_BOX (vbox), MainNotebook, TRUE, TRUE, 0);

	/* initialize message bar */
	init_msgbar (GTK_BOX (vbox));

	/* show main window and contents */
	gtk_widget_show_all (MainWindow);

	/* create hashtable with the names of all opened
	 * files. this avoids opening the same file twice and
	 * thus creating coherency problems in those files */
	opened_file_names = g_hash_table_new (g_str_hash, g_str_equal);

	/* initialize syntax higlighting */
	read_uedit_wordfile (WORDFILE);
	editor_init ();
	init_languages_menu ();

	/* initialize plugin system */
	plugin_init ();

	/* load up all files that we're given in the
	 * command line
	 */
	gint n = 1;
	for (; n < argc; n++)
	{
		struct stat Stats;
		if (stat (argv[n], &Stats) != -1)
		{
			notebook_add_page (argv[n]);
		}
	}

	/* show/hide the toolbar depending on setting */
	if (TOOLBAR_DISPLAY)
	{
		gtk_widget_show (bar_toolbar);
		GtkWidget *menu_toolbar =
			gtk_ui_manager_get_widget (MenuManager,
									   "/ui/MainMenu/ViewMenu/Toolbar");
		gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM (menu_toolbar),
										TRUE);
	}
	else
	{
		gtk_widget_hide (bar_toolbar);
	}

	/* show/hide the statusbar depending on setting */
	if (MSGBAR_DISPLAY)
	{
		show_msgbar ();
		GtkWidget *menu_statusbar =
			gtk_ui_manager_get_widget (MenuManager,
									   "/ui/MainMenu/ViewMenu/Statusbar");
		gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM (menu_statusbar),
										TRUE);
	}
	else
	{
		hide_msgbar ();
	}

	/* refresh window */
	refresh_interface ();

	/* attach some signals */
	g_signal_connect_after (G_OBJECT (MainNotebook), "switch-page",
							G_CALLBACK (refresh_interface), NULL);
	g_signal_connect (G_OBJECT (MainWindow), "delete-event", G_CALLBACK (quit),
					  NULL);

	/* go into gtk_main function (loop) */
	gtk_main ();
}

/* DEPRECATED - just for compile */
void menu_set_sensitive (const gchar * id, gboolean setting)
{
#ifdef DEBUG
	printf ("DEPRECATED: %s (%s,%d)\n", __FUNCTION__, id, setting);
#endif
}

void menu_items_treatment (GtkWidget * Widget, gpointer Op_in)
{
#ifdef DEBUG
	printf ("DEPRECATED: %s\n", __FUNCTION__);
#endif
}

GtkItemFactory *MainFactory;
