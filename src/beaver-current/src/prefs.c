
/*
** Beaver's an Early AdVanced EditoR
** (C) 1999-2000 Marc Bevand, Damien Terrier and Emmanuel Turquin
**
** prefs.c
**
** Author<s>:   Emmanuel Turquin (aka "Ender") <turqui_e@epita.fr>
** Latest update: Wed Jan 10 20:30:44 2001
** Description:   Beaver preferences tool source
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/** @file prefs.c
 * 
 */

#include <string.h> /* memset */
#include <gtk/gtk.h>
#include "struct.h"
#include "prefs.h"
#include "main.h"
#include "msgbar.h"
#include "interface.h"
#include "conf.h"

/* statics */
static GtkWidget *pref_window = NULL;
static GtkWidget *pref_notebook = NULL;

/* externs */
extern t_settings Settings;		/* settings from interface.c */

/**
 * initializes settings (*duh* ^^)
 * 
 * @returns	returning a structure with settings loaded
 * 			from the config file for easy access.
 */
t_settings init_settings (void)
{
	t_settings set;

	memset (&set, 0, sizeof(set));
	set.recent_files = get_int_conf ("General/RecentFiles/MaxNb");
	set.recent_files = MIN (set.recent_files, MAX_POSSIBLE_RECENT_FILES);
	set.main_window_size_autosave = get_bool_conf ("General/Window/Autosave");
	set.main_window_width = get_int_conf ("General/Window/Width");
	set.main_window_height = get_int_conf ("General/Window/Height");
	set.msgbar_display = get_bool_conf ("General/MsgBar/Display");
	set.msgbar_interval = get_int_conf ("General/MsgBar/Interval");
	set.toggle_wordwrap = get_bool_conf ("General/Editor/Wordwrap");
	set.tab_width = get_int_conf ("General/Editor/TabWidth");
	set.toolbar_display = get_bool_conf ("General/ToolBar/Display");
	set.max_tab_label_length = get_int_conf ("General/Tabs/LabelLength");
	set.tab_position = get_int_conf ("General/Tabs/Position");
	set.scrollbar_position = get_int_conf ("General/ScrollBar/Position");
	set.complete_window_width = get_int_conf ("General/CompletionPopUp/Width");
	set.complete_window_height =
		get_int_conf ("General/CompletionPopUp/Height");
	set.backup = get_bool_conf ("General/AutoSave/Backup");
	set.backup_ext = (gchar *) get_string_conf ("General/AutoSave/BackupExt");
	set.autosave_delay = get_int_conf ("General/AutoSave/Delay");
	set.directory = (gchar *) get_string_conf ("General/RecentFiles/Directory");
	set.font = (gchar *) get_string_conf ("General/Editor/Font");
	set.wordfile = (gchar *) get_string_conf ("General/Editor/Wordfile");
	set.bg[0] = get_int_conf ("General/Editor/BGRed");
	set.bg[1] = get_int_conf ("General/Editor/BGGreen");
	set.bg[2] = get_int_conf ("General/Editor/BGBlue");
	set.fg[0] = get_int_conf ("General/Editor/FGRed");
	set.fg[1] = get_int_conf ("General/Editor/FGGreen");
	set.fg[2] = get_int_conf ("General/Editor/FGBlue");
	set.selected_bg[0] = get_int_conf ("General/Editor/SelectedBGRed");
	set.selected_bg[1] = get_int_conf ("General/Editor/SelectedBGGreen");
	set.selected_bg[2] = get_int_conf ("General/Editor/SelectedBGBlue");
	set.selected_fg[0] = get_int_conf ("General/Editor/SelectedFGRed");
	set.selected_fg[1] = get_int_conf ("General/Editor/SelectedFGGreen");
	set.selected_fg[2] = get_int_conf ("General/Editor/SelectedFGBlue");
	set.syn_high = get_bool_conf ("General/Adv/SynHigh");
	set.syn_high_depth = get_int_conf ("General/Adv/SynHighDepth");
	set.auto_indent = get_bool_conf ("General/Editor/AutoIndent");
	set.auto_correct = get_bool_conf ("General/Adv/AutoCorrect");
	set.toolbar_style = get_int_conf ("General/ToolBar/Style");
	set.toolbar_size = get_int_conf ("General/ToolBar/Size");
	set.marker = get_bool_conf ("General/Misc/Marker");


	/* make sure selected font exists and can be 
	 * used. we check this by simply creating a pango
	 * font descriptor. if this is working we fall back
	 * to a default font. */
	PangoFontDescription *font_desc;
	font_desc = pango_font_description_from_string (set.font);
	if (pango_font_description_get_size (font_desc) == 0)
	{
		set.font = g_strdup ("Courier 12");
	}
	pango_font_description_free (font_desc);

	return (set);
}

/**
 *
 */
static void signal_tab_menu_activate (GtkWidget * menu_item, gpointer user_data)
{
	/* get the position that the user choose */
	gint choice = GPOINTER_TO_INT (user_data);

	/* save back new settings */
	TAB_POSITION = choice;
	set_int_conf ("/Tabs/Position", choice);

	/* refresh the interface for the new settings */
	refresh_interface ();
}

/**
 * 
 */
static void signal_scroll_menu_activate (GtkWidget * menu_item,
										 gpointer user_data)
{
	/* get the position that the user choose */
	gint choice = GPOINTER_TO_INT (user_data);

	/* save back new settings */
	SCROLLBAR_POSITION = choice;
	set_int_conf ("/ScrollBar/Position", choice);

	/* refresh the interface for the new settings */
	refresh_interface ();
}

/**
 *
 */
static void signal_toolbar_menu_activate (GtkWidget * menu_item,
										  gpointer user_data)
{
	/* get selection */
	gint choice = GPOINTER_TO_INT (user_data);

	/* save back new settings */
	TOOLBAR_STYLE = choice;
	set_int_conf ("/ToolBar/Style", choice);

	/* refresh the interface for the new settings */
	refresh_interface ();
}

/**
 *
 */
static void signal_toolbar_icon_menu_activate (GtkWidget * menu_item,
											   gpointer user_data)
{
	/* get selection */
	gint choice = GPOINTER_TO_INT (user_data);

	/* save back new settings */
	TOOLBAR_SIZE = choice;
	set_int_conf ("/ToolBar/Size", choice);

	/* refresh the interface for the new settings */
	refresh_interface ();
}

/**
 *
 */
static void signal_statusbar_spinner (GtkSpinButton * spinner,
									  gpointer user_data)
{
	/* get the value out of the spin button */
	gint value = gtk_spin_button_get_value_as_int (spinner);

	/* save back new settings */
	MSGBAR_INTERVAL = value;
	set_int_conf ("/MsgBar/Interval", value);

	/* no refresh needed */
}

/**
 * 
 */
static void signal_button_font (GtkFontButton * button, gpointer user_data)
{
	/* read out the new font from the button */
	gchar *font = (gchar *) gtk_font_button_get_font_name (button);

	/* save back new settings */
	FONT = font;
	set_string_conf ("/Editor/Font", font);

	/* refresh interface */
	refresh_interface ();
}

/**
 * 
 */
static void signal_button_color (GtkColorButton * button, gpointer user_data)
{
	/* which button did we press? */
	gint choice = GPOINTER_TO_INT (user_data);

	/* get the color */
	GdkColor color;
	gtk_color_button_get_color (button, &color);

	/* set the settings according to button */
	switch (choice)
	{
	case 0:	/* FG */
		FG_RED = color.red;
		FG_GREEN = color.green;
		FG_BLUE = color.blue;
		set_int_conf ("General/Editor/FGRed", color.red);
		set_int_conf ("General/Editor/FGGreen", color.green);
		set_int_conf ("General/Editor/FGBlue", color.blue);
		break;
	case 1:	/* BG */
		BG_RED = color.red;
		BG_GREEN = color.green;
		BG_BLUE = color.blue;
		set_int_conf ("General/Editor/BGRed", color.red);
		set_int_conf ("General/Editor/BGGreen", color.green);
		set_int_conf ("General/Editor/BGBlue", color.blue);
		break;
	case 2:	/* selected FG */
		SELECTED_FG_RED = color.red;
		SELECTED_FG_GREEN = color.green;
		SELECTED_FG_BLUE = color.blue;
		set_int_conf ("General/Editor/SelectedFGRed", color.red);
		set_int_conf ("General/Editor/SelectedFGGreen", color.green);
		set_int_conf ("General/Editor/SelectedFGBlue", color.blue);
		break;
	case 3:	/* selected BG */
		SELECTED_BG_RED = color.red;
		SELECTED_BG_GREEN = color.green;
		SELECTED_BG_BLUE = color.blue;
		set_int_conf ("General/Editor/SelectedBGRed", color.red);
		set_int_conf ("General/Editor/SelectedBGGreen", color.green);
		set_int_conf ("General/Editor/SelectedBGBlue", color.blue);
		break;
	}

	/* refresh interface */
	refresh_interface ();
}

/**
 * 
 */
static void signal_button_wordwrap (GtkToggleButton * button,
									gpointer user_data)
{
	/* what is the state of the button? */
	gboolean state = gtk_toggle_button_get_active (button);

	/* save back the new settings */
	TOGGLE_WORDWRAP = state;
	set_bool_conf ("/Editor/Wordwrap", state);

	/* refresh the interface */
	refresh_interface ();
}

/**
 * 
 */
static void signal_button_marker (GtkToggleButton * button, gpointer user_data)
{
	/* what is the state of the button? */
	gboolean state = gtk_toggle_button_get_active (button);

	/* save back the new settings */
	MARKER = state;
	set_bool_conf ("/Misc/Marker", state);

	/* refresh the interface */
	refresh_interface ();
}

static void signal_button_auto_indent (GtkToggleButton * button, gpointer user_data)
{
	/* what is the state of the button? */
	gboolean state = gtk_toggle_button_get_active (button);

	/* save back the new settings */
	AUTO_INDENT = state;
	set_bool_conf ("/Editor/AutoIndent", state);

	/* refresh the interface */
	refresh_interface ();
}

static void signal_tab_width (GtkSpinButton * button, gpointer user_data)
{
	set_int_conf ("/Editor/TabWidth",
				  gtk_spin_button_get_value_as_int (button));
	refresh_interface ();
}

/** 
 * inserts tab for editor settings into the preference dialog
 * 
 * @param	settings 	the settings that should be used to display
 * 						in the preference dialog
 */
static inline gint pref_tab_editor (t_settings * settings)
{
	GtkWidget *frame = NULL;
	GtkWidget *table = NULL;
	GtkWidget *label = NULL;
	GtkWidget *button = NULL;
	GdkColor color;

	/* create a new vertical box in which we will place some
	 * frames with different settings to change */
	GtkWidget *box = gtk_vbox_new (FALSE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (box), 10);

	/* color & fonts frame. here you can set the color and
	 * font scheme for the editor. */
	frame = gtk_frame_new ("Colors and Font");
	gtk_box_pack_start (GTK_BOX (box), frame, FALSE, FALSE, 0);

	table = gtk_table_new (4, 2, FALSE);
	gtk_container_set_border_width (GTK_CONTAINER (table), 5);
	gtk_table_set_row_spacings (GTK_TABLE (table), 7);
	gtk_table_set_col_spacings (GTK_TABLE (table), 7);
	gtk_container_add (GTK_CONTAINER (frame), table);

	label = gtk_label_new_with_mnemonic ("_Font: ");
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);
	gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, 0, 1);
	button = gtk_font_button_new_with_font (settings->font);
	gtk_table_attach_defaults (GTK_TABLE (table), button, 1, 2, 0, 1);
	g_signal_connect (G_OBJECT (button), "font-set",
					  (GtkSignalFunc) signal_button_font, NULL);

	label = gtk_label_new_with_mnemonic ("Text Foreground: ");
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);
	gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, 1, 2);
	color.red = settings->fg[0];
	color.green = settings->fg[1];
	color.blue = settings->fg[2];
	button = gtk_color_button_new_with_color (&color);
	gtk_table_attach_defaults (GTK_TABLE (table), button, 1, 2, 1, 2);
	g_signal_connect (G_OBJECT (button), "color-set",
					  (GtkSignalFunc) signal_button_color, (gpointer) 0);

	label = gtk_label_new_with_mnemonic ("Text Background: ");
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);
	gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, 2, 3);
	color.red = settings->bg[0];
	color.green = settings->bg[1];
	color.blue = settings->bg[2];
	button = gtk_color_button_new_with_color (&color);
	gtk_table_attach_defaults (GTK_TABLE (table), button, 1, 2, 2, 3);
	g_signal_connect (G_OBJECT (button), "color-set",
					  (GtkSignalFunc) signal_button_color, (gpointer) 1);

	label = gtk_label_new_with_mnemonic ("Highlighted Foreground: ");
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);
	gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, 3, 4);
	color.red = settings->selected_fg[0];
	color.green = settings->selected_fg[1];
	color.blue = settings->selected_fg[2];
	button = gtk_color_button_new_with_color (&color);
	gtk_table_attach_defaults (GTK_TABLE (table), button, 1, 2, 3, 4);
	g_signal_connect (G_OBJECT (button), "color-set",
					  (GtkSignalFunc) signal_button_color, (gpointer) 2);

	label = gtk_label_new_with_mnemonic ("Highlighted Background: ");
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);
	gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, 4, 5);
	color.red = settings->selected_bg[0];
	color.green = settings->selected_bg[1];
	color.blue = settings->selected_bg[2];
	button = gtk_color_button_new_with_color (&color);
	gtk_table_attach_defaults (GTK_TABLE (table), button, 1, 2, 4, 5);
	g_signal_connect (G_OBJECT (button), "color-set",
					  (GtkSignalFunc) signal_button_color, (gpointer) 3);

	/* append a new page to the notebook */
	gtk_notebook_append_page (GTK_NOTEBOOK (pref_notebook), box,
							  gtk_label_new ("Editor"));

	/* behaviour frame. here one can set if the text should
	 * wrap or not. */
	frame = gtk_frame_new ("Behaviour");
	gtk_box_pack_start (GTK_BOX (box), frame, FALSE, FALSE, 0);

	table = gtk_table_new (4, 1, FALSE);
	gtk_container_set_border_width (GTK_CONTAINER (table), 5);
	gtk_table_set_row_spacings (GTK_TABLE (table), 7);
	gtk_table_set_col_spacings (GTK_TABLE (table), 7);
	gtk_container_add (GTK_CONTAINER (frame), table);

	button = gtk_check_button_new_with_label ("Word Wrap");
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button),
								  settings->toggle_wordwrap);
	gtk_table_attach_defaults (GTK_TABLE (table), button, 0, 1, 0, 1);
	g_signal_connect (G_OBJECT (button), "toggled",
					  (GtkSignalFunc) signal_button_wordwrap, NULL);

	button = gtk_check_button_new_with_label ("Show 80 character marker");
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button), settings->marker);
	gtk_table_attach_defaults (GTK_TABLE (table), button, 0, 1, 1, 2);
	g_signal_connect (G_OBJECT (button), "toggled",
					  (GtkSignalFunc) signal_button_marker, NULL);

	button = gtk_check_button_new_with_label ("Auto indent");
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button), settings->auto_indent);
	gtk_table_attach_defaults (GTK_TABLE (table), button, 0, 1, 2, 3);
	g_signal_connect (G_OBJECT (button), "toggled",
					  (GtkSignalFunc) signal_button_auto_indent, NULL);

	box = gtk_hbox_new (FALSE, 0);
	label = gtk_label_new ("Tab width");
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);
	button = gtk_spin_button_new_with_range (1, 8, 1);
	gtk_box_pack_start (GTK_BOX (box), label, TRUE, TRUE, 0);
	gtk_box_pack_start (GTK_BOX (box), button, TRUE, TRUE, 0);
	gtk_table_attach_defaults (GTK_TABLE (table), box, 0, 1, 3, 4);
	gtk_spin_button_set_value (GTK_SPIN_BUTTON (button), settings->tab_width);
	g_signal_connect (G_OBJECT (button), "value-changed",
					  (GtkSignalFunc) signal_tab_width, NULL);
	return 0;

}

/**
 * inserts tab for general settings into the preference dialog.
 * 
 * @param	settings	the settings that should be used to display
 * 						in the preference dialog
 */
static inline gint pref_tab_general (t_settings * settings)
{
	GtkWidget *frame = NULL;
	GtkWidget *table = NULL;
	GtkWidget *label = NULL;

	/* create a new vertical box in which we will place some
	 * frames with different settings to change */
	GtkWidget *box = gtk_vbox_new (FALSE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (box), 10);

	/* statusbar frame. here you can edit wheter the statusbar
	 * should be shown or not, and for how long a message
	 * stays in it. */
	frame = gtk_frame_new ("Statusbar");
	gtk_box_pack_start (GTK_BOX (box), frame, FALSE, FALSE, 0);

	table = gtk_table_new (1, 1, FALSE);
	gtk_container_set_border_width (GTK_CONTAINER (table), 5);
	gtk_table_set_row_spacings (GTK_TABLE (table), 7);
	gtk_table_set_col_spacings (GTK_TABLE (table), 7);
	gtk_container_add (GTK_CONTAINER (frame), table);

	gtk_table_attach_defaults (GTK_TABLE (table),
							   gtk_label_new_with_mnemonic
							   ("_Message display time (in ms)"), 0, 1, 0, 1);

	GtkWidget *adjuster =
		(GtkWidget *) gtk_adjustment_new (settings->msgbar_interval,
										  0, 9999, 1, 10, 0);
	GtkWidget *spinner = gtk_spin_button_new (GTK_ADJUSTMENT (adjuster), 1, 0);
	gtk_entry_set_max_length (GTK_ENTRY (spinner), 4);
	gtk_spin_button_set_numeric (GTK_SPIN_BUTTON (spinner), TRUE);
	g_signal_connect (G_OBJECT (spinner), "value-changed",
					  (GtkSignalFunc) signal_statusbar_spinner, NULL);
	gtk_table_attach_defaults (GTK_TABLE (table), spinner, 1, 2, 0, 1);

	/* misc. frame. here you can edit the settings for
	 * tab position and scrollbar position */
	frame = gtk_frame_new ("Miscellaneous");
	gtk_box_pack_start (GTK_BOX (box), frame, FALSE, FALSE, 0);

	table = gtk_table_new (2, 4, FALSE);
	gtk_container_set_border_width (GTK_CONTAINER (table), 5);
	gtk_table_set_row_spacings (GTK_TABLE (table), 7);
	gtk_table_set_col_spacings (GTK_TABLE (table), 7);
	gtk_container_add (GTK_CONTAINER (frame), table);

	label = gtk_label_new_with_mnemonic ("_Tabs position");
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);
	gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, 0, 1);

	GtkWidget *tab_menu = gtk_menu_new ();
	GtkWidget *tab_menu_item = gtk_menu_item_new_with_label ("Top");
	g_signal_connect (G_OBJECT (tab_menu_item), "activate",
					  (GtkSignalFunc) signal_tab_menu_activate, (gpointer) 0);
	gtk_menu_append (GTK_MENU (tab_menu), tab_menu_item);
	tab_menu_item = gtk_menu_item_new_with_label ("Bottom");
	g_signal_connect (G_OBJECT (tab_menu_item), "activate",
					  (GtkSignalFunc) signal_tab_menu_activate, (gpointer) 1);
	gtk_menu_append (GTK_MENU (tab_menu), tab_menu_item);
	tab_menu_item = gtk_menu_item_new_with_label ("Left");
	g_signal_connect (G_OBJECT (tab_menu_item), "activate",
					  (GtkSignalFunc) signal_tab_menu_activate, (gpointer) 2);
	gtk_menu_append (GTK_MENU (tab_menu), tab_menu_item);
	tab_menu_item = gtk_menu_item_new_with_label ("Right");
	g_signal_connect (G_OBJECT (tab_menu_item), "activate",
					  (GtkSignalFunc) signal_tab_menu_activate, (gpointer) 3);
	gtk_menu_append (GTK_MENU (tab_menu), tab_menu_item);

	GtkWidget *tab_options = gtk_option_menu_new ();
	gtk_option_menu_set_menu (GTK_OPTION_MENU (tab_options), tab_menu);
	gtk_option_menu_set_history (GTK_OPTION_MENU (tab_options),
								 settings->tab_position);
	gtk_table_attach_defaults (GTK_TABLE (table), tab_options, 1, 2, 0, 1);

	label = gtk_label_new_with_mnemonic ("_Scrollbar position");
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);
	gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, 1, 2);

	GtkWidget *scroll_menu = gtk_menu_new ();
	GtkWidget *scroll_menu_item = gtk_menu_item_new_with_label ("Left");
	g_signal_connect (G_OBJECT (scroll_menu_item), "activate",
					  (GtkSignalFunc) signal_scroll_menu_activate,
					  (gpointer) 1);
	gtk_menu_append (GTK_MENU (scroll_menu), scroll_menu_item);
	scroll_menu_item = gtk_menu_item_new_with_label ("Right");
	g_signal_connect (G_OBJECT (scroll_menu_item), "activate",
					  (GtkSignalFunc) signal_scroll_menu_activate,
					  (gpointer) 2);
	gtk_menu_append (GTK_MENU (scroll_menu), scroll_menu_item);

	GtkWidget *scroll_options = gtk_option_menu_new ();
	gtk_option_menu_set_menu (GTK_OPTION_MENU (scroll_options), scroll_menu);
	gtk_option_menu_set_history (GTK_OPTION_MENU (scroll_options),
								 settings->scrollbar_position - 1);
	gtk_table_attach_defaults (GTK_TABLE (table), scroll_options, 1, 2, 1, 2);

	label = gtk_label_new_with_mnemonic ("T_oolbar Style");
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);
	gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, 2, 3);

	GtkWidget *toolbar_menu = gtk_menu_new ();
	GtkWidget *toolbar_menu_item = gtk_menu_item_new_with_label ("Icons");
	g_signal_connect (G_OBJECT (toolbar_menu_item), "activate",
					  (GtkSignalFunc) signal_toolbar_menu_activate,
					  (gpointer) 0);
	gtk_menu_append (GTK_MENU (toolbar_menu), toolbar_menu_item);
	toolbar_menu_item = gtk_menu_item_new_with_label ("Text");
	g_signal_connect (G_OBJECT (toolbar_menu_item), "activate",
					  (GtkSignalFunc) signal_toolbar_menu_activate,
					  (gpointer) 1);
	gtk_menu_append (GTK_MENU (toolbar_menu), toolbar_menu_item);
	toolbar_menu_item = gtk_menu_item_new_with_label ("Both");
	g_signal_connect (G_OBJECT (toolbar_menu_item), "activate",
					  (GtkSignalFunc) signal_toolbar_menu_activate,
					  (gpointer) 2);
	gtk_menu_append (GTK_MENU (toolbar_menu), toolbar_menu_item);

	GtkWidget *toolbar_options = gtk_option_menu_new ();
	gtk_option_menu_set_menu (GTK_OPTION_MENU (toolbar_options), toolbar_menu);
	gtk_option_menu_set_history (GTK_OPTION_MENU (toolbar_options),
								 settings->toolbar_style);
	gtk_table_attach_defaults (GTK_TABLE (table), toolbar_options, 1, 2, 2, 3);

	label = gtk_label_new_with_mnemonic ("Toolbar _Icon size");
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);
	gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, 3, 4);

	toolbar_menu = gtk_menu_new ();
	toolbar_menu_item = gtk_menu_item_new_with_label ("Small");
	g_signal_connect (G_OBJECT (toolbar_menu_item), "activate",
					  (GtkSignalFunc) signal_toolbar_icon_menu_activate,
					  (gpointer) 0);
	gtk_menu_append (GTK_MENU (toolbar_menu), toolbar_menu_item);
	toolbar_menu_item = gtk_menu_item_new_with_label ("Large");
	g_signal_connect (G_OBJECT (toolbar_menu_item), "activate",
					  (GtkSignalFunc) signal_toolbar_icon_menu_activate,
					  (gpointer) 1);
	gtk_menu_append (GTK_MENU (toolbar_menu), toolbar_menu_item);

	toolbar_options = gtk_option_menu_new ();
	gtk_option_menu_set_menu (GTK_OPTION_MENU (toolbar_options), toolbar_menu);
	gtk_option_menu_set_history (GTK_OPTION_MENU (toolbar_options),
								 settings->toolbar_size);
	gtk_table_attach_defaults (GTK_TABLE (table), toolbar_options, 1, 2, 3, 4);

	/* append a new page to the notebook */
	gtk_notebook_append_page (GTK_NOTEBOOK (pref_notebook), box,
							  gtk_label_new ("General"));
	return 0;
}

/**
 * displays a preference dialog to edit settings.
 * 
 * @param	set		settings that are momentally active in the 
 * 					environment
 */
void display_prefs (t_settings * set)
{
	/* if pref window is not yet created, then create one */
	if (pref_window == NULL)
	{
		/* create new dialog window */
		pref_window = gtk_dialog_new ();
		gtk_window_set_title (GTK_WINDOW (pref_window),
							  APP_NAME " Preferences");
		gtk_window_set_policy (GTK_WINDOW (pref_window), FALSE, FALSE, FALSE);
		gtk_window_set_modal (GTK_WINDOW (pref_window), TRUE);

		/* create a button for exiting the dialog box */
		GtkWidget *button = gtk_button_new_from_stock (GTK_STOCK_CLOSE);
		gtk_box_pack_start (GTK_BOX (GTK_DIALOG (pref_window)->action_area),
							button, TRUE, TRUE, 0);

		/* create a notebook in the dialog box */
		pref_notebook = gtk_notebook_new ();
		gtk_notebook_set_homogeneous_tabs (GTK_NOTEBOOK (pref_notebook), TRUE);
		gtk_container_set_border_width (GTK_CONTAINER (pref_notebook), 10);

		/* add tabs to the notebook */
		pref_tab_general (set);
		pref_tab_editor (set);

		/* register button and notebook to the dialog window */
		gtk_box_pack_start (GTK_BOX (GTK_DIALOG (pref_window)->vbox),
							pref_notebook, TRUE, TRUE, 0);

		/* register signals to dialog box */
		g_signal_connect_swapped (G_OBJECT (pref_window), "delete-event",
								  (GtkSignalFunc) gtk_widget_hide,
								  G_OBJECT (pref_window));
		g_signal_connect_swapped (G_OBJECT (pref_window), "destroy",
								  (GtkSignalFunc) gtk_widget_hide,
								  G_OBJECT (pref_window));
		g_signal_connect_swapped (G_OBJECT (button), "clicked",
								  (GtkSignalFunc) gtk_widget_hide,
								  G_OBJECT (pref_window));
	}

	/* show the dialog */
	gtk_widget_show_all (pref_window);
	print_msg ("Display Preferences window ...");
}
