
/*
** Beaver's an Early AdVanced EditoR
** (C) 1999-2000 Marc Bevand, Damien Terrier and Emmanuel Turquin
**
** struct.h
**
** Author<s>:   Emmanuel Turquin (aka "Ender") <turqui_e@epita.fr>
**				Tobias Heinzen
**
** Description:   Beaver main structures, types definitions and macros
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef __STRUCT_H__
#define __STRUCT_H__

#include <time.h>
#include <sys/stat.h>

/*
** Here is different limits (imposed by UEdit), but they are not very
** well respected: there exists some wordfiles that overtake these
** limits, that's why I have implemented a syntax highlighting system
** that can be easily reconfigured (by changing the values defined)
**
** Especially, the values of MAXLEN_LANG_DESCRIPTION and
** MAXLEN_COL_DESCRIPTION (which was originally specified to be 8 in the
** reference documentation provided by UEdit) have been changed to 128 in
** order to support lots of wordfiles which descriptions were often more
** than 8 chars long
*/
#define MAX_LANG		10
#define MAXLEN_LANG_DESCRIPTION	128
#define MAX_COL			8
#define MAXLEN_COL_DESCRIPTION	128

/* MAXLEN_LINE_COMMENT must be a 1-digit number */
#define MAXLEN_LINE_COMMENT	3
#define MAXLEN_BLOCK_COMMENT	5

typedef enum
{
	UNIX = 0,
	MAC,
	DOS
} FormatType;

enum
{
	TypeBlockCommentOn,
	TypeBlockCommentOff,
	TypeBlockCommentOnAlt,
	TypeBlockCommentOffAlt,
	TypeLineComment,
	TypeLineCommentAlt,
	TypeString0,
	TypeString1
};

/* Type: marker */

typedef struct
{
	gint type;
	GtkTextMark *mark;
} t_marker;

/* Type: t_widget_info */

typedef struct
{
	/* undo redo stuff */
	GList *current_action;
	GList *stack;
	gboolean handlers_set;
	gboolean in_user_action;
	gboolean undoredo_toggler;
	gboolean undoredo_activated;

	/* indicates wheter this file can be edited or
	 * not. Editing includes the following operations:
	 *   - closing
	 *   - saving
	 *   - cut/paste/copy
	 *   - undo/redo */
	gboolean Editable;

	/* syntax highlighting info */
	/* this is a list of marks in the
	   buffer which indicate where the appropriate
	   tokens are */
	GList *markers;
	gint Lg;
	GtkTextTag *string0_tag;
	GtkTextTag *string1_tag;
	GtkTextTag *comment_tag;
	GtkTextTag *comment_alt_tag;
	GtkTextTag *number_tag;
	GtkTextTag *keyword_tags[MAX_COL];
	gboolean syhi_ran;

} t_widget_info;

/* Type: t_fprops */

/* This stores information related to each open file/textview */

typedef struct
{
	gchar *Name;
	gchar *BaseName;
	gchar *Type;
	FormatType Format;
	t_widget_info WidgetInfo;
	gint ReadOnly;
	time_t LastSave;
	struct stat Stats;
	GtkWidget *Text;
	GtkTextBuffer *Buffer;
} t_fprops;

/* Type: t_search_prefs */

typedef struct
{
	gchar *FileName;
	const gchar *StringToSearch;
	gboolean BeginCursorPos;
	gboolean CaseSen;
	gboolean RegExp;
	gboolean RepAll;
	gboolean RepAllBuffers;
} t_search_prefs;

/* Type: t_search_tab */

typedef struct
{
	gint Line;
	gint Begin;
	gint End;
} t_search_results;

/* Type: t_settings */

typedef struct
{
	gint recent_files;
	gboolean main_window_size_autosave;
	gint main_window_width;
	gint main_window_height;
	gboolean msgbar_display;
	gint msgbar_interval;
	gboolean toggle_wordwrap;
	gboolean toolbar_display;
	gint max_tab_label_length;
	gint tab_position;
	gint scrollbar_position;
	gint complete_window_width;
	gint complete_window_height;
	gint bg[3];
	gint fg[3];
	gint selected_bg[3];
	gint selected_fg[3];
	gboolean backup;
	gchar *backup_ext;
	gint autosave_delay;
	gchar *directory;
	gchar *font;
	gchar *print_cmd;
	gchar *wordfile;
	gboolean beep;
	gboolean syn_high;
	gint syn_high_depth;
	gboolean auto_indent;
	gboolean auto_correct;
	gint toolbar_style;
	gint toolbar_size;
	gboolean marker;
	gint tab_width;
} t_settings;

/* Type: t_colors */

typedef struct
{
	gchar lg_descript[MAX_LANG][MAXLEN_LANG_DESCRIPTION];
	gint colors[MAX_LANG][MAX_COL][3];
	gchar colors_descript[MAX_LANG][MAX_COL][MAXLEN_COL_DESCRIPTION];
	gboolean syhi_enable;
	gboolean auto_correct_enable;
	gboolean auto_indent_enable;
	gint syhi_depth;
} t_colors;

/* Type: t_conf */

typedef struct
{
	gchar **file_content;
	gint section[2];
	gchar *line;
} t_conf;

/* Some usefull macros... */

#define FPROPS(Page, Elt) g_array_index (FileProperties, t_fprops, Page).Elt

#ifdef DEBUG_FCN
#define START_FCN \
	GTimer *fcn_timer = g_timer_new ();\
	g_print(__FILE__": %s(): Begin\n", __func__);
#else
#define START_FCN
#endif

#ifdef DEBUG_FCN
#define END_FCN \
	g_print(__FILE__": %s(): End after %f seconds\n", __func__, \
		g_timer_elapsed (fcn_timer, NULL));\
	g_timer_destroy (fcn_timer);
#else
#define END_FCN
#endif

#endif
