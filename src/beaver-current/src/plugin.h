
/*
** Beaver's an Early AdVanced EditoR
** (C) 1999-2000 Marc Bevand, Damien Terrier and Emmanuel Turquin, 2008 Tobias Heinzen, Double 12
**
** plugin.h
**
** Author<s>:   Tobias Heinzen
** Description:   Plugins system
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef __PLUGIN_H_
#define __PLUGIN_H_

/**
 * Structure holding information about plugin
 * 
 * @param   name        name of plugin
 * @param   description short description of plugin
 * @param   author      author of plugin
 * @param   version     version of plugin
 * @param   filename    filename of plugin, path is absolute
 * @param   activated       wheter or not the plugin is active
 * @param   module          reference to a GModule (only when activated)
 * @param   cleanup_func    the function pointer to the cleanup function (only when activated)
 * @param   preference_func the function pointer to the preference function (only when activated);
 */
struct plugin_info_s
{
	gchar *name;
	gchar *description;
	gchar *author;
	gchar *version;
	gchar *filename;

	/* stores information when plugin is
	 * activated */
	gboolean activated;
	GModule *module;
	void *cleanup_func;
	void *preference_func;
};
typedef struct plugin_info_s plugin_info;

/* defines function pointers that are used to load
 * these functions from the plugin */
typedef gchar *(*__plugin_name_func) (void);
typedef gchar *(*__plugin_description_func) (void);
typedef gchar *(*__plugin_author_func) (void);
typedef gchar *(*__plugin_version_func) (void);
typedef void (*__plugin_init_func) (void);
typedef void (*__plugin_cleanup_func) (void);
typedef void (*__plugin_preference_func) (void);

/* PROTOTYPES */
void plugin_init (void);

#endif							/* __PLUGIN_H_ */
