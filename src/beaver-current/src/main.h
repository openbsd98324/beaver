
/*
** Beaver's an Early AdVanced EditoR
** (C) 1999-2000 Marc Bevand, Damien Terrier and Emmanuel Turquin, 2008 Tobias Heinzen, Double 12
**
** main.h
**
** Author<s>:   Emmanuel Turquin (aka "Ender") <turqui_e@epita.fr>
**				Tobias Heinzen
** 				Double 12
**
** Description: Beaver main source file header
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef __MAIN_H__
#define __MAIN_H__

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifdef WIN32
#define PATH_SEP '\\'
#define PATH_SEP_STRING "\\"
#define TEMP_DIR "c:\\windows\\temp\\"
#define TEMP_PREFIX "buffer_"
#else
#define PATH_SEP '/'
#define PATH_SEP_STRING "/"
#define TEMP_DIR "/tmp/"
#define TEMP_PREFIX "buffer_"
#endif

#define APP_NAME        "Beaver"
#define APP_MOTTO       "Beaver's an Early AdVanced EditoR"
#define APP_URL	        "http://beaver-editor.sourceforge.net/"
#define TRANSLATORS		"(none yet)"
#define VERSION_NUMBER  PACKAGE_VERSION
#define UNTITLED        "Untitled"
#define WELCOME_MSG     "Welcome to"
#define UNKNOWN         "???"
#define NO_SYHI		   "None"


int main (int argc, char *argv[]);

#endif
