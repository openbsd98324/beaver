
/*
** Beaver's an Early AdVanced EditoR
** (C) 1999-2000 Marc Bevand, Damien Terrier and Emmanuel Turquin, 2008 Tobias Heinzen, Double 12
**
** undoredo.c
**
** Author<s>:   Marc Bevand (aka "After") <bevand_m@epita.fr>
**              Michael Terry <mterry@fastmail.fm>
** 				Tobias Heinzen
** Description: Provide Undo/Redo function
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <gtk/gtk.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include "editor.h"
#include "struct.h"
#include "msgbar.h"
#include "search.h"
#include "prefs.h"
#include "interface.h"
#include "undoredo.h"


//#define DEBUG_UNDOREDO


extern GtkWidget *MainNotebook;
extern GArray *FileProperties;
extern t_settings Settings;

void free_undoredo()
{
	gint CurrentPage;

	CurrentPage = gtk_notebook_get_current_page (GTK_NOTEBOOK (MainNotebook));

	GList *stack = FPROPS (CurrentPage, WidgetInfo.stack),
				*el;
	for (el = stack; el; el = el->next) {
		t_action *a = el->data;
		g_free(a->text);
		g_free(a);
	}
	g_list_free(stack);
}

void init_undoredo (void)
{
	gint CurrentPage;
	GtkTextBuffer *Buffer;

	CurrentPage = gtk_notebook_get_current_page (GTK_NOTEBOOK (MainNotebook));

	if (!FPROPS(CurrentPage, WidgetInfo.handlers_set) && CurrentPage != -1)
	{	/* wrong! only one buffer has signals connected. change that!! */
		Buffer = FPROPS (CurrentPage, Buffer);
		g_signal_connect (G_OBJECT (Buffer), "begin-user-action",
						  G_CALLBACK (undoredo_start_action),
						  GINT_TO_POINTER (CurrentPage));
		g_signal_connect (G_OBJECT (Buffer), "end-user-action",
						  G_CALLBACK (undoredo_end_action),
						  GINT_TO_POINTER (CurrentPage));
		g_signal_connect (G_OBJECT (Buffer), "delete-range",
						  G_CALLBACK (undoredo_on_delete),
						  GINT_TO_POINTER (CurrentPage));
		g_signal_connect (G_OBJECT (Buffer), "insert-text",
						  G_CALLBACK (undoredo_on_insert),
						  GINT_TO_POINTER (CurrentPage));

		FPROPS(CurrentPage, WidgetInfo.handlers_set) = TRUE;
	}
	//note_redoundo ();
}

void new_action (void)
{
	t_action *action;

	action = g_malloc (sizeof (t_action));
	action->text = NULL;
	action->start = -1;

	record_action (action);
}

void undoredo_start_action (GtkTextBuffer * Buffer, gpointer data)
{
	gint page = GPOINTER_TO_INT (data);

	START_FCN FPROPS (page, WidgetInfo.in_user_action) = TRUE;
	FPROPS (page, WidgetInfo.undoredo_toggler) =
		FPROPS (page, WidgetInfo.undoredo_toggler) ? FALSE : TRUE;

END_FCN}

void undoredo_end_action (GtkTextBuffer * Buffer, gpointer data)
{
	gint page = GPOINTER_TO_INT (data);

	START_FCN FPROPS (page, WidgetInfo.in_user_action) = FALSE;

END_FCN}

void undoredo_on_insert (GtkTextBuffer * Buffer, GtkTextIter * start_in,
						 gchar * text, gint size, gpointer data)
{
	gint page = GPOINTER_TO_INT (data);

	START_FCN if (FPROPS (page, WidgetInfo.in_user_action))
	{
		t_action *action;
		GList *current_action;

		new_action ();

		current_action = FPROPS (page, WidgetInfo.current_action);
		action = (t_action *) current_action->data;

		action->type = insert;
		action->start = gtk_text_iter_get_offset (start_in);
		action->end = action->start + size;
		action->text = g_strdup (text);
		action->toggle_state = FPROPS (page, WidgetInfo.undoredo_toggler);
	}

END_FCN}

void undoredo_on_delete (GtkTextBuffer * Buffer, GtkTextIter * start_in,
						 GtkTextIter * end_in, gpointer data)
{
	START_FCN
	gint page = GPOINTER_TO_INT (data);

	if (FPROPS (page, WidgetInfo.in_user_action))
	{
#ifdef DEBUG_UNDOREDO
		g_print (__FILE__ ": %s in_user_action = TRUE\n", __FUNCTION__);
#endif
		t_action *action;
		gint start, end;
		gchar *text;
		GList *current_action;

		new_action ();

		current_action = FPROPS (page, WidgetInfo.current_action);
		action = (t_action *) current_action->data;

		start = gtk_text_iter_get_offset (start_in);
		end = gtk_text_iter_get_offset (end_in);
		text = gtk_text_iter_get_text (start_in, end_in);

		action->type = delete;
		action->start = start;
		action->end = end;
		action->text = text;
		action->toggle_state = FPROPS (page, WidgetInfo.undoredo_toggler);
	}

	END_FCN
}

void record_action (t_action * action)
{
	GList *current_action;
	GList *first_action;
	gint position;
	gint last_action_index;
	gint i;
	gint page;

#ifdef DEBUG_FCN
	g_print (__FILE__ ": %s(): Begin\n", __func__);
#endif
	page = gtk_notebook_get_current_page (GTK_NOTEBOOK (MainNotebook));

	if (!FPROPS (page, WidgetInfo.undoredo_activated))
		return;

	if (!FPROPS (page, WidgetInfo.current_action))
	{
		position = -1;
		current_action = NULL;
		first_action = FPROPS (page, WidgetInfo.stack);
		last_action_index = (gint) g_list_length (first_action) - 1;
	}
	else
	{
		current_action = FPROPS (page, WidgetInfo.current_action);
		first_action = FPROPS (page, WidgetInfo.stack);
		position = g_list_position (first_action, current_action);
		last_action_index = (gint) g_list_length (first_action) - 1;
	}
	/*
	 ** If the user has just undone some actions, these can be redone
	 ** (they are saved in a list). But if he has manually done another
	 ** action (and this is the case: the current function has been
	 ** called) we must free the saved actions, because they are lost
	 ** forever. This can seem complicated, but it is exactly the same
	 ** behaviour that the 'next' and 'back' button in any browser.
	 ** -- After
	 */
	for (i = last_action_index; i > position; i--)
	{
		gpointer data = g_list_nth_data (first_action, i);
		first_action = g_list_remove (first_action, data);
		g_free (data);
	}

	if (position < last_action_index && current_action)
		FPROPS (page, WidgetInfo.undoredo_toggler) =
			!((t_action *) current_action->data)->toggle_state;

	current_action = g_list_last (first_action =
								  g_list_append (first_action, action));

	FPROPS (page, WidgetInfo.stack) = first_action;
	FPROPS (page, WidgetInfo.current_action) = current_action;

	//note_redoundo ();
#ifdef DEBUG_FCN
	g_print (__FILE__ ": %s(): End\n", __func__);
#endif
}

gboolean undo_is_possible (void)
{
	GList *current_action;
	gboolean rv;
	gint page;

	START_FCN page =
		gtk_notebook_get_current_page (GTK_NOTEBOOK (MainNotebook));

	if (page < 0 || !FPROPS (page, WidgetInfo.undoredo_activated))
		return FALSE;

	current_action = FPROPS (page, WidgetInfo.current_action);

	if (current_action && current_action->data)
		rv = TRUE;
	else
		rv = FALSE;
	END_FCN return rv;
}

gboolean redo_is_possible (void)
{
	GList *current_action;
	gboolean rv;
	gint page;
	START_FCN page =
		gtk_notebook_get_current_page (GTK_NOTEBOOK (MainNotebook));

	if (page < 0 || !FPROPS (page, WidgetInfo.undoredo_activated))
		return FALSE;

	current_action = FPROPS (page, WidgetInfo.current_action);

	if (current_action && current_action->next && current_action->next->data)
		rv = TRUE;
	else if (!current_action && FPROPS (page, WidgetInfo.stack)
			 && FPROPS (page, WidgetInfo.stack)->data)
		rv = TRUE;
	else
		rv = FALSE;
	END_FCN return rv;
}

void proceed_redo (void)
{
	GList **current_action;
	gint CurrentPage;
	GtkTextBuffer *Buffer;

	START_FCN CurrentPage =
		gtk_notebook_get_current_page (GTK_NOTEBOOK (MainNotebook));

	if (!FPROPS (CurrentPage, WidgetInfo.undoredo_activated))
		return;

	current_action = &FPROPS (CurrentPage, WidgetInfo.current_action);
	Buffer = FPROPS (CurrentPage, Buffer);
	if (redo_is_possible ())
	{
		gboolean my_toggle_state;

		if (!*current_action)
			*current_action = FPROPS (CurrentPage, WidgetInfo.stack);
		else
			*current_action = (*current_action)->next;

		my_toggle_state = ((t_action *) (*current_action)->data)->toggle_state;

		switch (((t_action *) (*current_action)->data)->type)
		{
		case insert:
			{
				gint position;
				GtkTextIter iter;

				position = ((t_action *) (*current_action)->data)->start;
				gtk_text_buffer_get_iter_at_offset (Buffer, &iter, position);
				g_signal_handlers_block_matched (G_OBJECT (Buffer),
												 G_SIGNAL_MATCH_DETAIL |
												 G_SIGNAL_MATCH_DATA, 0,
												 g_quark_from_static_string
												 ("insert-text"), NULL, NULL,
												 GINT_TO_POINTER (CurrentPage));
				gtk_text_buffer_insert (Buffer, &iter,
										((t_action *) (*current_action)->data)->
										text,
										((t_action *) (*current_action)->data)->
										end -
										((t_action *) (*current_action)->data)->
										start);
				g_signal_handlers_unblock_matched (G_OBJECT (Buffer),
												   G_SIGNAL_MATCH_DETAIL |
												   G_SIGNAL_MATCH_DATA, 0,
												   g_quark_from_static_string
												   ("insert-text"), NULL, NULL,
												   GINT_TO_POINTER
												   (CurrentPage));
				//show_on_screen (GTK_TEXT_VIEW (FPROPS(CurrentPage, Text)), iter);
				break;
			}
		case delete:
			{
				GtkTextIter start, end;
				gtk_text_buffer_get_iter_at_offset (Buffer, &start,
													((t_action
													  *) (*current_action)->
													 data)->start);
				gtk_text_buffer_get_iter_at_offset (Buffer, &end,
													((t_action
													  *) (*current_action)->
													 data)->end);
				g_signal_handlers_block_matched (G_OBJECT (Buffer),
												 G_SIGNAL_MATCH_DETAIL |
												 G_SIGNAL_MATCH_DATA, 0,
												 g_quark_from_static_string
												 ("delete-range"), NULL, NULL,
												 GINT_TO_POINTER (CurrentPage));
				gtk_text_buffer_delete (Buffer, &start, &end);
				g_signal_handlers_unblock_matched (G_OBJECT (Buffer),
												   G_SIGNAL_MATCH_DETAIL |
												   G_SIGNAL_MATCH_DATA, 0,
												   g_quark_from_static_string
												   ("delete-range"), NULL, NULL,
												   GINT_TO_POINTER
												   (CurrentPage));
				//show_on_screen (GTK_TEXT_VIEW (FPROPS(CurrentPage, Text)), start);
				break;
			}
		}

		/* if the new current action has the same toggle state, then
		   keep redoing */
		if (*current_action && (*current_action)->next
			&& (*current_action)->next->data
			&& ((t_action *) (*current_action)->next->data)->toggle_state ==
			my_toggle_state)
		{
			proceed_redo ();
		}
		print_msg ("Redo...");
	}
	else
	{
		if (BEEP)
			gdk_beep ();
		print_msg ("Nothing to redo !");
	}


	//note_redoundo ();

END_FCN}

void proceed_undo (void)
{
	GList **current_action;
	GtkTextBuffer *Buffer;
	gint CurrentPage;

	START_FCN CurrentPage =
		gtk_notebook_get_current_page (GTK_NOTEBOOK (MainNotebook));

	if (!FPROPS (CurrentPage, WidgetInfo.undoredo_activated))
		return;

	current_action = &FPROPS (CurrentPage, WidgetInfo.current_action);
	Buffer =
		FPROPS (gtk_notebook_get_current_page (GTK_NOTEBOOK (MainNotebook)),
				Buffer);
	if (undo_is_possible ())
	{
		gboolean my_toggle_state;
		my_toggle_state = ((t_action *) (*current_action)->data)->toggle_state;

		switch (((t_action *) (*current_action)->data)->type)
		{
		case insert:
			{
				GtkTextIter start, end;
				gtk_text_buffer_get_iter_at_offset (Buffer, &start,
													((t_action
													  *) (*current_action)->
													 data)->start);
				gtk_text_buffer_get_iter_at_offset (Buffer, &end,
													((t_action
													  *) (*current_action)->
													 data)->end);
				g_signal_handlers_block_matched (G_OBJECT (Buffer),
												 G_SIGNAL_MATCH_DETAIL |
												 G_SIGNAL_MATCH_DATA, 0,
												 g_quark_from_static_string
												 ("delete-range"), NULL, NULL,
												 GINT_TO_POINTER (CurrentPage));
				gtk_text_buffer_delete (Buffer, &start, &end);
				g_signal_handlers_unblock_matched (G_OBJECT (Buffer),
												   G_SIGNAL_MATCH_DETAIL |
												   G_SIGNAL_MATCH_DATA, 0,
												   g_quark_from_static_string
												   ("delete-range"), NULL, NULL,
												   GINT_TO_POINTER
												   (CurrentPage));
				//show_on_screen (GTK_TEXT_VIEW (FPROPS(CurrentPage, Text)), start);
				break;
			}
		case delete:
			{
				gint position;
				GtkTextIter iter;

				position = ((t_action *) (*current_action)->data)->start;
				gtk_text_buffer_get_iter_at_offset (Buffer, &iter, position);
				g_signal_handlers_block_matched (G_OBJECT (Buffer),
												 G_SIGNAL_MATCH_DETAIL |
												 G_SIGNAL_MATCH_DATA, 0,
												 g_quark_from_static_string
												 ("insert-text"), NULL, NULL,
												 GINT_TO_POINTER (CurrentPage));
				gtk_text_buffer_insert (Buffer, &iter,
										((t_action *) (*current_action)->data)->
										text,
										((t_action *) (*current_action)->data)->
										end -
										((t_action *) (*current_action)->data)->
										start);
				g_signal_handlers_unblock_matched (G_OBJECT (Buffer),
												   G_SIGNAL_MATCH_DETAIL |
												   G_SIGNAL_MATCH_DATA, 0,
												   g_quark_from_static_string
												   ("insert-text"), NULL, NULL,
												   GINT_TO_POINTER
												   (CurrentPage));
				//show_on_screen (GTK_TEXT_VIEW (FPROPS(CurrentPage, Text)), iter);
				break;
			}
		}
		*current_action = (*current_action)->prev;

		/* if the new current action has the same toggle state, then
		   keep redoing */
		if (*current_action && (*current_action)->data
			&& ((t_action *) (*current_action)->data)->toggle_state ==
			my_toggle_state)
		{
			proceed_undo ();
		}

		print_msg ("Undo...");
	}
	else
	{
		if (BEEP)
			gdk_beep ();
		print_msg ("Nothing to undo !");
	}

	//note_redoundo ();

END_FCN}

/*
** This function is only for debugging purpose.
*/
void view_undoredo_list (gchar * prefix)
{
	GList **current_action;
	GList *action;

	current_action =
		&FPROPS (gtk_notebook_get_current_page (GTK_NOTEBOOK (MainNotebook)),
				 WidgetInfo.current_action);
	action = g_list_first (*current_action);
	g_print ("%s: **** Start of undoredo list\n", prefix);
	while (action)
	{
		if (action->data)
		{
			g_print ("%s: %s in [%i; %i] of \"%s\"",
					 prefix,
					 ((t_action *) action->data)->type == insert ?
					 "Insertion" : "Deletion",
					 ((t_action *) action->data)->start,
					 ((t_action *) action->data)->end,
					 ((t_action *) action->data)->text);
		}
		else
			g_print ("%s: Data == NULL: The 1st null `action\'", prefix);
		if (action == *current_action)
			g_print (" <== This action is the previous\n");
		else
			g_print ("\n");
		action = action->next;
	}
	g_print ("%s: **** End of undoredo list\n", prefix);
}
