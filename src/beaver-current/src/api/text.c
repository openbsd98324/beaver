
/*
** Beaver's an Early AdVanced EditoR
** (C) 1999-2000 Marc Bevand, Damien Terrier and Emmanuel Turquin, 
** (C) 2008 Tobias Heinzen, Double 12
** (C) 2008 Higor Eurípedes
**
** text.c
**
** Author<s>:   Tobias Heinzen, Higor Eurípedes
** Description:   Text manipulation
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <glib.h>
#include <gtk/gtk.h>
#include <string.h>

#include "../struct.h"
#include "../search.h"
#include "beaver.h"
#include "api.h"

/* the main notebook (from interface.c) */
extern GtkWidget *MainNotebook;

/* how many files are open (from interface.c) */
extern gint OpenedFilesCnt;

/* file properties (from interface.c) */
extern GArray *FileProperties;

/** @file text.c
 * Implementation for text manipulations for plugins.
 */

/**
 * Insert text at current cursos position. If no text/cursor position
 * is avaible then ignore insert. Also if text is read-only, ignore
 * the call.
 *
 * @param   text    string to be inserted
 */
void beaver_text_insert_string_impl (const gchar * text)
{
	/* get the current page that should be edited */
	gint current_page =
		gtk_notebook_get_current_page (GTK_NOTEBOOK (MainNotebook));

	/* if no page is open (ie. no files are open) or
	 * the file is read-only, then abort */
	if (!(OpenedFilesCnt >= 0) || FPROPS (current_page, ReadOnly))
	{
		return;
	}

	/* get the buffer to write into */
	GtkTextBuffer *buffer = FPROPS (current_page, Buffer);

	/* insert the text */
	gtk_text_buffer_insert_at_cursor (buffer, text, strlen (text));
}

/**
 * @returns returns a structure with information about the position of the
 *          selection. structure must be freed after use with g_free. returns
 *          NULL if information could not be load.
 */
BeaverTextSelection *beaver_text_selection_position_impl (void)
{
	/* get the current page that should be edited */
	gint current_page =
		gtk_notebook_get_current_page (GTK_NOTEBOOK (MainNotebook));

	/* if no page is avaiable, so no text is avaiable,
	 * therefore return NULL */
	if (!(OpenedFilesCnt >= 0))
	{
		return NULL;
	}

	/* get the text buffer */
	GtkTextBuffer *buffer = FPROPS (current_page, Buffer);

	/* if no selection is avaiable, then return NULL */
	if (!gtk_text_buffer_get_has_selection (buffer))
	{
		return NULL;
	}

	/* try to load cursors from selection */
	GtkTextIter start, end;
	gtk_text_buffer_get_selection_bounds (buffer, &start, &end);

	/* new object to return */
	BeaverTextSelection *result = g_malloc (sizeof (BeaverTextSelection));
	result->start = gtk_text_iter_get_line_offset (&start) + 1;
	result->end = gtk_text_iter_get_line_offset (&end) + 1;
	result->line = gtk_text_iter_get_line (&start) + 1;
	result->offset = gtk_text_iter_get_offset (&start) + 1;

	return result;
}

/**
 * Get the text that was selected. Returns NULL if no text was
 * selected or if no text is avaiable. The returned string
 * should be freed after usage.
 *
 * @returns  selected text. should be freed after usage.
 */
gchar *beaver_text_selection_get_impl (void)
{
	/* get the current page that should be edited */
	gint current_page =
		gtk_notebook_get_current_page (GTK_NOTEBOOK (MainNotebook));

	/* if no page is avaiable, so no text is avaiable,
	 * therefore return NULL */
	if (!(OpenedFilesCnt >= 0))
	{
		return NULL;
	}

	/* get the text buffer */
	GtkTextBuffer *buffer = FPROPS (current_page, Buffer);

	/* if no selection is avaiable, then return NULL */
	if (!gtk_text_buffer_get_has_selection (buffer))
	{
		return NULL;
	}

	/* try to load cursors from selection */
	GtkTextIter start, end;
	gtk_text_buffer_get_selection_bounds (buffer, &start, &end);

	/* return selection text */
	return gtk_text_buffer_get_text (buffer, &start, &end, FALSE);
}

/**
 * Overwrites the selection with the given text. If no text
 * is selected then just insert the text at the current cursor
 * position. If no text is avaiable or text is read-only
 * then ignore the call.
 *
 * @param   text    the text to be inserted into the selection
 */
void beaver_text_selection_set_impl (const gchar * text)
{
	/* get the current page that should be edited */
	gint current_page =
		gtk_notebook_get_current_page (GTK_NOTEBOOK (MainNotebook));

	/* if no page is open (ie. no files are open) or
	 * the file is read-only, then abort */
	if (!(OpenedFilesCnt >= 0) || FPROPS (current_page, ReadOnly))
	{
		return;
	}

	/* get the buffer to write into */
	GtkTextBuffer *buffer = FPROPS (current_page, Buffer);

	/* if there's a selection, remove it, and set the cursor
	 * right, so if we then insert the text, it has the feeling
	 * as if we overwritten the selection. */
	if (gtk_text_buffer_get_has_selection (buffer))
	{
		GtkTextIter start, end;
		gtk_text_buffer_get_selection_bounds (buffer, &start, &end);
		gtk_text_buffer_delete (buffer, &start, &end);
	}

	/* insert the text at the current cursor position. */
	beaver_text_insert_string (text);
}

/**
 * Returns the format of the text. The format revers to the
 * line-break type of the different OS's.
 *
 * @retval  0   UNIX line-break
 * @retval  2   DOS line-break
 * @retval  1   MAC line-break
 * @retval -1   Unknown or error
 */
enum BeaverTextFormat beaver_text_format_impl (void)
{
	/* get the current page that should be edited */
	gint current_page =
		gtk_notebook_get_current_page (GTK_NOTEBOOK (MainNotebook));

	/* if no page is open (ie. no files are open) then abort */
	if (!(OpenedFilesCnt >= 0))
	{
		return -1;
	}

	/* get the text out of the current page */
	GtkTextIter start, end;
	gtk_text_buffer_get_bounds (FPROPS (current_page, Buffer), &start, &end);
	gchar *string = gtk_text_buffer_get_slice (FPROPS (current_page, Buffer),
											   &start, &end, FALSE);

	/* try to determine the type */
	gint rv;
	if (strstr (string, "\r\n"))
		rv = BEAVER_FORMAT_DOS;
	else if (strchr (string, '\r'))
		rv = BEAVER_FORMAT_MAC;
	else
		rv = BEAVER_FORMAT_UNIX;	/* default is UNIX */

	/* free up space */
	g_free (string);

	return rv;
}

/**
 * Searches for needle in the text and replaces
 * it with subsitute. Replaces all occurences of
 * needle.
 *
 * @param   needle      the search string to replace
 * @param   substitute  the replacement of found needle
 * @param   sensitive   wheter or not to search case sensitive
 */
void beaver_text_replace_impl (const gchar * needle, const gchar * substitute,
							   gboolean sensitive)
{
	/* get the current page that should be edited */
	gint current_page =
		gtk_notebook_get_current_page (GTK_NOTEBOOK (MainNotebook));

	/* if no page is open (ie. no files are open) or
	 * the file is read-only, then abort */
	if (!(OpenedFilesCnt >= 0) || FPROPS (current_page, ReadOnly))
	{
		return;
	}

	/* search for the needle. if found then replace it with substitute
	 * and continue searching until nothing can be found */
	guint m = strlen (needle);
	guint offset = 0;
	while (beaver_text_find (needle, offset, sensitive))
	{
		/* try to load cursors from selection */
		GtkTextIter start, end;
		gtk_text_buffer_get_selection_bounds (FPROPS (current_page, Buffer),
											  &start, &end);

		/* move the offset */
		offset = gtk_text_iter_get_offset (&start) + m + 1;

		/* replace selection */
		beaver_text_selection_set (substitute);
	}

	/* the replacement could have changed something
	 * about the format, so try to get it once more. */
	FPROPS (current_page, Format) = beaver_text_format ();
}

/* SEARCH HELPER FUNCTIONS */

/* define the size of the alphabet */
#define SEARCH_ALPHABET_SIZE    256

/**
 * Calculates the shift table for the "bad character" strategy
 * of boyer-moore. Helper function for beaver_text_find_impl.
 *
 * @param   needle  the search string, for which to prepare the table
 * @param   table   a pointer to preallocated memory, where to put in
 *                  the table
 */
static inline void occurence (const gchar * needle, gint * table)
{
	/* get the size of the needle */
	gint len = strlen (needle);

	/* set all shift length to the size of the
	 * needle. */
	gint i;
	for (i = 0; i < SEARCH_ALPHABET_SIZE; i++)
	{
		table[i] = len;
	}

	/* now set the real shift length for all the
	 * characters that are in the needle. the value to
	 * insert here is the occurence of said character
	 * from the end of the text (or the last occurence
	 * in the needle). this value is 'len-i-1'. because we
	 * traverse the whole word this is always true. if we
	 * encounter the same character later, the value for
	 * this character will be replaced with the correct
	 * value.
	 */
	for (i = 0; i < len - 1; i++)
	{
		table[(gint) needle[i]] = len - i - 1;
	}
}

/* SEARCH HELPER FUNCTIONS (END) */

/**
 * Searches for a needle in the text. When found the text will be
 * selected at the position and TRUE will be returned. If nothing
 * was found, then nothing will be selected and FALSE will
 * be returned. The selected text can be altered via
 * beaver_text_selection_get/set.
 *
 * The search is done via Boyer-Moore-Horspool's Algorithm. See
 * http://www-igm.univ-mlv.fr/~lecroq/string/node18.html
 * for more explanation about it.
 *
 * @param   needle      the string to search for
 * @param   offset      where in the text to begin to search (0 for beginning
 *                      of text)
 * @param   sensitive   wheter or not to search case sensitive
 * @retval  true    needle was found in text
 * @retval  false   otherwise
 */
gboolean beaver_text_find_impl (const gchar * needle, guint offset,
								gboolean sensitive)
{
	/* get the current page that should be edited */
	gint current_page =
		gtk_notebook_get_current_page (GTK_NOTEBOOK (MainNotebook));

	/* if no page is open (ie. no files are open) then abort */
	if (!(OpenedFilesCnt >= 0))
	{
		return FALSE;
	}

	/* get the text out of the current page */
	GtkTextIter start, end;
	gtk_text_buffer_get_bounds (FPROPS (current_page, Buffer), &start, &end);
	gchar *text = gtk_text_buffer_get_slice (FPROPS (current_page, Buffer),
											 &start, &end, FALSE);

	/* if there's no text avaiable so we can stop
	 * here. we will not find anything */
	if (text == NULL)
	{
		return FALSE;
	}

	/* preprocess */
	gint occurence_table[SEARCH_ALPHABET_SIZE];
	occurence (needle, occurence_table);

	/* the result */
	gboolean result = FALSE;

	/* search */
	gint i = offset;			/* start at given offset */
	gint m = strlen (needle);	/* length of the needle */
	gint n = gtk_text_iter_get_offset (&end);	/* length of the text */
	if (i > n)
		return FALSE;
	while ((gint) i <= (gint) n - m)
	{
		gchar c = text[i + m - 1];
		if (needle[m - 1] == c &&	/* compare last character of the needle
									   with the text character */
			/* compare the rest of the pattern. either search case sensitive
			 * or not */
			((sensitive && memcmp (needle, text + i, m - 1) == 0) ||
			 (!sensitive
			  && g_ascii_strncasecmp (needle, text + i, m - 1) == 0)))
		{

			/* select the occurence */
			gtk_text_iter_set_offset (&start, i);
			gtk_text_iter_set_offset (&end, i + m);
			gtk_text_buffer_select_range (FPROPS (current_page, Buffer), &start,
										  &end);

			/* mark result as TRUE */
			result = TRUE;

			/* now we can exit the loop */
			break;
		}

		/* there was a mismatch. we always shift
		 * according to the last character of the
		 * compared text with the bad character table */
		i += occurence_table[(gint) c];
	}

	/* free up space */
	g_free (text);

	return result;
}

#ifdef REGEX_SUPPORT

/**
 * Tests the given pattern againts the text. If the function find something
 * it returns a BeaverRegexResult which holds offsets for each result.
 *
 * The pattern testing is done using glib's internal pcre-compatible functions.
 *
 * @param   pattern     the perl-compatible pattern to be tested against the current document
 * @param   offset      where in the text to begin to search (0 for beginning
 *                      of text)
 * @param   sensitive   wheter or not to search case sensitive
 * @returns             NULL if nothing is found or a pointer to the regex results structure
 *                      (dont forget to free it when you dont need it anymore).
 */
BeaverRegexResult *beaver_text_find_regex_impl (const gchar * pattern,
												guint offset,
												gboolean sensitive)
{
	/* get the current page that should be edited */
	gint current_page =
		gtk_notebook_get_current_page (GTK_NOTEBOOK (MainNotebook));

	/* stop if theres no open file */
	if (!(OpenedFilesCnt >= 0))
	{
		return NULL;
	}

	/* get the text out of the current page */
	GtkTextIter start, end;

	gtk_text_buffer_get_end_iter (FPROPS (current_page, Buffer), &end);

	/* if offset doesnt exist, we stop here */
	if (offset > gtk_text_iter_get_offset (&end))
		return NULL;

	gtk_text_buffer_get_iter_at_offset (FPROPS (current_page, Buffer), &start,
										offset);

	gchar *text = gtk_text_buffer_get_slice (FPROPS (current_page, Buffer),
											 &start, &end, FALSE);

	/* theres no text, so we stop */
	if (text == NULL)
	{
		return NULL;
	}

	GRegex *regex;
	GError *error = NULL;

	/* pattern compiling options. multiline is default. */
	guint options = G_REGEX_MULTILINE + (sensitive ? 0 : G_REGEX_CASELESS);

	GtkTextIter *iter;

	/* this structure holds information about the results */
	GMatchInfo *matchinf;

	/* the results (doh!) */
	BeaverRegexResult *result;

	/* compiles the pattern and creates a 'instance' of the GRegex structure */
	regex = g_regex_new (pattern, options, 0, &error);

	/* if some error has happened, free the used resources and stop */
	if (error)
	{
		/* Show some message to the user, the line bellow is just for debugging */
		g_print ("g_regex_new() error: %s\n", error->message);

		g_free (regex);
		g_free (text);
		return NULL;
	}

	/* if no match is found, stop. do not allow empty matches */
	if (!g_regex_match (regex, text, G_REGEX_MATCH_NOTEMPTY, &matchinf))
	{
		g_free (regex);
		g_free (text);
		return NULL;
	}

	/* initializes the result */
	result = g_new (BeaverRegexResult, 1);
	result->results = g_new (BeaverTextSelection, 1);
	result->count = 0;

	iter = g_new (GtkTextIter, 1);

	gint i = 0, start_o, end_o;

	/* loop while there are matches left */
	while (g_match_info_matches (matchinf))
	{
		result->results = g_renew (BeaverTextSelection, result->results, i + 1);
		g_match_info_fetch_pos (matchinf, 0, &start_o, &end_o);

		gtk_text_buffer_get_iter_at_offset (FPROPS (current_page, Buffer), iter,
											start_o);

		result->results[i].line = gtk_text_iter_get_line (iter) + 1;
		result->results[i].start = gtk_text_iter_get_line_offset (iter);

		gtk_text_buffer_get_iter_at_offset (FPROPS (current_page, Buffer), iter,
											end_o);

		result->results[i].end = gtk_text_iter_get_line_offset (iter);
		result->results[i].offset = start_o + 1;

		g_match_info_next (matchinf, NULL);
		i++;
	}

	/* free the used resources */
	g_free (text);
	g_free (matchinf);
	g_free (regex);
	g_free (iter);

	result->count = i;

	return (!result->count ? NULL : result);
}

/**
 * Searches for needle in the text and replaces
 * it with subsitute. Replaces all occurences of
 * needle.
 *
 * @param   needle      the search pattern
 * @param   substitute  the replacement of found matches
 * @param   sensitive   wheter or not to search case sensitive
 * @param   all         wheter or not replace all
 */
void beaver_text_replace_regex_impl (const gchar * pattern,
									 const gchar * substitute,
									 gboolean sensitive, gboolean all)
{
	/* get the current page that should be edited */
	gint current_page =
		gtk_notebook_get_current_page (GTK_NOTEBOOK (MainNotebook));

	/* no open file or file is read-only */
	if (!(OpenedFilesCnt >= 0) || FPROPS (current_page, ReadOnly))
	{
		return;
	}

	/* get the text out of the current page */
	GtkTextIter start, end;

	gtk_text_buffer_get_bounds (FPROPS (current_page, Buffer), &start, &end);

	gchar *text =
		gtk_text_buffer_get_slice (FPROPS (current_page, Buffer), &start, &end,
								   FALSE);

	/* theres no text, so we stop */
	if (text == NULL)
	{
		return;
	}

	GRegex *regex;
	GError *error = NULL;

	/* pattern compiling options. multiline is default. */
	guint options = G_REGEX_MULTILINE + (sensitive ? 0 : G_REGEX_CASELESS);

	/* compiles the pattern and creates a 'instance' of the GRegex structure */
	regex = g_regex_new (pattern, options, 0, &error);

	/* if some error has happened, free the used resources and stop */
	if (error)
	{
		/* Show some message to the user, the line bellow is just for debugging */
		g_print ("g_regex_new() error: %s\n", error->message);

		g_free (regex);
		return;
	}

	error = NULL;

	gchar *new_text;

	new_text =
		g_regex_replace (regex, text, strlen (text), 0, substitute,
						 G_REGEX_MATCH_NOTEMPTY, &error);
	new_text =
		g_utf8_normalize (new_text, strlen (new_text), G_NORMALIZE_DEFAULT);
	gtk_text_buffer_set_text (FPROPS (current_page, Buffer), new_text,
							  strlen (new_text));

	if (error)
	{
		/* Show some message to the user, the line bellow is just for debugging */
		g_print ("g_regex_replace_eval() error: %s\n", error->message);
		g_free (regex);
		return;
	}

	FPROPS (current_page, Format) = beaver_text_format ();
}

#endif
