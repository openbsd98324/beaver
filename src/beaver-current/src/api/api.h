
/*
** Beaver's an Early AdVanced EditoR
** (C) 1999-2000 Marc Bevand, Damien Terrier and Emmanuel Turquin
** (C) 2008 Tobias Heinzen, Double 12
** (C) 2008 Higor Eurípedes
**
** api.h
**
** Author<s>:   Tobias Heinzen, Higor Eurípedes
** Description:   include file for internal use (implementation)
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef __API_H_
#define __API_H_

#include <glib.h>

#include "beaver.h"

/* API PROTOTYPES */

/* box */
void beaver_box_message_impl (gchar * message);
void beaver_box_error_impl (gchar * message);
gint beaver_box_question_impl (gchar * message);
gchar *beaver_box_prompt_impl (gchar * message);
gchar *beaver_box_file_selection_impl (gchar * title,
									   GtkFileChooserAction action,
									   const gchar * starting_folder);

/* ui */
gint beaver_ui_item_add_impl (enum BeaverMenuSection section,
							  const gchar * stock_id, const gchar * name,
							  const gchar * text, void (*callback) (void));
gint beaver_ui_item_submenu_add_impl (enum BeaverMenuSection section,
									  const gchar * name, const gchar * text);
gint beaver_ui_item_seperator_add_impl (enum BeaverMenuSection section);
void beaver_ui_item_remove_impl (gint id, const gchar * name);

/* text */
void beaver_text_insert_string_impl (const gchar * text);
BeaverTextSelection *beaver_text_selection_position_impl (void);
gchar *beaver_text_selection_get_impl (void);
void beaver_text_selection_set_impl (const gchar * text);
enum BeaverTextFormat beaver_text_format_impl (void);
void beaver_text_replace_impl (const gchar * needle, const gchar * substitute,
							   gboolean sensitive);
gboolean beaver_text_find_impl (const gchar * needle, guint offset,
								gboolean sensitive);

#ifdef REGEX_SUPPORT
BeaverRegexResult *beaver_text_find_regex_impl (const gchar * pattern,
												guint offset,
												gboolean sensitive);
void beaver_text_replace_regex_impl (const gchar * pattern,
									 const gchar * substitute,
									 gboolean sensitive, gboolean all);
#endif

#endif							/* __API_H_ */
