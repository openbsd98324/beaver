
/*
** Beaver's an Early AdVanced EditoR
** (C) 1999-2000 Marc Bevand, Damien Terrier and Emmanuel Turquin, 2008 Tobias Heinzen, Double 12
**
** box.c
**
** Author<s>:   Tobias Heinzen, Double 12
** Description:   Message and error boxes
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <glib.h>
#include <gtk/gtk.h>

#include "api.h"

/** @file box.c
 * Implementation of message and error boxes for plugins.
 */

/**
 * shows a message box. the message box contains of a message
 * label and an ok button.
 *
 * @param	message	the message to display
 */
void beaver_box_message_impl (gchar * message)
{
	/* create new message dialog (info dialog with ok button) */
	GtkWidget *dialog;
	dialog = gtk_message_dialog_new (NULL,	/* parent */
									 GTK_DIALOG_MODAL,	/* flags */
									 GTK_MESSAGE_INFO,	/* type */
									 GTK_BUTTONS_OK,	/* buttons */
									 /* message */
									 message, NULL);

	/* show and destroy */
	gtk_dialog_run (GTK_DIALOG (dialog));
	gtk_widget_destroy (dialog);
}

/**
 * shows an error box. the error box contains of a message 
 * label and an ok button.
 * 
 * @param   message the message to display
 */
void beaver_box_error_impl (gchar * message)
{
	/* create new message dialog (error dialog with ok button) */
	GtkWidget *dialog;
	dialog = gtk_message_dialog_new (NULL,	/* parent */
									 GTK_DIALOG_MODAL,	/* flags */
									 GTK_MESSAGE_ERROR,	/* type */
									 GTK_BUTTONS_OK,	/* buttons */
									 /* message */
									 message, NULL);

	/* show and destroy */
	gtk_dialog_run (GTK_DIALOG (dialog));
	gtk_widget_destroy (dialog);
}

/**
 * shows a question box. the question box contains of a
 * message label and two buttons: yes and no.
 *
 * @param   message the message to display
 * @retval  1   yes was pressed
 * @retval  0   otherwise
 */
gint beaver_box_question_impl (gchar * message)
{
	/* create new message dialog (error dialog with ok button) */
	GtkWidget *dialog;
	dialog = gtk_message_dialog_new (NULL,	/* parent */
									 GTK_DIALOG_MODAL,	/* flags */
									 GTK_MESSAGE_QUESTION,	/* type */
									 GTK_BUTTONS_YES_NO,	/* buttons */
									 /* message */
									 message, NULL);

	/* show and destroy */
	gint res = gtk_dialog_run (GTK_DIALOG (dialog));
	gtk_widget_destroy (dialog);

	if (res == GTK_RESPONSE_YES)
	{
		return 1;
	}
	return 0;
}

/**
 * shows a prompt box. the prompt box asks for
 * input.
 *
 * @param   	message		the message to display
 * @retval					the input given by the user. returns NULL if 
 * 							there are errors. This pointer needs to be freed.
 */
gchar *beaver_box_prompt_impl (gchar * message)
{
	/* create new message dialog (error dialog with ok button) */
	GtkWidget *dialog;
	GtkWidget *inputbox;
	dialog = gtk_message_dialog_new (NULL,	/* parent */
									 GTK_DIALOG_MODAL,	/* flags */
									 GTK_MESSAGE_QUESTION,	/* type */
									 GTK_BUTTONS_OK_CANCEL,	/* buttons */
									 /* message */
									 message, NULL);

	/* create a new GtkEntry to receive input and put it into the vbox of the dialog */
	inputbox = gtk_entry_new ();
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), inputbox, FALSE,
						FALSE, 0);

	/* show inputbox and then the whole dialog */
	gtk_widget_show (inputbox);
	gint res = gtk_dialog_run (GTK_DIALOG (dialog));

	/* get the text out of the inputbox before it is destroyed */
	gchar *given_input = g_strdup (gtk_entry_get_text (GTK_ENTRY (inputbox)));
	gtk_widget_destroy (dialog);

	if (res == GTK_RESPONSE_OK)
	{
		return given_input;
	}
	else
	{
		return NULL;
	}


}

/**
 * shows a file selection box. 
 * 
 * @param   title   			Title of the file selection box.
 * @param	action				Set whether the dialog is an Open or Save dialog.
 * 								action can be GTK_FILE_CHOOSER_ACTION_OPEN or GTK_FILE_CHOOSER_ACTION_SAVE
 * @param	starting_folder		The folder where the file selection dialog will start to look.
 * @returns 					Returns the path of the file selected. has to be freed after
 *          					usage. Returns NULL if no file was selected or an error occured.
 */
gchar *beaver_box_file_selection_impl (gchar * title,
									   GtkFileChooserAction action,
									   const gchar * starting_folder)
{
	/*
	   GtkWidget *FileSelector = gtk_file_selection_new(title);
	   gtk_window_set_modal (GTK_WINDOW(FileSelector), TRUE);

	   gint response = gtk_dialog_run (GTK_DIALOG (FileSelector));

	   gchar* file = g_strdup (gtk_file_selection_get_filename (GTK_FILE_SELECTION (FileSelector)));

	   gtk_widget_destroy (FileSelector);
	 */

	GtkWidget *dialog;

	/* Create a GtkFileChooserDialog, depending on the action (open or save) */
	if (action == GTK_FILE_CHOOSER_ACTION_SAVE)
	{
		dialog = gtk_file_chooser_dialog_new (title,
											  NULL,
											  GTK_FILE_CHOOSER_ACTION_SAVE,
											  GTK_STOCK_CANCEL,
											  GTK_RESPONSE_CANCEL,
											  GTK_STOCK_SAVE_AS,
											  GTK_RESPONSE_ACCEPT, NULL);
	}
	else
	{
		dialog = gtk_file_chooser_dialog_new (title,
											  NULL,
											  GTK_FILE_CHOOSER_ACTION_OPEN,
											  GTK_STOCK_CANCEL,
											  GTK_RESPONSE_CANCEL,
											  GTK_STOCK_OPEN,
											  GTK_RESPONSE_ACCEPT, NULL);
	}

	/* Ask for a confirmation when overwriting an existing file */
	gtk_file_chooser_set_do_overwrite_confirmation (GTK_FILE_CHOOSER (dialog),
													TRUE);

	/* Set the current folder of the dialog to starting_folder.
	 * Give an error box when it fails */
	if (!gtk_file_chooser_set_current_folder
		(GTK_FILE_CHOOSER (dialog), starting_folder))
	{
		gchar *info = g_strconcat ("Invalid location: ", starting_folder, NULL);
		beaver_box_error (info);
		g_free (info);
	}

	if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT)
	{
		gchar *file =
			g_strdup (gtk_file_chooser_get_filename
					  (GTK_FILE_CHOOSER (dialog)));
		gtk_widget_destroy (dialog);
		return file;
	}
	else
	{
		gtk_widget_destroy (dialog);
		return NULL;
	}

}
