
/*
** Beaver's an Early AdVanced EditoR
** (C) 1999-2000 Marc Bevand, Damien Terrier and Emmanuel Turquin, 2008 Tobias Heinzen, Double 12
**
** ui.c
**
** Author<s>:   Tobias Heinzen and Double 12
** Description:   Menu manipulation
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <glib.h>
#include <gtk/gtk.h>

#include "beaver.h"
#include "api.h"

/** @file ui.c
 * Implementation for menu manipulations for plugins.
 */

/* the menu manager from interface.c */
extern GtkUIManager *MenuManager;

/* some statics */
static GtkActionGroup *plugin_action_group = NULL;

/**
 * Generates a path (XPath, for UiManager) for a specific section. 
 * 
 * @param   section the section for which a path should be generated 
 * @returns path for specified section. must be freed.
 */
static inline gchar *construct_path (enum BeaverMenuSection section)
{
	gchar *path = NULL;
	switch (section)
	{
	case BEAVER_SECTION_MENU_TOOLS:
	default:
		path = g_strdup ("ui/MainMenu/ToolsMenu");
		break;
	case BEAVER_SECTION_MENU_VIEW:
		path = g_strdup ("ui/MainMenu/ViewMenu");
		break;
	case BEAVER_SECTION_MENU_EDIT:
		path = g_strdup ("ui/MainMenu/EditMenu");
		break;
	case BEAVER_SECTION_TOOLBAR:
		path = g_strdup ("ui/MainToolbar");
		break;
	}

	return path;
}

/**
 * Adds a submenu into the specified section. The
 * submenu has to be populated with calls to beaver_menu_add_menu. To
 * add items the text field in this call has to have the form <name>/<text>
 * where name is the name you provided here and text is the label
 * of the new submenuentry.
 * 
 * @param   section the section in which to create the new item
 * @param   name    internal name of menu item
 * @param   text    text on the label of the menu item
 * @returns integer id representing the inserted menu item 
 */
gint beaver_ui_item_submenu_add_impl (enum BeaverMenuSection section,
									  const gchar * name, const gchar * text)
{
	/* get a new merge id */
	gint merge_id = gtk_ui_manager_new_merge_id (MenuManager);

	/* get the path to insert the menu into */
	gchar *path = construct_path (section);

	/* create action name, this is just <name>Action */
	gchar *action_name = g_strconcat (name, "Action", NULL);

	/* create action entry */
	GtkActionEntry entries[] = {
		{action_name,	/* action name */
		 NULL,	/* stock item */
		 text,	/* label */
		 NULL,	/* accelerator */
		 NULL,	/* tooltip */
		 NULL	/* callback */
		 }
	};
	gtk_action_group_add_actions (plugin_action_group, entries, 1, NULL);

	/* add submenu */
	gtk_ui_manager_add_ui (MenuManager, merge_id, path, name, action_name,
						   GTK_UI_MANAGER_MENU, FALSE);

	/* free up space */
	g_free (action_name);
	g_free (path);

	return merge_id;
}

/**
 * Adds a menu item into specified menu section.
 * 
 * @param   section 	the section in which to create the new item
 * @param   name    	internal name of the item
 * @param   stock_id    The stock id for the action, or the name of an icon from the icon theme.
 * @param   text    	text on the label of the item
 * @param   callback    the function that get's called when user 
 *                      clicks on this menu item.
 * @returns integer id representing the inserted menu item 
 */
gint beaver_ui_item_add_impl (enum BeaverMenuSection section,
							  const gchar * name, const gchar * stock_id,
							  const gchar * text, void (*callback) (void))
{
	/* create action group if necessary */
	if (!plugin_action_group)
	{
		plugin_action_group = gtk_action_group_new ("PluginActionGroup");
		gtk_ui_manager_insert_action_group (MenuManager, plugin_action_group,
											0);
	}

	/* create merge id */
	gint merge_id = gtk_ui_manager_new_merge_id (MenuManager);

	/* create action name, this is just <name>Action */
	gchar *action_name = g_strconcat (name, "Action", NULL);

	/* split the text (to look if we want a menuitem of a previously
	 * created submenu) */
	gchar **argv = g_strsplit (text, "/", 2);

	gchar *label = NULL;
	gchar *menu_path = NULL;
	if (argv[1] == NULL)
	{
		label = argv[0];
	}
	else
	{
		menu_path = argv[0];
		label = argv[1];
	}

	/* create action entry */
	GtkActionEntry entries[] = {
		{action_name,	/* action name */
		 stock_id,	/* stock item */
		 label,	/* label */
		 NULL,	/* accelerator */
		 NULL,	/* tooltip */
		 G_CALLBACK (callback)	/* callback */
		 }
	};
	gtk_action_group_add_actions (plugin_action_group, entries, 1, NULL);

	/* determine right path */
	gchar *path = construct_path (section);

	/* determine which type the new created item has. this is done
	 * by checking what section of the ui the item falls into */
	GtkUIManagerItemType itemtype = GTK_UI_MANAGER_MENUITEM;
	if (section == BEAVER_SECTION_TOOLBAR)
	{
		itemtype = GTK_UI_MANAGER_TOOLITEM;
	}

	/* add menu in the specified path. insert the new menu item in
	 * a submenu if wanted */
	if (menu_path == NULL)
	{
		gtk_ui_manager_add_ui (MenuManager, merge_id, path, name, action_name,
							   itemtype, FALSE);
	}
	else
	{
		gchar *new_path = g_strconcat (path, "/", menu_path, NULL);
		gtk_ui_manager_add_ui (MenuManager, merge_id, new_path, name,
							   action_name, itemtype, FALSE);
		g_free (new_path);
	}

	/* free up space */
	g_strfreev (argv);
	g_free (action_name);
	g_free (path);

	return merge_id;
}



/**
 * Adds a menu seperator.
 * 
 * @returns integer id representing the inserted menu item
 */
gint beaver_ui_item_seperator_add_impl (enum BeaverMenuSection section)
{
	/* this counts the seperators created. used for creating unique
	 * seperator names */
	static gint n_seperator = 0;

	/* create merge id */
	gint merge_id = gtk_ui_manager_new_merge_id (MenuManager);

	/* determine right path */
	gchar *path = construct_path (section);

	/* generating seperator name */
	gchar *name = g_strdup_printf ("plugin-seperator-%d", n_seperator++);

	/* add menu in the specified path */
	gtk_ui_manager_add_ui (MenuManager, merge_id, path, name, NULL,
						   GTK_UI_MANAGER_SEPARATOR, FALSE);

	/* free up space */
	g_free (name);
	g_free (path);

	return merge_id;
}

/**
 * Removes a previously added menu item.
 * 
 * @param   id      integer id as returned by beaver_menu_add_* functions
 * @param   name    internal name of menu item (as passed in beaver_menu_add_item)
 *                  if NULL then action will not be removed.
 */
void beaver_ui_item_remove_impl (gint id, const gchar * name)
{
	/* remove the action */
	if (name != NULL)
	{
		gchar *action_name = g_strconcat (name, "Action", NULL);
		GtkAction *action =
			gtk_action_group_get_action (plugin_action_group, action_name);
		gtk_action_group_remove_action (plugin_action_group, action);
		g_free (action_name);
	}

	/* remove the the menu item */
	gtk_ui_manager_remove_ui (MenuManager, id);
}
