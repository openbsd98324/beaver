
/*
** Beaver's an Early AdVanced EditoR
** (C) 1999-2000 Marc Bevand, Damien Terrier and Emmanuel Turquin
** (C) 2008 Tobias Heinzen, Double 12
** (C) 2008 Higor Euripedes
**
** plugin.h
**
** Author<s>:   Tobias Heinzen, Higor Euripedes
** Description:   API for plugins
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef __BEAVER_H_
#define __BEAVER_H_

#include <glib.h>
#include <gtk/gtk.h>

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/** @file beaver.h
 * Plugin API
 */

/*! \mainpage Beaver API documentation
 *
 * You can check the functions available in the Beaver API, by clicking \link api_group here \endlink.
 *
 */


/** @defgroup api_group API documentation
 *  The Beaver API consists of several functions, enabling you to write plugins.
 *  @{
 */


/**
 * Creates a function that returns a string identifying
 * the author of this plugin.
 *
 * @param   name_string name of the author
 */
#define PLUGIN_AUTHOR(name_string) \
gchar* __plugin_author (void) \
{ \
    return (name_string); \
}

/**
 * Creates a function that returns a string identifying
 * the version of this plugin
 *
 * @param   version_string  version
 */
#define PLUGIN_VERSION(version_string) \
gchar* __plugin_version (void) \
{ \
    return (version_string); \
}

/**
 * Creates a function that calls a function for initialization
 *
 * @param   func_handler    pointer to function that should
 *                          be called.
 */
#define PLUGIN_INIT(func_handler) \
void __plugin_init (void) \
{ \
    (func_handler) (); \
}

/**
 * Creates a function that calls a function for cleanup
 *
 * @param   func_handler    pointer to function that should
 *                          be called.
 */
#define PLUGIN_CLEANUP(func_handler) \
void __plugin_cleanup (void) \
{ \
    (func_handler) (); \
}

/**
 * Creates a function that returns a string identifying
 * the name of this plugin
 *
 * @param   name_string name of this plugin
 */
#define PLUGIN_NAME(name_string) \
gchar* __plugin_name (void) \
{ \
    return (name_string); \
}

/**
 * Creates a function that returns a string identifying
 * a short description about this plugin.
 *
 * @param   descr_string    a short description
 */
#define PLUGIN_DESCRIPTION(descr_string) \
gchar* __plugin_description (void) \
{ \
    return (descr_string); \
}

/**
 * Creates a function that calls a function when
 * the preferences button in the plugin manager
 * was clicked.
 *
 * @param   func_handler    pointer to function that should
 *                          be called.
 */
#define PLUGIN_PREFERENCE(func_handler) \
void __plugin_preference (void) \
{ \
    (func_handler) (); \
}

/** @see    src/api/box.c */
void (*beaver_box_message) (gchar * message);

/** @see    src/api/box.c */
void (*beaver_box_error) (gchar * message);

/** @see    src/api/box.c */
gint (*beaver_box_question) (gchar * message);

/** @see    src/api/box.c */
gchar *(*beaver_box_prompt) (gchar * message);

/** @see    src/api/box.c */
gchar *(*beaver_box_file_selection) (gchar * title, GtkFileChooserAction action,
									 const gchar * starting_folder);

/**
 * the different menu sections.
 *
 * @param BEAVER_SECTION_MENU_TOOLS
 * @param BEAVER_SECTION_MENU_VIEW
 * @param BEAVER_SECTION_MENU_EDIT
 * @param BEAVER_SECTION_TOOLBAR
 */
enum BeaverMenuSection
{
	BEAVER_SECTION_MENU_TOOLS = 0,
	BEAVER_SECTION_MENU_VIEW,
	BEAVER_SECTION_MENU_EDIT,
	BEAVER_SECTION_TOOLBAR
};

/** @see    src/api/ui.c */
gint (*beaver_ui_item_add) (enum BeaverMenuSection section, const gchar * name,
							const gchar * stock_id, const gchar * text,
							void (*callback) (void));

/** @see    src/api/ui.c */
gint (*beaver_ui_item_submenu_add) (enum BeaverMenuSection section,
									const gchar * name, const gchar * text);

/** @see    src/api/ui.c */
gint (*beaver_ui_item_seperator_add) (enum BeaverMenuSection section);

/** @see    src/api/ui.c */
void (*beaver_ui_item_remove) (gint id, const gchar * name);

/** @see    src/api/text.c */
void (*beaver_text_insert_string) (const gchar * text);

/**
 * A structure holding the position of the selection.
 *
 * @param   line    the line the selection is in
 * @param   start   the start of the selection (according to line)
 * @param   end     the end of the selection (according to line)
 * @param   offset  start of the selection (according to whole document)
 */
struct beaver_text_selection_s
{
	gint line;
	gint start;
	gint end;
	gint offset;
};
typedef struct beaver_text_selection_s BeaverTextSelection;

/**
 * A structure that holds the regex testing results
 *
 * @param count   the number of elements in results
 * @param results an BeaverTextSelection array
 */
struct beaver_regex_result_s
{
	gint count;
	BeaverTextSelection *results;
};
typedef struct beaver_regex_result_s BeaverRegexResult;

/** @see    src/api/text.c */
BeaverTextSelection *(*beaver_text_selection_position) (void);

/** @see    src/api/text.c */
gchar *(*beaver_text_selection_get) (void);

/** @see    src/api/text.c */
void (*beaver_text_selection_set) (const gchar * text);

/**
 * the different text formats
 *
 * @param   BEAVER_FORMAT_UNIX
 * @param   BEAVER_FORMAT_MAC
 * @param   BEAVER_FORMAT_DOS
 */
enum BeaverTextFormat
{
	BEAVER_FORMAT_UNIX = 0,
	BEAVER_FORMAT_MAC,
	BEAVER_FORMAT_DOS
};

/** @see    src/api/text.c */
enum BeaverTextFormat (*beaver_text_format) (void);

/** @see    src/api/text.c */
void (*beaver_text_replace) (const gchar * needle, const gchar * substitute,
							 gboolean sensitive);

/** @see    src/api/text.c */
gboolean (*beaver_text_find) (const gchar * needle, guint offset,
							  gboolean sensitive);

#ifdef REGEX_SUPPORT

/** @see    src/api/text.c */
BeaverRegexResult *(*beaver_text_find_regex) (const gchar * pattern,
											  guint offset, gboolean sensitive);

/** @see    src/api/text.c */
void (*beaver_text_replace_regex) (const gchar * pattern,
								   const gchar * substitute, gboolean sensitive,
								   gboolean all);
#endif

					/** @} */// end of api_group


#endif							/* __BEAVER_H_ */
