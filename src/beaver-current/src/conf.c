
/*
** Beaver's an Early AdVanced EditoR
** (C) 1999-2000 Marc Bevand, Damien Terrier and Emmanuel Turquin, 2008 Tobias Heinzen, Double 12
**
** conf.c
**
** Author<s>:   Emmanuel Turquin (aka "Ender") <turqui_e@epita.fr>
**				Tobias Heinzen
** Description: Beaver preferences management source
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/** @file conf.c 
 * Configuration file managment. These functions handle setting/getting of key
 * in a .ini like configuration file. The configuration file resides in the 
 * home directory of a user.
 * 
 * A query (key) for the config file has the following form:\n
 *      file/category/entry.\n
 * The file identifier is ignored.
 * 
 * A config file has the following form:\n
 * [category]\n
 * entry = value\n
 * entry = value\n
 * ...
 */

#include <fcntl.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <stdio.h>
#include <stdlib.h>
#include "conf.h"
#include "main.h"
#include "api/beaver.h"
#include "../config.h"

/**
 * Structure to hold data of config file.
 */
static GKeyFile *__config_file = NULL;

/**
 * Load data from config file. The data only get's loaded once.
 */
static inline void load_config (void)
{
	/* do not load the config file a second time */
	if (__config_file)
		return;

	/* construct the filename */
	gchar *filename = g_strconcat (g_get_home_dir (), PATH_SEP_STRING,
								   CONF_DIR, PATH_SEP_STRING, CONF_FILE_NAME,
								   NULL);

	/* create new gkeyfile and load the configuration
	 * into it */
	__config_file = g_key_file_new ();
	g_key_file_load_from_file (__config_file, filename, G_KEY_FILE_NONE, NULL);

	/* free filename */
	g_free (filename);
}

/**
 * closes the config file.
 */
static inline void close_config (void)
{
	if (!__config_file)
		return;
	g_key_file_free (__config_file);
	__config_file = NULL;
}

/**
 * Saves changes to the config file. Whenever a change (ie. whenever
 * a set_* function is used) the config file data must be written
 * back to the config file so that these options get loaded
 * the next time.
 */
static inline void save_config (void)
{
	/* check if the config file was opened, if not
	 * then nothing needs to be saved */
	if (!__config_file)
		return;

	/* construct the filename */
	gchar *filename = g_strconcat (g_get_home_dir (), PATH_SEP_STRING,
								   CONF_DIR, PATH_SEP_STRING, CONF_FILE_NAME,
								   NULL);

	/* get the data and length of the new config file */
	gsize length = 0;
	gchar *data = g_key_file_to_data (__config_file, &length, NULL);

	/* save into file */
	FILE *fp = fopen (filename, "w");
	fwrite (data, 1, length, fp);
	fclose (fp);

	/* free filename */
	g_free (filename);
	g_free (data);
}

/**
 * Initsializes the config file. This checks wheter the config file
 * exists. If not then the config file is created with some default
 * values.
 */
void init_conf (void)
{
	/* dummy */
	struct stat dummy;

	/* check if the config directory exists */
	gchar *config_dir = g_strconcat (g_get_home_dir (), PATH_SEP_STRING,
									 CONF_DIR, NULL);
	if (g_stat (config_dir, &dummy) == -1)
	{
		g_mkdir (config_dir, 0755);
	}

	/* check if the config file exists */
	gchar *config_fil = g_strconcat (config_dir, PATH_SEP_STRING,
									 CONF_FILE_NAME, NULL);
	if (g_stat (config_fil, &dummy) == -1)
	{
		/* copy default config file from resource directory */
		gchar *command =
			g_strdup_printf
			("cp %s/beaver.conf.default %s 2>/dev/null 1>/dev/null",
			 RESOURCE_DIR, config_fil);
		if (system (command))
		{
			printf ("Error in creating default config file!\nAbort!\n");
			exit (-1);
		}

		/* setting up some plattform dependent stuff */
		gchar *wordfile = g_strconcat (config_dir, PATH_SEP_STRING,
									   "wordfile.txt", NULL);
		set_string_conf ("/Editor/Wordfile", wordfile);
		g_free (wordfile);

#ifdef WIN32
		set_string_conf ("/AutoSave/BackupExt", ".bak");
#else
		set_string_conf ("/AutoSave/BackupExt", "~");
#endif
#ifdef WIN32
		set_string_conf ("/Misc/PrintCommand", "%s > lpt1");
#else
		set_string_conf ("/Misc/PrintCommand", "lpr");
#endif

		/* free up space */
		g_free (command);
	}

	/* freeup used variables */
	g_free (config_dir);
	g_free (config_fil);
}

/**
 * Loads a string value.
 * 
 * @param   key query string in the form "file/category/entry"
 * @return  the string value of the key in the config file or empty string 
 *          if key not found. string must be freed.
 */
gchar *get_string_conf (gchar * key)
{
	/* make sure config file is loaded */
	load_config ();

	/* parse the key query string */
	gchar **argv = g_strsplit (key, "/", 3);

	/* load the value for that key */
	gchar *result =
		g_key_file_get_string (__config_file, argv[1], argv[2], NULL);

	/* freeup used space */
	g_strfreev (argv);
	close_config ();

	if (!result)
		return g_strdup ((gchar *) "");
	return result;
}

/**
 * Loads an integer value.
 * 
 * @param   key query string in the form "file/category/entry"
 * @return  the integer value of the key in the config file
 */
gint get_int_conf (gchar * key)
{
	/* make sure config file is loaded */
	load_config ();

	/* parse the key query string */
	gchar **argv = g_strsplit (key, "/", 3);

	/* load the value for that key */
	gint result =
		g_key_file_get_integer (__config_file, argv[1], argv[2], NULL);

	/* freeup used space */
	g_strfreev (argv);
	close_config ();

	return result;
}

/**
 * Loads an boolean value.
 * 
 * @param   key query string in the form "file/category/entry"
 * @return  the boolean value of the key in the config file
 */
gboolean get_bool_conf (gchar * key)
{
	/* make sure config file is loaded */
	load_config ();

	/* parse the key query string */
	gchar **argv = g_strsplit (key, "/", 3);

	/* load the value for that key */
	gboolean result =
		g_key_file_get_boolean (__config_file, argv[1], argv[2], NULL);

	/* freeup used space */
	g_strfreev (argv);
	close_config ();

	return result;
}

/**
 * Setting a string value.
 * 
 * @param   key     query string in the form "file/category/entry"
 * @param   value   value of the entry
 * @retval 1 on success
 * @retval 0 otherwise
 */
gint set_string_conf (gchar * key, gchar * value)
{
	/* make sure config file is loaded */
	load_config ();

	/* parse the key query string */
	gchar **argv = g_strsplit (key, "/", 3);

	/* set the value */
	g_key_file_set_string (__config_file, argv[1], argv[2], value);

	g_strfreev(argv);
	/* save back changes */
	save_config ();
	close_config ();

	return 1;
}

/**
 * Setting an integer value.
 * 
 * @param   key     query string in the form "file/category/entry"
 * @param   value   value of the entry
 * @retval 1 on success
 * @retval 0 otherwise
 */
gint set_int_conf (gchar * key, gint value)
{
	/* make sure config file is loaded */
	load_config ();

	/* parse the key query string */
	gchar **argv = g_strsplit (key, "/", 3);

	/* set the value */
	g_key_file_set_integer (__config_file, argv[1], argv[2], value);

	g_strfreev(argv);

	/* save back changes */
	save_config ();
	close_config ();

	return 0;
}

/**
 * Setting a boolean value.
 * 
 * @param   key     query string in the form "file/category/entry"
 * @param   value   value of the entry
 * @retval 1 on success
 * @retval 0 otherwise
 */
gint set_bool_conf (gchar * key, gboolean value)
{
	/* make sure config file is loaded */
	load_config ();

	/* parse the key query string */
	gchar **argv = g_strsplit (key, "/", 3);

	/* set the value */
	g_key_file_set_boolean (__config_file, argv[1], argv[2], value);

	g_strfreev(argv);

	/* save back changes */
	save_config ();
	close_config ();

	return 0;
}
