
/*
** Beaver's an Early AdVanced EditoR
** (C) 1999-2000 Marc Bevand, Damien Terrier and Emmanuel Turquin, 2008 Tobias Heinzen, Double 12
**
** completion.c
**
** Author<s>:   Marc Bevand (aka "After") <bevand_m@epita.fr>
**              Emmanuel Turquin (aka "Ender") <turqui_e@epita.fr>
**              Michael Terry <mterry@fastmail.fm>
**
** Description: Do auto-completion stuff
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/** @file completion.c
 * Auto completion
 */

#include <gdk/gdkkeysyms.h>
#include <gtk/gtk.h>
#include <string.h>
#include "completion.h"
#include "editor.h"
#include "prefs.h"
#include "struct.h"
#include "msgbar.h"

/* some external defined structures */
extern GtkWidget *MainNotebook;
extern GArray *FileProperties;
extern t_settings Settings;

/* some static variables for the completion window. we only
 * want to create this window once! */
static GtkWidget *__CompletionWindow = NULL;
static GtkWidget *__CompletionTreeView = NULL;
static GtkWidget *__CompletionScrolledWindow = NULL;
static gint handlers[2] = { 0, 0 };

/**
 * Structure holding start and end position where
 * to insert found keyword.
 * 
 * @param   Editor  the textview where the keyword is
 * @param   start   position where the keyword starts 
 * @param   end     position where the keyword ends
 */
struct keyword_pos_s
{
	GtkTextView *Editor;
	gint start;
	gint end;
};

/**
 * Load the last inserted word by the user.
 * 
 * @param   Editor      a textview where to search in
 * @param   Delimiters  delimiters that seperate words
 * @param   kstart      will be overwritten with the start position of
 *                      the found keyword
 * @param   kend        will be overwritten with the end position of
 *                      the found keyword
 * @return  string of found word or NULL if nothing was entered
 */
static inline gchar *get_last_user_word (GtkTextView * Editor,
										 gchar * Delimiters, gint * kstart,
										 gint * kend)
{
	GtkTextIter bstart, bend;	/* start and end of buffer */
	GtkTextBuffer *EditorBuffer;	/* the textbuffer to search into */
	gchar *buffer_text = NULL;	/* the text in the textbuffer */
	gchar *keyword = NULL;

	/* get textbuffer and boundaries */
	EditorBuffer = gtk_text_view_get_buffer (Editor);
	gtk_text_buffer_get_bounds (EditorBuffer, &bstart, &bend);
	buffer_text = gtk_text_buffer_get_text (EditorBuffer, &bstart, &bend, TRUE);

	/* get end of the user keyword */
	gtk_text_buffer_get_iter_at_mark (EditorBuffer, &bstart,
									  gtk_text_buffer_get_mark (EditorBuffer,
																"insert"));
	*kend = gtk_text_iter_get_offset (&bstart);
	gtk_text_buffer_get_bounds (EditorBuffer, &bstart, &bend);

	/* traverse backwards in the buffer until we found the beginning
	 * of the textbuffer or a delimiter, thus we've found the start
	 * of the user keyword */
	*kstart = *kend - 1;
	gint text_start = gtk_text_iter_get_offset (&bstart);
	while (*kstart > text_start && !strchr (Delimiters, buffer_text[*kstart]))
	{
		(*kstart)--;
	}

	/* trims additional space before a word */
	if (*kstart > text_start)
	{
		(*kstart)++;
	}

	/* finally get the keyword out of the buffer */
	gtk_text_buffer_get_iter_at_offset (EditorBuffer, &bstart, *kstart);
	gtk_text_buffer_get_iter_at_offset (EditorBuffer, &bend, *kend);
	keyword = gtk_text_buffer_get_text (EditorBuffer, &bstart, &bend, FALSE);

	/* free up space */
	g_free (buffer_text);

	/* if the keyword is empty, then return NULL */
	if (strncmp (keyword, "", 1) == 0)
	{
		*kstart = *kend = -1;
		g_free (keyword);
		return NULL;
	}

	return keyword;
}

/**
 * Overwrites the text at position (start,end) with the new word.
 * 
 * @param   Editor  the textview to make the change
 * @param   Word    the new word
 * @param   start   start of old word
 * @param   end     end of old word
 */
static inline void overwrite_word (GtkTextView * Editor, gchar * Word,
								   gint start, gint end)
{
	GtkTextIter bstart, bend;	/* start and end of buffer */
	GtkTextBuffer *EditorBuffer;	/* the textbuffer to search into */
	gchar *msg = NULL;

	/* get textbuffer and set cursor positions */
	EditorBuffer = gtk_text_view_get_buffer (Editor);
	gtk_text_buffer_get_iter_at_offset (EditorBuffer, &bstart, start);
	gtk_text_buffer_get_iter_at_offset (EditorBuffer, &bend, end);

	/* delete old word */
	gtk_text_buffer_delete (EditorBuffer, &bstart, &bend);
	gtk_text_buffer_insert_at_cursor (EditorBuffer, Word, strlen (Word));

	/* print message */
	msg = g_strconcat ("String \"", Word, "\" inserted...", NULL);
	print_msg (msg);
	g_free (msg);
}

/**
 * Handler for key press events. Get's called by the completion window.
 * 
 * @param   window  on which window appeared the signal
 * @param   event   what happened
 * @param   kpos    some information about input position
 */
static void signal_key_press (GtkWidget * window, GdkEventKey * event,
							  struct keyword_pos_s *kpos)
{
	/* we're only interested in the PRESS events */
	if (event->type == GDK_KEY_RELEASE)
		return;

	/* get the selected data */
	GtkTreeIter iter;
	GtkTreeModel *model;
	gchar *value = NULL;

	GtkTreeSelection *selection =
		gtk_tree_view_get_selection (GTK_TREE_VIEW (__CompletionTreeView));
	if (selection
		&& gtk_tree_selection_get_selected (GTK_TREE_SELECTION (selection),
											&model, &iter))
	{
		gtk_tree_model_get (model, &iter, 0, &value, -1);
	}

	/* which button was pressed? */
	switch (event->keyval)
	{
	case GDK_Return:
		if (value)
		{
			overwrite_word (kpos->Editor, value, kpos->start, kpos->end);
		}
	case 32:	/* space */
		gtk_widget_destroy (__CompletionTreeView);
		__CompletionTreeView = NULL;
		gtk_widget_hide (__CompletionWindow);
		g_free (kpos);
		break;
	case GDK_Down:
		if (value && gtk_tree_model_iter_next (model, &iter))
		{
			gtk_tree_selection_select_iter (selection, &iter);
		}
		break;
	case GDK_Up:
		if (value)
		{
			GtkTreePath *path = gtk_tree_model_get_path (model, &iter);
			if (gtk_tree_path_prev (path)
				&& gtk_tree_model_get_iter (model, &iter, path))
			{
				gtk_tree_selection_select_iter (selection, &iter);
			}
		}
		break;
	}

	/* free up space */
	if (value)
		g_free (value);
}

/**
 * Handler for button press events. Get's called by the completion window.
 * 
 * @param   window  on which window appeared the signal
 * @param   event   what happened
 * @param   kpos    some information about input position
 */
static void signal_button_press (GtkWidget * window, GdkEventKey * event,
								 struct keyword_pos_s *kpos)
{
	/* just remove window */
	gtk_widget_destroy (__CompletionTreeView);
	__CompletionTreeView = NULL;
	gtk_widget_hide (__CompletionWindow);
	g_free (kpos);
}

/**
 * Shows the completion window.
 * 
 * @param   Editor      the text view on which the completion window should appear
 * @param   keywords    the list of keywords that must appear in window
 * @param   kpos        stores some information about input position 
 */
static inline void show_completion_window (GtkTextView * Editor,
										   GList * keywords,
										   struct keyword_pos_s *kpos)
{
	GdkWindow *parent = NULL;	/* the parent window of the editor */
	GdkRectangle rect;
	GtkTextIter end;
	GtkTextBuffer *EditorBuffer;
	gint parent_x, parent_y;	/* the parent origin coordinates */
	gint *coord = NULL;			/* coordinates to move the window to */
	GtkListStore *store = NULL;	/* the treeview in the popup */
	GtkCellRenderer *renderer = NULL;
	GtkTreeIter iter;

	/* create the completion window if necessary */
	if (!__CompletionWindow)
	{
		__CompletionWindow = gtk_window_new (GTK_WINDOW_TOPLEVEL);
		gtk_window_set_decorated (GTK_WINDOW (__CompletionWindow), FALSE);
		gtk_window_set_skip_taskbar_hint (GTK_WINDOW (__CompletionWindow),
										  TRUE);
		gtk_window_set_title (GTK_WINDOW (__CompletionWindow), "Complete");
		gtk_window_set_modal (GTK_WINDOW (__CompletionWindow), TRUE);
		gtk_window_set_transient_for (GTK_WINDOW (__CompletionWindow),
									  GTK_WINDOW (gtk_widget_get_toplevel
												  (GTK_WIDGET (Editor))));
	}

	/* set default size */
	gtk_window_set_default_size (GTK_WINDOW (__CompletionWindow),
								 COMPLETE_WINDOW_WIDTH, COMPLETE_WINDOW_HEIGHT);

	/* create the scrolled window if necessary */
	if (!__CompletionScrolledWindow)
	{
		__CompletionScrolledWindow = gtk_scrolled_window_new (NULL, NULL);
		gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW
										(__CompletionScrolledWindow),
										GTK_POLICY_AUTOMATIC,
										GTK_POLICY_ALWAYS);
		gtk_container_add (GTK_CONTAINER (__CompletionWindow),
						   __CompletionScrolledWindow);
		gtk_widget_show (__CompletionScrolledWindow);
	}

	/* move the window to the right coordinates */
	EditorBuffer = gtk_text_view_get_buffer (Editor);
	gtk_text_buffer_get_iter_at_mark (EditorBuffer, &end,
									  gtk_text_buffer_get_mark (EditorBuffer,
																"insert"));
	gtk_text_view_get_iter_location (Editor, &end, &rect);
	gtk_text_view_buffer_to_window_coords (Editor, GTK_TEXT_WINDOW_WIDGET,
										   rect.x, rect.y, &rect.x, &rect.y);

	parent = (GdkWindow *) gtk_widget_get_parent_window (GTK_WIDGET (Editor));
	gdk_window_get_origin (parent, &parent_x, &parent_y);

	coord = (gint *) g_malloc (2 * sizeof (gint));
	coord[0] =
		parent_x +
		GTK_WIDGET (GTK_WIDGET (MainNotebook)->parent)->allocation.x + rect.x +
		GTK_WIDGET (Editor)->allocation.x + 3;
	if (coord[0] > gdk_screen_width () - COMPLETE_WINDOW_WIDTH)
		coord[0] -= COMPLETE_WINDOW_WIDTH;
	coord[1] =
		parent_y +
		GTK_WIDGET (GTK_WIDGET (MainNotebook)->parent)->allocation.y + rect.y +
		GTK_WIDGET (Editor)->allocation.y + 4;
	if (coord[1] > gdk_screen_height () - COMPLETE_WINDOW_HEIGHT)
		coord[1] -= COMPLETE_WINDOW_HEIGHT;
	gtk_window_move (GTK_WINDOW (__CompletionWindow), coord[0], coord[1]);
	g_free (coord);

	/* fill in the list of keywords into the model */
	store = gtk_list_store_new (1, G_TYPE_STRING);
	while (keywords)
	{
		/* add the keyword into the model */
		gtk_list_store_append (store, &iter);
		gtk_list_store_set (store, &iter, 0, keywords->data, -1);
		/* advance */
		keywords = g_list_next (keywords);
	}

	/* setup treeview and fill it with the model */
	if (!__CompletionTreeView)
	{
		__CompletionTreeView = gtk_tree_view_new ();
		gtk_container_add (GTK_CONTAINER (__CompletionScrolledWindow),
						   __CompletionTreeView);

		renderer = gtk_cell_renderer_text_new ();
		gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW
													 (__CompletionTreeView), -1,
													 "completion word",
													 renderer, "text", 0, NULL);
		gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (__CompletionTreeView),
										   FALSE);
	}

	/* append list of keywords in treeview */
	gtk_tree_view_set_model (GTK_TREE_VIEW (__CompletionTreeView),
							 GTK_TREE_MODEL (store));
	GtkTreeSelection *selection =
		gtk_tree_view_get_selection (GTK_TREE_VIEW (__CompletionTreeView));
	gtk_tree_model_get_iter_first (GTK_TREE_MODEL (store), &iter);
	gtk_tree_selection_select_iter (selection, &iter);	/* mark first entry in list */

	g_object_unref (GTK_TREE_MODEL (store));
	gtk_widget_show (__CompletionTreeView);

	/* attach some signals */
	if (handlers[0] != 0)
		g_signal_handler_disconnect (G_OBJECT (__CompletionWindow),
									 handlers[0]);
	handlers[0] =
		g_signal_connect (G_OBJECT (__CompletionWindow), "key_press_event",
						  GTK_SIGNAL_FUNC (signal_key_press), kpos);
	if (handlers[1] != 0)
		g_signal_handler_disconnect (G_OBJECT (__CompletionWindow),
									 handlers[1]);
	handlers[1] =
		g_signal_connect (G_OBJECT (__CompletionWindow), "button_press_event",
						  GTK_SIGNAL_FUNC (signal_button_press), kpos);

	/* display the window */
	gtk_widget_show (__CompletionWindow);
}

/**
 * This function proceeds auto-completion. It is called in interface.c by
 * a ItemFactory method... (standard accelerator: <control>space)
 * 
 * @param   Editor  a textview where the auto completion should happen
 * @retval  -2  no word to complete
 * @retval  -1  language not recognized
 * @retval  0   otherwise    
 */
gint auto_completion (GtkTextView * Editor)
{
	GList *klist = NULL;		/* list of keywords */
	gchar *ukey = NULL;			/* the user input */
	struct keyword_pos_s *kpos = g_new (struct keyword_pos_s, 1);	/* position of user input */
	gint Lg = -1;				/* which language */
	gint i = 0;

	/* has this textview an associated language */
	Lg = FPROPS (gtk_notebook_get_current_page (GTK_NOTEBOOK (MainNotebook)),
				 WidgetInfo.Lg);
	if (Lg == -1)
	{
		print_msg ("Cannot complete: language not recognized...");
		return -1;
	}

	/* get the user input (beginning of keyword) */
	ukey =
		get_last_user_word (Editor, Prefs.L[Lg].Delimiters, &kpos->start,
							&kpos->end);
	if (!ukey)
	{
		print_msg ("Cannot complete: nothing to complete...");
		return -2;
	}

	/* process all color classes */
	for (i = 0; i < MAX_COL; i++)
	{
		/* are there any keywords? */
		if (!Prefs.L[Lg].C[i].Keywords)
			continue;

		/* the keywords in the color classes are seperated via
		 * a space. so we split these */
		gchar **keywords = g_strsplit (Prefs.L[Lg].C[i].Keywords, " ", 0);

		/* process all these keywords. when a keyword begins with the same
		 * letters as the user input store this into the klist */
		gint word = 0;
		while (keywords[word])
		{
			/* check if the keyword begins with ukey and add it 
			 * to the klist */
			if (g_str_has_prefix (keywords[word], ukey))
			{
				klist = g_list_prepend (klist, g_strdup (keywords[word]));
			}

			/* proceed */
			word++;
		}

		/* free up space */
		g_strfreev (keywords);
	}

	/* if we only got one keyword in the list, then we can 
	 * write that directly into the textbuffer. otherwise
	 * display a need dialog */
	if (g_list_length (klist) == 1)
	{
		gchar *keyword = (gchar *) klist->data;
		overwrite_word (Editor, keyword, kpos->start, kpos->end);
		/* no words found */
	}
	else if (g_list_length (klist) == 0)
	{
		g_free (ukey);
		print_msg ("Cannot complete: no matching keywords found...");
		return -2;
		/* go over the list and create the window */
	}
	else
	{
		kpos->Editor = Editor;
		show_completion_window (Editor, klist, kpos);
	}

	/* free up space */
	g_free (ukey);
	GList *list = klist;
	while (list)
	{
		g_free ((gchar *) list->data);
		list = g_list_next (list);
	}
	g_list_free (klist);

	return 0;
}
