
/*
** Beaver's an Early AdVanced EditoR
** (C) 1999-2000 Marc Bevand, Damien Terrier and Emmanuel Turquin
** (C) 2008 Tobias Heinzen, Double 12
** (C) 2008 Higor Eurípedes
**
** plugin.c
**
** Author<s>:   Tobias Heinzen
**				Higor Eurípedes
** Description:   Plugin system
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/** @file plugin.c
 * Plugin system
 */

#include <string.h>
#include <stdio.h>
#include <glib.h>
#include <glib/gprintf.h>
#include <gmodule.h>
#include <gtk/gtk.h>

#include "api/beaver.h"
#include "api/api.h"
#include "plugin.h"
#include "conf.h"
#include "main.h"
#include "../config.h"

/**
 * List of all found plugins with their information
 */
static GList *__found_plugins = NULL;

/**
 * Plugin Manager window (singleton)
 */
static GtkWidget *manager_dialog = NULL;

/* the preference button */
static GtkWidget *preference_button = NULL;

/* some labels */
static GtkWidget *label_author = NULL;
static GtkWidget *label_version = NULL;
static GtkWidget *label_descr = NULL;

/* the main notebook */
extern GtkWidget *MainNotebook;

/**
 * Frees an information pointer
 *
 * @param   plugin  information pointer
 */
static inline void plugin_info_free (plugin_info * plugin)
{
	/* do not free if not pointer */
	if (plugin == NULL)
		return;

	/* free all fields */
	if (plugin->name)
		g_free (plugin->name);
	if (plugin->description)
		g_free (plugin->description);
	if (plugin->author)
		g_free (plugin->author);
	if (plugin->version)
		g_free (plugin->version);
	if (plugin->filename)
		g_free (plugin->filename);

	/* free structure itself */
	g_free (plugin);
}

/**
 * Load information about plugin.
 *
 * This function loads some information (author, name, version) about
 * the plugin and returns a structure containing those information.
 *
 * @param   filename    the filename of a plugin, the path must be absolute
 * @return  returns the loaded information
 */
static inline plugin_info *plugin_info_load (gchar * filename)
{
	GModule *module = NULL;		/* module for g_module functions */

	/* function pointers for some functions in the
	 * plugin */
	__plugin_author_func __plugin_author;
	__plugin_version_func __plugin_version;
	__plugin_name_func __plugin_name;
	__plugin_description_func __plugin_description;

	/* result to return */
	plugin_info *plugin = g_new (plugin_info, 1);

	/* try to open shared object file */
	module = g_module_open (filename, G_MODULE_BIND_LOCAL);
	if (!module)
	{
		beaver_box_error ((gchar *) g_module_error ());
		g_free (plugin);
		return NULL;
	}

	/* try to load author information */
	if (g_module_symbol
		(module, "__plugin_author", (gpointer *) & __plugin_author))
	{
		gchar *string = __plugin_author ();
		plugin->author = g_strdup (string);
	}
	else
	{
		/* plugin has no author field */
		plugin->author = g_strdup ("U. N. Known");
	}

	/* try to load version information */
	if (g_module_symbol
		(module, "__plugin_version", (gpointer *) & __plugin_version))
	{
		gchar *string = __plugin_version ();
		plugin->version = g_strdup (string);
	}
	else
	{
		/* plugin has no version field */
		plugin->version = g_strdup ("0");
	}

	/* try to load name information */
	if (g_module_symbol (module, "__plugin_name", (gpointer *) & __plugin_name))
	{
		gchar *string = __plugin_name ();
		plugin->name = g_strdup (string);
	}
	else
	{
		/* plugin has no version field */
		plugin->name = g_strdup ("Unnamed");
	}

	/* try to load version information */
	if (g_module_symbol
		(module, "__plugin_description", (gpointer *) & __plugin_description))
	{
		gchar *string = __plugin_description ();
		plugin->description = g_strdup (string);
	}
	else
	{
		/* plugin has no version field */
		plugin->description = g_strdup ("");
	}

	/* unload module, we've got the information */
	if (!g_module_close (module))
	{
		/* something went wrong */
		plugin_info_free (plugin);
		return NULL;
	}

	/* set some dummy fields */
	plugin->activated = FALSE;
	plugin->cleanup_func = NULL;
	plugin->preference_func = NULL;
	plugin->module = NULL;

	/* save back filename */
	plugin->filename = g_strdup (filename);
	return plugin;
}

/**
 * Loads and initializes plugin.
 *
 * @param   info    a plugin info descriptor
 */
static inline void plugin_activate (plugin_info * info)
{
	/* if invalid info descriptor or plugin already activated, then
	 * ignore this. */
	if (!info || info->activated)
	{
		return;
	}

	/* function pointers for some functions in the
	 * plugin */
	__plugin_init_func __plugin_init;
	__plugin_cleanup_func __plugin_cleanup;
	__plugin_preference_func __plugin_preference;

	/* load the module */
	info->module = g_module_open (info->filename, G_MODULE_BIND_LOCAL);
	if (!info->module)
	{
		return;
	}

	/* try to load some functions */
	g_module_symbol (info->module, "__plugin_init",
					 (gpointer *) & __plugin_init);
	g_module_symbol (info->module, "__plugin_cleanup",
					 (gpointer *) & __plugin_cleanup);
	g_module_symbol (info->module, "__plugin_preference",
					 (gpointer *) & __plugin_preference);

	/* set the fields */
	info->activated = TRUE;
	info->cleanup_func = (void *) __plugin_cleanup;
	info->preference_func = NULL;
	if (__plugin_preference)
		info->preference_func = (void *) __plugin_preference;

	/* init */
	if (__plugin_init)
		__plugin_init ();
}

/**
 * Unloads and deactivates a plugin.
 *
 * @param   info    a plugin info descriptor
 */
static inline void plugin_deactivate (plugin_info * info)
{
	/* if invalid info descriptor or plugin already deactivated, then
	 * ignore this. */
	if (!info || !info->activated)
	{
		return;
	}

	/* clean up plugin */
	__plugin_cleanup_func __plugin_cleanup;
	__plugin_cleanup = (__plugin_cleanup_func) info->cleanup_func;
	if (__plugin_cleanup)
		__plugin_cleanup ();

	/* unload module and set fields */
	g_module_close (info->module);
	info->activated = FALSE;
}

/**
 * Loads and stores information about all plugins in a
 * specified directory.
 *
 * @param   directory   directory in which to search for plugins
 */
static inline void plugin_load_plugin_dir (gchar * directory)
{
	/* open directory */
	GDir *dir = g_dir_open (directory, 0, NULL);

	/* if everything went ok, then proceed */
	if (dir != NULL)
	{
		/* iterate through the directory */
		const gchar *plugin = NULL;
		while ((plugin = g_dir_read_name (dir)) != NULL)
		{
			gchar** extension = g_strsplit(plugin,".",2);
			if(!g_strcmp0(extension[1],"so"))
			{
				/* try to load the plugin */
				gchar *pfile = g_strconcat (directory, plugin, NULL);
				plugin_info *info = plugin_info_load (pfile);

				/* store the information about found plugin
				 * into a list */
				if (info)
				{
					__found_plugins =
						g_list_append (__found_plugins, (gpointer) info);

					/* if the plugin is marked as "loaded" in the config file
					 * then we should activate it */
					gchar *text = g_strconcat ("/Plugins/", info->filename, NULL);
					gchar *setting = get_string_conf (text);

					if (strncmp ("loaded", setting, 6) == 0)
					{
						plugin_activate (info);
					}

					/* free up space */
					g_free (text);
					g_free (setting);
				}

				/* free up space */
				g_free (pfile);
			}
			g_strfreev(extension);
		}

		/* close directory */
		g_dir_close (dir);
	}
}

/**
 * Sets function pointer to specific implementations.
 */
static inline void plugin_setup_api (void)
{
	/* box */
	beaver_box_message = beaver_box_message_impl;
	beaver_box_error = beaver_box_error_impl;
	beaver_box_question = beaver_box_question_impl;
	beaver_box_prompt = beaver_box_prompt_impl;
	beaver_box_file_selection = beaver_box_file_selection_impl;

	/* ui */
	beaver_ui_item_add = beaver_ui_item_add_impl;
	beaver_ui_item_seperator_add = beaver_ui_item_seperator_add_impl;
	beaver_ui_item_remove = beaver_ui_item_remove_impl;
	beaver_ui_item_submenu_add = beaver_ui_item_submenu_add_impl;

	/* text */
	beaver_text_insert_string = beaver_text_insert_string_impl;
	beaver_text_selection_position = beaver_text_selection_position_impl;
	beaver_text_selection_get = beaver_text_selection_get_impl;
	beaver_text_selection_set = beaver_text_selection_set_impl;
	beaver_text_format = beaver_text_format_impl;
	beaver_text_replace = beaver_text_replace_impl;
	beaver_text_find = beaver_text_find_impl;

#ifdef REGEX_SUPPORT
	beaver_text_find_regex = beaver_text_find_regex_impl;
	beaver_text_replace_regex = beaver_text_replace_regex_impl;
#endif
}

/**
 *
 * @param   data        pointer to a plugin_info structure
 * @param   user_data   pointer to a GtkListStore structure
 */
static void plugin_list_add (gpointer data, gpointer user_data)
{
	GtkListStore *store = (GtkListStore *) user_data;
	plugin_info *info = (plugin_info *) data;
	GtkTreeIter iter;

	if (info)
	{
		gtk_list_store_append (store, &iter);
		gtk_list_store_set (store, &iter, 1, info->name, 2, info->filename, 3,
							(gint) info, -1);

		/* set the toggle button correctly (if plugin
		 * was loaded or not) */
		gtk_list_store_set (store, &iter, 0, info->activated, -1);
	}
}

/**
 *
 */
static void signal_toggle (GtkCellRendererToggle * cell_renderer, gchar * spath,
						   gpointer user_data)
{
	GtkTreeModel *model = GTK_TREE_MODEL (user_data);
	GtkTreePath *path = gtk_tree_path_new_from_string (spath);
	gint plugin;
	GtkTreeIter it;
	gboolean toggle;

	/* get the state of the toggle button */
	gtk_tree_model_get_iter (model, &it, path);
	gtk_tree_model_get (model, &it, 0, &toggle, -1);
	gtk_tree_model_get (model, &it, 3, &plugin, -1);

	/* if the toggle button was activated, then a click means
	 * that it's getting deactivated.
	 *
	 * if it's activated, then unload this plugin. otherwise
	 * we have to activate this plugin */
	/* deactivate plugin */
	if (toggle)
	{
		plugin_deactivate ((plugin_info *) plugin);

		/* mark this plugin as unloaded in the config file */
		gchar *text =
			g_strconcat ("/Plugins/", ((plugin_info *) plugin)->filename, NULL);
		set_string_conf (text, "unloaded");
		g_free (text);
		/* deactivate plugin */
	}
	else
	{
		plugin_activate ((plugin_info *) plugin);

		/* mark this plugin as loaded in the config file */
		gchar *text =
			g_strconcat ("/Plugins/", ((plugin_info *) plugin)->filename, NULL);
		set_string_conf (text, "loaded");
		g_free (text);
	}

	/* really toggle the checkbox */
	toggle ^= 1;
	gtk_list_store_set (GTK_LIST_STORE (model), &it, 0, toggle, -1);

	/* free up space */
	gtk_tree_path_free (path);
}

/**
 * @param   tree_view   the treeview that received signal
 * @param   user_data   pointer to data that was given at signal registration
 */
static void signal_activate (GtkTreeView * tree_view, gpointer user_data)
{
	GtkTreeIter iter;
	GtkTreeModel *model;
	gint plugin = 0;
	plugin_info *info;

	/* get the selected row and read the plugin descriptor out
	 * of it if possible */
	GtkTreeSelection *selection = gtk_tree_view_get_selection (tree_view);
	if (selection
		&& gtk_tree_selection_get_selected (GTK_TREE_SELECTION (selection),
											&model, &iter))
	{
		gtk_tree_model_get (model, &iter, 3, &plugin, -1);
	}

	/* read the information out of the plugin descriptor */
	info = (plugin_info *) plugin;
	if (!info)
	{
		/* nothing to display */
		gtk_label_set_text (GTK_LABEL (label_author), "");
		gtk_label_set_text (GTK_LABEL (label_version), "");
		gtk_label_set_text (GTK_LABEL (label_descr), "");
		//gtk_label_set_text (label, "Author:\nVersion:\nDescription:");
	}
	else
	{
		/*gchar* text = g_strdup_printf ("Author:\t\t%s\nVersion:\t\t%s\nDescription:\t%s",
		   info->author, info->version, info->description); */
		gtk_label_set_text (GTK_LABEL (label_author), info->author);
		gtk_label_set_text (GTK_LABEL (label_version), info->version);
		gtk_label_set_text (GTK_LABEL (label_descr), info->description);

		if (info->preference_func && info->activated)
		{
			gtk_widget_set_sensitive (preference_button, TRUE);
		}
		else
		{
			gtk_widget_set_sensitive (preference_button, FALSE);
		}

		/*gtk_label_set_text (label, text);
		   g_free (text); */
	}
}

/**
 * @param   button      the button that got the clicked event
 * @param   user_data   pointer to data that was given at signal registration
 */
static void signal_preference (GtkButton * button, gpointer user_data)
{
	GtkTreeView *tree_view = GTK_TREE_VIEW (user_data);
	GtkTreeIter iter;
	GtkTreeModel *model;
	gint plugin = 0;
	plugin_info *info;

	/* get the selected row and read the plugin descriptor out
	 * of it if possible */
	GtkTreeSelection *selection = gtk_tree_view_get_selection (tree_view);
	if (selection
		&& gtk_tree_selection_get_selected (GTK_TREE_SELECTION (selection),
											&model, &iter))
	{
		gtk_tree_model_get (model, &iter, 3, &plugin, -1);
	}

	/* read the information out of the plugin descriptor */
	info = (plugin_info *) plugin;
	if (info && info->preference_func)
	{
		__plugin_preference_func preferences =
			(__plugin_preference_func) info->preference_func;
		preferences ();
	}
}

/**
 * Creates and shows a dialog to manage the plugins.
 */
static void plugin_manage (void)
{
	/* if manager dialog wasn't created before, then create it */
	if (!manager_dialog)
	{
		GtkWidget *vbox;

		GtkWidget *top_label;
		GtkWidget *scroll;
		GtkWidget *plugins_list;
		GtkWidget *button;
		GtkWidget *table;
		GtkCellRenderer *renderer_text;
		GtkCellRenderer *renderer_toggle;

		GtkListStore *store;

		/* create a new dialog */
		manager_dialog = gtk_dialog_new ();
		gtk_window_set_modal (GTK_WINDOW (manager_dialog), TRUE);
		gtk_window_set_default_size (GTK_WINDOW (manager_dialog), 500, 260);
		gtk_window_set_transient_for (GTK_WINDOW (manager_dialog),
									  GTK_WINDOW (gtk_widget_get_toplevel
												  (GTK_WIDGET (MainNotebook))));
		gtk_window_set_title (GTK_WINDOW (manager_dialog),
							  APP_NAME " - Plugin Manager");

		/* create a preference button (set unsensitive) */
		preference_button = gtk_button_new_from_stock (GTK_STOCK_PREFERENCES);
		gtk_widget_set_sensitive (preference_button, FALSE);

		/* we need four vertical boxes:
		 *  - top label
		 *  - tree view (plugin list)
		 *  - table (containing more infos about
		 *    plugins)
		 */
		vbox = gtk_vbox_new (FALSE, 3);
		top_label = gtk_label_new ("Available plugins: ");
		plugins_list = gtk_tree_view_new ();
		scroll = gtk_scrolled_window_new (NULL, NULL);

		gtk_misc_set_alignment (GTK_MISC (top_label), 0, 0.3);
		gtk_label_set_single_line_mode (GTK_LABEL (top_label), TRUE);
		gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scroll),
										GTK_POLICY_AUTOMATIC,
										GTK_POLICY_ALWAYS);

		/* create a 3x3 table. one row is for the author, one
		 * row for the version and one for the description.
		 * the third column is empty except for the last row, were
		 * there is a preference button */
		table = gtk_table_new (3, 3, FALSE);
		gtk_table_set_row_spacings (GTK_TABLE (table), 5);
		gtk_table_set_col_spacings (GTK_TABLE (table), 5);
		gtk_table_set_homogeneous (GTK_TABLE (table), FALSE);
		gtk_container_set_border_width (GTK_CONTAINER (table), 10);

		/* populate the virtual box */
		gtk_container_add (GTK_CONTAINER (GTK_DIALOG (manager_dialog)->vbox),
						   vbox);
		gtk_container_add (GTK_CONTAINER (vbox), top_label);
		gtk_container_add (GTK_CONTAINER (vbox), scroll);
		gtk_container_add (GTK_CONTAINER (vbox), table);
		gtk_widget_show_all (vbox);

		/* populate table */
		GtkWidget *label = gtk_label_new ("Author: ");
		label_author = gtk_label_new ("");
		gtk_misc_set_alignment (GTK_MISC (label), 0, 0.3);
		gtk_misc_set_alignment (GTK_MISC (label_author), 0, 0.3);
		gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, 0, 1);
		gtk_table_attach_defaults (GTK_TABLE (table), label_author, 1, 2, 0, 1);

		label = gtk_label_new ("Version: ");
		label_version = gtk_label_new ("");
		gtk_misc_set_alignment (GTK_MISC (label), 0, 0.3);
		gtk_misc_set_alignment (GTK_MISC (label_version), 0, 0.3);
		gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, 1, 2);
		gtk_table_attach_defaults (GTK_TABLE (table), label_version, 1, 2, 1,
								   2);

		label = gtk_label_new ("Description: ");
		label_descr = gtk_label_new ("");
		gtk_label_set_line_wrap (GTK_LABEL (label_descr), TRUE);
		gtk_label_set_line_wrap_mode (GTK_LABEL (label_descr), PANGO_WRAP_WORD);
		gtk_misc_set_alignment (GTK_MISC (label), 0, 0.3);
		gtk_misc_set_alignment (GTK_MISC (label_descr), 0, 0.3);
		gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, 3, 4);
		gtk_table_attach_defaults (GTK_TABLE (table), label_descr, 1, 2, 3, 4);
		gtk_table_attach_defaults (GTK_TABLE (table), preference_button, 3, 4,
								   3, 4);

		/* show table */
		gtk_widget_show_all (table);

		/* add ok button */
		button = gtk_dialog_add_button (GTK_DIALOG (manager_dialog), "OK", 1);

		/* add all items into a store list */
		store =
			gtk_list_store_new (4, G_TYPE_BOOLEAN, G_TYPE_STRING, G_TYPE_STRING,
								G_TYPE_INT);
		g_list_foreach (__found_plugins, plugin_list_add, (gpointer) store);

		/* finish setting up store list and display it */
		renderer_text = gtk_cell_renderer_text_new ();
		renderer_toggle = gtk_cell_renderer_toggle_new ();

		gtk_cell_renderer_toggle_set_radio (GTK_CELL_RENDERER_TOGGLE
											(renderer_toggle), FALSE);
		gtk_cell_renderer_toggle_set_active (GTK_CELL_RENDERER_TOGGLE
											 (renderer_toggle), TRUE);

		gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW
													 (plugins_list), -1,
													 "Active", renderer_toggle,
													 "active", 0, NULL);
		gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW
													 (plugins_list), -1,
													 "Plugin", renderer_text,
													 "text", 1, NULL);
		gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW
													 (plugins_list), -1, "File",
													 renderer_text, "text", 2,
													 NULL);

		gtk_tree_view_set_model (GTK_TREE_VIEW (plugins_list),
								 GTK_TREE_MODEL (store));
		gtk_container_add (GTK_CONTAINER (scroll), plugins_list);
		g_object_unref (GTK_TREE_MODEL (store));
		gtk_widget_show (plugins_list);

		/* register some signals */
		g_signal_connect (G_OBJECT (renderer_toggle), "toggled",
						  GTK_SIGNAL_FUNC (signal_toggle), store);
		g_signal_connect (G_OBJECT (plugins_list), "cursor-changed",
						  GTK_SIGNAL_FUNC (signal_activate), NULL);
		g_signal_connect (G_OBJECT (manager_dialog), "delete-event",
						  GTK_SIGNAL_FUNC (gtk_widget_hide_on_delete),
						  manager_dialog);
		g_signal_connect (G_OBJECT (manager_dialog), "close",
						  GTK_SIGNAL_FUNC (gtk_widget_hide), manager_dialog);
		g_signal_connect (G_OBJECT (manager_dialog), "response",
						  GTK_SIGNAL_FUNC (gtk_widget_hide), manager_dialog);
		g_signal_connect (G_OBJECT (preference_button), "clicked",
						  GTK_SIGNAL_FUNC (signal_preference), plugins_list);

	}

	/* show dialog */
	gtk_widget_show (GTK_WIDGET (manager_dialog));
}

/**
 * Initialize plugin system.
 *
 * Sets all functions pointers to specific implementations for
 * the API used in the plugins. function pointers are defined
 * in api/beaver.h. Also loads all activated plugins and
 * initializes them too and loads information about all
 * found plugins.\n
 * Plugins can be in the home directory "~/.beaver/plugins/" or
 * in the installation directory.
 */
void plugin_init (void)
{
	/* check wheter module loading is supported or not */
	if (!g_module_supported ())
	{
		return;
	}

	/* directory of home installed plugins */
	gchar *home_plugin_dir = g_strconcat (g_get_home_dir (), PATH_SEP_STRING,
										  CONF_DIR, PATH_SEP_STRING, "plugins",
										  PATH_SEP_STRING, NULL);

	/* setup list */
	__found_plugins = g_list_alloc ();

	/* fill up the function pointers for the API */
	plugin_setup_api ();

	/* add a menu item into tools section, that starts the
	 * plugin manager */
	beaver_ui_item_add (BEAVER_SECTION_MENU_TOOLS, "manager", GTK_STOCK_EXECUTE,
						"Plugins ...", plugin_manage);
	beaver_ui_item_seperator_add (BEAVER_SECTION_MENU_TOOLS);

	/* check if in the home directory are some plugins to load
	 * information from, and load them. */
	plugin_load_plugin_dir (home_plugin_dir);

	/* check if in the installation directory are some plugins
	 * to load information from, and load them */
	plugin_load_plugin_dir (PLUGINS_DIR);

	/* free up space */
	g_free (home_plugin_dir);
}
