
/*
** Beaver's an Early AdVanced EditoR
** (C) 1999-2000 Marc Bevand, Damien Terrier and Emmanuel Turquin, 2008 Tobias Heinzen, Double 12
**
** interface.h
**
** Author<s>:   Emmanuel Turquin (aka "Ender") <turqui_e@epita.fr>
**              Michael Terry <mterry@fastmail.fm>
**				Tobias Heinzen
**				Double 12
**
** Description:   Beaver interface source header
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef __INTERFACE_H__
#define __INTERFACE_H__

/**
 * defines how many lines there are in a page. this
 * line count does not include header/footer of a
 * page.
 */
#define LINES_PER_PAGE	55

/**
 * Struct containing all the information about 
 * the document needed when printing 
 */
typedef struct
{
	gint n_pages;				/* total number of pages */
	gint chars_printed;			/* the ammount of characters that already have
								   been printed */
	gchar *content;				/* the original content (text) 
								   of the document */
	GtkTextBuffer *textbuffer;
	gchar *name;				/* the name of the file that is printed. is
								   shown in the header of the printed 
								   document */
} PrintDocument;


/* PROTOTYPE */
void interface (gint argc, gchar * argv[]);
void refresh_interface (void);
void calculate_internal_tab_width (GtkTextView * view);

/* DEPRECATED */
void menu_items_treatment (GtkWidget * Widget, gpointer Op_in);
void menu_set_sensitive (const gchar * id, gboolean setting);

extern GtkWidget *MainWindow;
extern GtkItemFactory *MainFactory;
enum
{
	NEW = 0,
	OPEN,
	SAVE,
	SAVE_ALL,
	SAVE_AS,
	PRINT,
	CLOSE,
	CLOSE_ALL,
	QUIT,
	UNDO,
	REDO,
	CUT,
	COPY,
	PASTE,
	CLEAR,
	SELECT_ALL,
	COMPLETE,
	FIND,
	REPLACE,
	LINE,
	READONLY,
	CONVERTER,
	COLOR,
	INSERT_TIME,
	TO_UPPER,
	TO_LOWER,
	CAPITALIZE,
	INVERT_CASE,
	TO_UNIX,
	TO_DOS,
	TO_MAC,
	TO_UNIX_ALL,
	TO_DOS_ALL,
	TO_MAC_ALL,
	DOS_UNIX,
	UNIX_DOS,
	DOS_MAC,
	MAC_DOS,
	MAC_UNIX,
	UNIX_MAC,
	DOS_UNIX_ALL,
	UNIX_DOS_ALL,
	DOS_MAC_ALL,
	MAC_DOS_ALL,
	MAC_UNIX_ALL,
	UNIX_MAC_ALL,
	FILE_INFO,
	TOOLBAR,
	MSGBAR,
	WORDWRAP,
	TAB_POS_TOP,
	TAB_POS_BOTTOM,
	TAB_POS_LEFT,
	TAB_POS_RIGHT,
	SCROLLBAR_POS_LEFT,
	SCROLLBAR_POS_RIGHT,
	PREFS,
	LANG,
	HELP,
	ABOUT,
	NUM_MENU_ACTIONS,
	RECENT_FILES_OFFSET
};

#endif
