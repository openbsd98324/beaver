
/*
** Beaver's an Early AdVanced EditoR
** (C) 1999-2000 Marc Bevand, Damien Terrier and Emmanuel Turquin, 2008 Tobias Heinzen, Double 12
**
** main.c
**
** Author<s>:     Emmanuel Turquin (aka "Ender") <turqui_e@epita.fr>
**				  Tobias Heinzen
**				  Double 12
** Description:   Beaver main source file
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/** @file	main.c
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <gtk/gtk.h>
#include "interface.h"
#include "main.h"
#include "conf.h"
#include "../config.h"
#include "api/api.h"

/**
 * 
 */
static void parse_commandline (int argc, char *argv[])
{
	gboolean include_dir = FALSE;

	GOptionEntry options[] = {
		{
		 "include-dir",	/* long name */
		 'i',	/* short name */
		 G_OPTION_FLAG_HIDDEN,	/* flag */
		 G_OPTION_ARG_NONE,	/* got argument? */
		 &include_dir,	/* argument pointer, gboolean if no argument */
		 "returns the directory where the include " "files are installed! needed for plugin " "development.",	/* description */
		 NULL	/* argument description */
		 },
		{NULL}
	};

	GOptionContext *ctx = g_option_context_new ("[FILENAME] ...");
	g_option_context_add_main_entries (ctx, options, "beaver");
	g_option_context_parse (ctx, &argc, &argv, NULL);
	g_option_context_free (ctx);

	if (include_dir)
	{
		printf (INCLUDE_DIR "\n");
		exit (0);
	}
}

/**
 * Main routine.
 */
int main (int argc, char *argv[])
{
	/* parse command line options */
	parse_commandline (argc, argv);

	/* initsialize config */
	init_conf ();

	/* load the wordfile (for the syhi) into the
	 * editor. also make sure the wordfile exists.
	 */
	struct stat Stats;
	gchar *WordPath = g_strconcat (g_get_home_dir (), PATH_SEP_STRING,
								   CONF_DIR, PATH_SEP_STRING, "wordfile.txt",
								   NULL);
	if (stat (WordPath, &Stats) == -1)
	{
		/* try to copy the wordfile from the resources. if
		 * it fails, we can't proceed
		 */
		gchar *command =
			g_strdup_printf
			("cp %s/wordfile.default %s 2>/dev/null 1>/dev/null",
			 RESOURCE_DIR, WordPath);

		if (system (command))
		{
			printf ("Error in writing wordfile!\n");
		}

		/* free up space */
		g_free (command);
	}

	/* free up space */
	g_free (WordPath);

	/* startup the interface */
	interface (argc, argv);

	/* probably never called */
	return 0;
}
