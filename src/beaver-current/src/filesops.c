
/*
** Beaver's an Early AdVanced EditoR
** (C) 1999-2000 Marc Bevand, Damien Terrier and Emmanuel Turquin, 2008 Tobias Heinzen, Double 12
**
** filesops.c
**
** Author<s>:     Emmanuel Turquin (aka "Ender") <turqui_e@epita.fr>
**                Michael Terry <mterry@fastmail.fm>
** 				  Tobias Heinzen 
**
** Description:   Files operations source
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <gtk/gtk.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include "main.h"
#include "editor.h"
#include "struct.h"
#include "interface.h"
#include "msgbar.h"
#include "conf.h"
#include "prefs.h"
#include "filesops.h"
#include "undoredo.h"


#define OPEN_FILE       0
#define SAVE_FILE_AS    1
#define QUIT_FILE       0
#define CLOSE_FILE      1
#define CLOSE_ALL_FILE  2


extern GtkWidget *MainNotebook;
extern GArray *FileProperties;
extern t_settings Settings;
extern gint OpenedFilesCnt;
extern gint NewFilesCnt;
static gboolean FileSelectorIsVisible = FALSE;
//static gboolean QuestionIsVisible = FALSE;
//static gint AutosaveSig = 0;

/* This function returns 'string' with all instances
   of 'obj' replaced with instances of 'replacement'
   It modifies string and re-allocs it.
   */
gchar *str_replace_tokens (gchar ** string, gchar obj, gchar * replacement)
{
	gchar *p;
	gint rsize = strlen (replacement);
	gint osize = 1;
	gint diff = rsize - osize;

	p = *string;
	while ((p = strchr (p, obj)))
	{
		*string = g_realloc (*string, strlen (*string) + diff + 1);
		g_memmove (p + rsize, p + osize, strlen (p + osize) + 1);

		memcpy (p, replacement, rsize);

		p = p + rsize;
	}

	return *string;
}

/* This function is used by 'init_file_properties' to set the base filename
   and the type of the file. */

const gchar *str_get_last_part (const gchar * String, gchar Separator,
								gboolean ReturnIfSeparatorNotFound)
{
	const gchar *LastPart;

	LastPart = strrchr (String, Separator);
	if (LastPart)
		LastPart += 1;
	else
		LastPart = String;

	if (!ReturnIfSeparatorNotFound && !LastPart)
		return "";
	return LastPart;
}


/* Return the absolute path of the file 'FileName' */

gchar *get_absolute_path (const gchar * FileName)
{
	gchar *TempFileName, *AbsolutePath;
	gchar **Tab;
	gint i = 0;

	if (g_path_is_absolute (FileName))
		return (strdup (FileName));
	TempFileName =
		g_strconcat (g_get_current_dir (), PATH_SEP_STRING, FileName, NULL);
	Tab = g_strsplit (TempFileName, PATH_SEP_STRING, 0);
	g_free (TempFileName);
	while (Tab[i] != NULL)
	{
		if (!strcmp (Tab[i], ".."))
		{
			gint j;

			for (j = i; Tab[j] != NULL; j++)
			{
				Tab[j - 1] = Tab[j + 1];
			}
			i--;
		}
		else
			i++;
	}
	AbsolutePath = g_strjoinv (PATH_SEP_STRING, Tab);
	g_strfreev (Tab);
	return (AbsolutePath);
}


/* This function is called when a text has changed (since last save) */

void buffer_changed (GtkTextBuffer * Buffer, gpointer user_data)
{
	refresh_interface ();

/*  gint CurrentPage;
  gboolean saveable;

#ifdef DEBUG_FCN
  g_print(__FILE__": %s(): Begin\n", __func__);
#endif
  CurrentPage = gtk_notebook_get_current_page (GTK_NOTEBOOK(MainNotebook));
  saveable = gtk_text_buffer_get_modified (FPROPS(CurrentPage, Buffer));
  set_label (GTK_NOTEBOOK(MainNotebook), CurrentPage);
  
  note_saveable ();
  
  print_msg("File has been modified...");
#ifdef DEBUG_FCN
  g_print(__FILE__": %s(): End\n", __func__);
#endif*/
}


/* Init the selected file properties */

void init_file_properties (const gchar * FileName, gint CurrentPage)
{
	struct stat Stats;

	FPROPS (CurrentPage, Name) = g_strdup (FileName);
	FPROPS (CurrentPage, BaseName) = g_path_get_basename (FileName);
	FPROPS (CurrentPage, Type) =
		g_strdup (str_get_last_part
				  (FPROPS (CurrentPage, BaseName), '.', FALSE));
	if (stat (FileName, &Stats) != -1)
	{
		FILE *File;

		if (((File = fopen (FPROPS (CurrentPage, Name), "a")) != NULL))
		{
			FPROPS (CurrentPage, ReadOnly) = 0;
			fclose (File);
		}
		else
		{
			FPROPS (CurrentPage, ReadOnly) = -1;
		}
	}
	else
		FPROPS (CurrentPage, ReadOnly) = 0;
	stat (FileName, &FPROPS (CurrentPage, Stats));

	FPROPS (CurrentPage, WidgetInfo.current_action) = NULL;
	FPROPS (CurrentPage, WidgetInfo.Lg) = -1;
	FPROPS (CurrentPage, WidgetInfo.stack) = NULL;
	FPROPS (CurrentPage, WidgetInfo.in_user_action) = FALSE;
	FPROPS (CurrentPage, WidgetInfo.undoredo_toggler) = FALSE;
	FPROPS (CurrentPage, WidgetInfo.undoredo_activated) = FALSE;
	FPROPS (CurrentPage, Format) = UNIX;	/* defaults to unix */
}


/* Return the rwx permissions of a file in a string */

gchar *get_file_mode (struct stat Stats)
{
	static gchar Mode[10];

	g_snprintf (Mode, 10, "%c%c%c%c%c%c%c%c%c",
				(Stats.st_mode & S_IRUSR) ? 'r' : '-',
				(Stats.st_mode & S_IWUSR) ? 'w' : '-',
				(Stats.st_mode & S_IXUSR) ? 'x' : '-',
				(Stats.st_mode & S_IRGRP) ? 'r' : '-',
				(Stats.st_mode & S_IWGRP) ? 'w' : '-',
				(Stats.st_mode & S_IXGRP) ? 'x' : '-',
				(Stats.st_mode & S_IROTH) ? 'r' : '-',
				(Stats.st_mode & S_IWOTH) ? 'w' : '-',
				(Stats.st_mode & S_IXOTH) ? 'x' : '-');
	return (Mode);
}


/* Update the recent files by putting a new file in the list */

/* these entries represent the entries after the recent files section that we modify */
static GtkItemFactoryEntry EndingEntries[] = {
	{"/File/sep_end", NULL, NULL, 0, "<Separator>"},
	{"/File/_Close", "<control>W", menu_items_treatment, CLOSE, "<StockItem>",
	 GTK_STOCK_CLOSE},
	{"/File/_Quit", "<control>Q", menu_items_treatment, QUIT, "<StockItem>",
	 GTK_STOCK_QUIT}
};

void init_recent_files (void)
{
#ifdef DEBUG_FCN
	g_print (__FILE__ ": %s(): Begin\n", __func__);
#endif
	/*display_recent_files (); */
#ifdef DEBUG_FCN
	g_print (__FILE__ ": %s(): End\n", __func__);
#endif
}


void put_recent_file (const gchar * FileName)
{
	gint i;
	gchar *last_val;

#ifdef DEBUG_FCN
	g_print (__FILE__ ": %s(): Begin\n", __func__);
#endif
	last_val = g_strdup (FileName);
	for (i = 1; i <= RECENT_FILES_MAX_NB; i++)
	{
		gchar *this_key;
		gchar *this_val;

		this_key = g_strdup_printf ("General/RecentFiles/File%d", i);
		this_val = get_string_conf (this_key);

		set_string_conf (this_key, last_val);
		g_free (last_val);

		if (strcmp (FileName, this_val))
			last_val = this_val;
		else
			break;
	}

	/*display_recent_files (); */

#ifdef DEBUG_FCN
	g_print (__FILE__ ": %s(): End\n", __func__);
#endif
}


/* Used to put the most recently opened files in a menu */

void display_recent_files (void)
{
	gint i = 0, displayed = 0;
	gboolean done = FALSE;
	gboolean close_sensitive;
	GtkWidget *item;

	START_FCN
		/* first, get rid of old entries (plus close and quit) */
		i = RECENT_FILES_OFFSET;
	while ((item = gtk_item_factory_get_item_by_action (MainFactory, ++i)))
	{
		const gchar *path = gtk_item_factory_path_from_widget (item);
		gtk_item_factory_delete_item (MainFactory, path);
	}

	close_sensitive =
		GTK_WIDGET_SENSITIVE (gtk_item_factory_get_item
							  (MainFactory, "/File/Close"));
	gtk_item_factory_delete_entries (MainFactory, 3, EndingEntries);

	/* now add new entries */
	for (i = 1; i <= RECENT_FILES_MAX_NB && !done; i++)
	{
		gchar *conf_val, *conf_key;

		conf_key = g_strdup_printf ("General/RecentFiles/File%d", i);
		conf_val = get_string_conf (conf_key);

		str_replace_tokens (&conf_val, '_', "__");

		if (strcmp (conf_val, ""))
		{
			gchar *base = g_path_get_basename (conf_val);
			gchar *menu_title = g_strdup_printf ("/File/_%d %s", i,
												 base);
			GtkItemFactoryEntry NewEntry = {
				menu_title, NULL,
				(GtkItemFactoryCallback) open_recent_file,
				RECENT_FILES_OFFSET + i, "<Item>"
			};
			gtk_item_factory_create_items (MainFactory, 1, &NewEntry, NULL);
			g_free (base);
			g_free (menu_title);

			displayed++;
		}
		else
			done = TRUE;

		g_free (conf_key);
		g_free (conf_val);
	}

	/* and add back the close and quit items */
	if (displayed > 0)
		gtk_item_factory_create_items (MainFactory, 3, EndingEntries, NULL);
	else
		gtk_item_factory_create_items (MainFactory, 2, &EndingEntries[1], NULL);

	gtk_widget_set_sensitive (gtk_item_factory_get_item
							  (MainFactory, "/File/Close"), close_sensitive);

END_FCN}


/* Callback function for the Recent files menu */

void open_recent_file (GtkWidget * DummyWidget, guint i)
{
	struct stat Stats;
	gchar *FileName, *conf_key;

	START_FCN i = i - RECENT_FILES_OFFSET;

	conf_key = g_strdup_printf ("General/RecentFiles/File%d", i);
	FileName = get_string_conf (conf_key);

	if (stat (FileName, &Stats) == -1)
	{
		print_msg ("This file doesn't exist anymore...");
		return;
	}
	else
	{
		open_filename (FileName);
	}
	(void) DummyWidget;	/* avoid the "unused parameter" warning */
	g_free (conf_key);
	g_free (FileName);

END_FCN}


/* Enable/Disable Autosave */

void autosave (gint Delay)
{

/*  if (Delay)
    {
      if (AutosaveSig)
	{
	  gtk_timeout_remove (AutosaveSig);
	}
      AutosaveSig = gtk_timeout_add (1000 * Delay,
				     (GtkFunction)save_all,
				     NULL);
    }
  else
    if (AutosaveSig)
      {
	gtk_timeout_remove (AutosaveSig);
	AutosaveSig = 0;
      }*/
}


void file_selection_window_not_visible (void)
{
	FileSelectorIsVisible = FALSE;
}


/* actually does the work of opening a file */
void open_filename (const gchar * filename)
{
//  gint CurrentPage;
	struct stat Stats;
	gchar *dir;

	START_FCN if (stat (filename, &Stats) == -1)
		return;
	if ((Stats.st_mode & S_IFMT) == S_IFDIR)
		return;

	g_free (DIRECTORY);
	dir = g_path_get_dirname (filename);
	DIRECTORY = g_strconcat (dir, PATH_SEP_STRING, NULL);
	g_free (dir);
	set_string_conf ("General/RecentFiles/Directory", DIRECTORY);

	/*put_recent_file (filename); */

//  add_page_in_notebook (GTK_NOTEBOOK(MainNotebook), filename);
	open_file_in_editor (GTK_WIDGET (FPROPS (OpenedFilesCnt - 1, Text)),
						 filename, OpenedFilesCnt - 1);
	if (FPROPS (OpenedFilesCnt - 1, ReadOnly))
		print_msg (g_strconcat
				   ("File \"", FPROPS (OpenedFilesCnt - 1, BaseName),
					"\" opened in Readonly mode...", NULL));
	else
		print_msg (g_strconcat
				   ("File \"", FPROPS (OpenedFilesCnt - 1, BaseName),
					"\" opened...", NULL));
	/* Init undo/redo */
	init_file_properties (filename, OpenedFilesCnt - 1);
	init_undoredo ();	/* reinitsialize undo/redo */

END_FCN}
