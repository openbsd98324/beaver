
/*
** Beaver's an Early AdVanced EditoR
** (C) 1999-2000 Marc Bevand, Damien Terrier and Emmanuel Turquin
** (C) 2008 Tobias Heinzen, Double 12
** (C) 2008 Higor Eurípedes
**
** search.c
**
** Author<s>:     Emmanuel Turquin (aka "Ender") <turqui_e@epita.fr
**				  Tobias Heinzen
**				  Higor Eurípedes
** Description:   Beaver search & replace functions source
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/** @file search.c
 * dialogs for searching and replacing
 */

#include <gtk/gtk.h>
#include <string.h>
#include "search.h"
#include "struct.h"
#include "api/beaver.h"

/* statics */
static GtkWidget *search_window = NULL;
static GtkWidget *goto_window = NULL;

static GtkWidget *frame_search = NULL;
static GtkWidget *frame_results = NULL;
static GtkWidget *frame_replace = NULL;

static GtkWidget *entry_search = NULL;
static GtkWidget *entry_replace = NULL;
static GtkWidget *entry_line = NULL;

static GtkWidget *button_find = NULL;

static GtkWidget *results_treeview = NULL;

static GtkWidget *check_case_sensitive = NULL;
static GtkWidget *check_replace_all = NULL;
static GtkWidget *check_start_beginning = NULL;
static GtkWidget *check_start_cursor = NULL;

#ifdef REGEX_SUPPORT
static GtkWidget *check_regex_mode = NULL;
#endif

/* the main notebook (from interface.c) */
extern GtkWidget *MainNotebook;

/* how many files are open (from interface.c) */
extern gint OpenedFilesCnt;

/* file properties (from interface.c) */
extern GArray *FileProperties;

/**
 * signal handler when a row on the treeview is dbl-clicked
 *
 * @param   treeview    the treeview the event occured
 * @param   path
 * @param   column      the column on the treeview
 * @param   data        data provided at signal registering time
 */
static void signal_row_activated (GtkTreeView * treeview, GtkTreePath * path,
								  GtkTreeView * column, gpointer data)
{
	/* get the selected data */
	GtkTreeIter iter;
	GtkTreeModel *model;
	gint offset = 0;
	gint length = 0;

	GtkTreeSelection *selection =
		gtk_tree_view_get_selection (GTK_TREE_VIEW (treeview));
	if (selection
		&& gtk_tree_selection_get_selected (GTK_TREE_SELECTION (selection),
											&model, &iter))
	{
		gtk_tree_model_get (model, &iter, 1, &offset, -1);
		gtk_tree_model_get (model, &iter, 2, &length, -1);
	}
	else
	{
		return;
	}

	/* get the current page that should be edited */
	gint current_page =
		gtk_notebook_get_current_page (GTK_NOTEBOOK (MainNotebook));

	/* get the text buffer */
	GtkTextBuffer *buffer = FPROPS (current_page, Buffer);

	/* mark found position */
	GtkTextIter start, end;
	gtk_text_buffer_get_selection_bounds (buffer, &start, &end);
	gtk_text_iter_set_offset (&start, offset - 1);
	gtk_text_iter_set_offset (&end, offset + length - 1);
	gtk_text_buffer_select_range (buffer, &start, &end);

	/* scroll the text to the right position */
	gtk_text_view_scroll_to_iter (GTK_TEXT_VIEW (FPROPS (current_page, Text)),
								  &start, 0.1, FALSE, 0, 0);
}

/**
 * called when pressed on goto line button
 *
 * @param   button  the button that was pressed
 * @param   data    user defined data
 */
static void jump_to (GtkButton * button, gpointer data)
{
	/* retrieve line number */
	gint line = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (entry_line));

	/* hide goto window */
	gtk_widget_hide (goto_window);

	/* get the current page that should be edited */
	gint current_page =
		gtk_notebook_get_current_page (GTK_NOTEBOOK (MainNotebook));

	/* get the text buffer */
	GtkTextBuffer *buffer = FPROPS (current_page, Buffer);

	/* mark line */
	GtkTextIter start;
	gtk_text_buffer_get_iter_at_line (buffer, &start, line - 1);
	gtk_text_buffer_place_cursor (buffer, &start);

	/* scroll window to position */
	gtk_text_view_scroll_to_iter (GTK_TEXT_VIEW (FPROPS (current_page, Text)),
								  &start, 0.1, FALSE, 0, 0);
}

/**
 * called when pressed on find button
 *
 * @param   button  the button that was pressed
 * @param   data    user defined data
 */
static void find (GtkButton * button, gpointer data)
{
	const gchar *text = gtk_entry_get_text (GTK_ENTRY (entry_search));

	/* has the text box some value? if not display message and
	 * abort */
	if (strcmp (text, "") == 0)
	{
		beaver_box_error ("Please provide a search string!");
		return;
	}

	/* get the current page that should be edited */
	gint current_page =
		gtk_notebook_get_current_page (GTK_NOTEBOOK (MainNotebook));

	/* get the text buffer */
	GtkTextBuffer *buffer = FPROPS (current_page, Buffer);

	/* try to load cursors from selection */
	GtkTextIter start, end;
	gtk_text_buffer_get_selection_bounds (buffer, &start, &end);

	/* do we have to search in case sensitive mode? */
	gboolean case_sensitive =
		gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (check_case_sensitive));

#ifdef REGEX_SUPPORT
	/* should we treat the input as a regex pattern and execute it? */
	gboolean regex_mode =
		gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (check_regex_mode));
#endif

	/* determine where to start */
	gint offset = 0;
	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (check_start_cursor)))
	{
		offset = gtk_text_iter_get_offset (&start);
	}

	/* create a new store list */
	GtkListStore *store =
		gtk_list_store_new (3, G_TYPE_STRING, G_TYPE_INT, G_TYPE_INT);

#ifdef REGEX_SUPPORT
	if (regex_mode)
	{
		BeaverRegexResult *res;
		gint i;

		res = beaver_text_find_regex (text, offset, case_sensitive);

		if (NULL == res)
		{
			g_print ("No match found\n");

			/* set the treeview model to the new model */
			gtk_tree_view_set_model (GTK_TREE_VIEW (results_treeview),
									 GTK_TREE_MODEL (store));
			g_object_unref (GTK_TREE_MODEL (store));

			/* restore cursor position */
			gtk_text_buffer_place_cursor (buffer, &start);
			return;
		}

		for (i = 0; i < res->count; i++)
		{
			GtkTreeIter iter;

			gchar *string =
				g_strdup_printf ("%d.\tLine %d (%d - %d)", i + 1,
								 res->results[i].line,
								 res->results[i].start, res->results[i].end);

			/* add found item into results list */
			gtk_list_store_append (store, &iter);
			gtk_list_store_set (store, &iter, 0, string, 1,
								res->results[i].offset, 2,
								(res->results[i].end - res->results[i].start),
								-1);

			/* free up space */
			g_free (string);
		}

		g_free (res);

	}
	else
	{
#endif
		/* search for all occurences in the text */
		gint m = strlen (text);
		gint num = 1;
		while (beaver_text_find (text, offset, case_sensitive))
		{
			GtkTreeIter iter;

			/* get the position of the selection */
			BeaverTextSelection *pos = beaver_text_selection_position ();

			/* create string with information */
			gchar *string =
				g_strdup_printf ("%d.\tLine %d (%d - %d)", num++, pos->line,
								 pos->start, pos->end);

			/* add found item into results list */
			gtk_list_store_append (store, &iter);
			gtk_list_store_set (store, &iter, 0, string, 1, pos->offset, 2, m,
								-1);

			/* proceed */
			offset = pos->offset + m;

			/* free up space */
			g_free (pos);
			g_free (string);
		}

#ifdef REGEX_SUPPORT
	}
#endif
	/* set the treeview model to the new model */
	gtk_tree_view_set_model (GTK_TREE_VIEW (results_treeview),
							 GTK_TREE_MODEL (store));
	g_object_unref (GTK_TREE_MODEL (store));

	/* restore cursor position */
	gtk_text_buffer_place_cursor (buffer, &start);
}


/**
 * called when pressed on find&replace button
 *
 * @param   button  the button that was pressed
 * @param   data    user defined data
 */
static void find_replace (GtkButton * button, gpointer data)
{
	const gchar *search_text = gtk_entry_get_text (GTK_ENTRY (entry_search));
	const gchar *replace_text = gtk_entry_get_text (GTK_ENTRY (entry_replace));
	gboolean text_found = FALSE;

	/* has the search text box some value? if not display message and
	 * abort */
	if (strcmp (search_text, "") == 0)
	{
		beaver_box_error ("Please provide a search string!");
		return;
	}

	/* has the replace text box some value? if not display message and
	 * abort */
	if (strcmp (replace_text, "") == 0)
	{
		if (! beaver_box_question("Empty string provided as a replacement.  Do you want to continue?"))
		{
			return;
		}
	}

	/* do we want to search case sensitive? */
	gboolean case_sensitive =
		gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (check_case_sensitive));

	// Proceed with the search & replace!
	// start by getting the page and toggling the user action flag on
	gint page = gtk_notebook_get_current_page (GTK_NOTEBOOK (MainNotebook));
	FPROPS (page, WidgetInfo.in_user_action) = TRUE;

#ifdef REGEX_SUPPORT
	/* should we treat the input as a regex pattern and execute it? */
	gboolean regex_mode =
		gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (check_regex_mode));

	if (regex_mode)
	{
		beaver_text_replace_regex (search_text, replace_text, case_sensitive,
								   gtk_toggle_button_get_active
								   (GTK_TOGGLE_BUTTON (check_replace_all)));
		text_found = TRUE;  // for now, pretend that text was found @TODO improve
	}
	else
	{
#endif
		/* if the user wants to replace all, then do so */
		if (gtk_toggle_button_get_active
			(GTK_TOGGLE_BUTTON (check_replace_all)))
		{
			beaver_text_replace (search_text, replace_text, case_sensitive);
			text_found = TRUE;  // for now, pretend that text was found @TODO improve
		}
		else
		{

			/* get the current page that should be edited */
			gint current_page =
				gtk_notebook_get_current_page (GTK_NOTEBOOK (MainNotebook));

			/* get the text buffer */
			GtkTextBuffer *buffer = FPROPS (current_page, Buffer);

			/* try to load cursors from selection */
			GtkTextIter start, end;
			gtk_text_buffer_get_selection_bounds (buffer, &start, &end);

			/* determine where to start */
			gint offset = 0;
			if (gtk_toggle_button_get_active
				(GTK_TOGGLE_BUTTON (check_start_cursor)))
			{
				offset = gtk_text_iter_get_offset (&start);
			}

			/* search for the string, if found, then ask the user if
			 * he wants to replace this string, if not, then search for the
			 * next.
			 */
			gint m = strlen (search_text);
			if (beaver_text_find (search_text, offset, case_sensitive))
			{
				/* get the position of the selection */
				BeaverTextSelection *pos = beaver_text_selection_position ();
	
				/* scroll to the selected position */
				GtkTextIter start, end;
				gtk_text_buffer_get_selection_bounds (buffer, &start, &end);
				gtk_text_view_scroll_to_iter (GTK_TEXT_VIEW
										  (FPROPS (current_page, Text)), &start,
										  0.1, FALSE, 0, 0);

				/* ask the question! */
				gint answer = beaver_box_question ("Do you want to replace?");
				if (answer)
				{
					gtk_text_buffer_select_range (buffer, &start, &end);
					beaver_text_selection_set (replace_text);
				}

				/* proceed */
				offset = pos->offset + m;

				/* free up space */
				g_free (pos);
				text_found = TRUE;
			}
		}
#ifdef REGEX_SUPPORT
	}
#endif

	// all done... turn the user action flag back off
	FPROPS (page, WidgetInfo.in_user_action) = FALSE;

	// if no text was found, let the user know instead of just sitting there.
	if (! text_found)
	{
		beaver_box_message ("String not found");
	}
}

/**
 * Displays a search window.
 *
 * @param   replace     wheter or not, the search window is a search&replace
 *                      window
 */
void search (gboolean replace)
{
	/* create the search window, if not created before.
	 * the search window consists of two vertical frames. in
	 * the first frame the user inputs his search and in the
	 * second frame the results are displayed. if the user wants
	 * a search&replace window, then in the second frame the
	 * user can input the text he wants to written instead of
	 * the found text.
	 *
	 * here we create singleton instances of the frame (the upper
	 * frame and the two possible lower frames). when the window
	 * get's shown, then the appropriate frames get choosen and
	 * and displayed.
	 */

	/* search frame not yet created! */
	if (!frame_search)
	{
		frame_search = gtk_frame_new ("Search");

		/* create a new vertical box. this vertical box
		 * will hold a text entry and two radio buttons, as well
		 * as 1 check button */
		GtkWidget *vbox = gtk_vbox_new (FALSE, 5);
		gtk_container_set_border_width (GTK_CONTAINER (vbox), 5);
		gtk_container_add (GTK_CONTAINER (frame_search), vbox);

		/* create a new text entry and attach it */
		entry_search = gtk_entry_new ();
		gtk_entry_set_activates_default (GTK_ENTRY (entry_search), TRUE);
		gtk_box_pack_start (GTK_BOX (vbox), entry_search, FALSE, FALSE, 0);

		/* create two radio buttons to choose if we want
		 * to start at cursor position or at the beginning of the
		 * text */
		check_start_cursor =
			gtk_radio_button_new_with_mnemonic (NULL,
												"Start at cursor _position");
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check_start_cursor),
									  TRUE);
		gtk_box_pack_start (GTK_BOX (vbox), check_start_cursor, FALSE, FALSE,
							0);

		GSList *group =
			gtk_radio_button_get_group (GTK_RADIO_BUTTON (check_start_cursor));

		check_start_beginning =
			gtk_radio_button_new_with_mnemonic (group,
												"Start at _beginning of the document");
		gtk_box_pack_start (GTK_BOX (vbox), check_start_beginning, FALSE, FALSE,
							0);

		/* create a check box that let's the user choose if he wants
		 * case sensitive search or not */
		check_case_sensitive =
			gtk_check_button_new_with_mnemonic ("Case _sensitive");
		gtk_box_pack_start (GTK_BOX (vbox), check_case_sensitive, FALSE, FALSE,
							0);

#ifdef REGEX_SUPPORT
		/* checkbox to allow pcre-compatible pattern search */
		check_regex_mode =
			gtk_check_button_new_with_mnemonic ("Use _regular expressions");
		gtk_box_pack_start (GTK_BOX (vbox), check_regex_mode, FALSE, FALSE, 0);
#endif
	}

	/* results frame not yet created! */
	if (!frame_results)
	{
		frame_results = gtk_frame_new ("Results");

		/* a box */
		GtkWidget *vbox = gtk_vbox_new (FALSE, 1);
		gtk_container_set_border_width (GTK_CONTAINER (vbox), 5);
		gtk_container_add (GTK_CONTAINER (frame_results), vbox);

		/* create a scrolled window (for the treeview) */
		GtkWidget *scroll = gtk_scrolled_window_new (NULL, NULL);
		gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scroll),
										GTK_POLICY_AUTOMATIC,
										GTK_POLICY_ALWAYS);
		gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scroll),
											 GTK_SHADOW_ETCHED_IN);
		gtk_box_pack_start (GTK_BOX (vbox), scroll, FALSE, FALSE, 0);

		/* create a treeview */
		results_treeview = gtk_tree_view_new ();
		gtk_container_add (GTK_CONTAINER (scroll), results_treeview);

		GtkCellRenderer *renderer = gtk_cell_renderer_text_new ();
		gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW
													 (results_treeview), -1,
													 "found", renderer, "text",
													 0, NULL);
		gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (results_treeview),
										   FALSE);

		/* register handlers */
		g_signal_connect (G_OBJECT (results_treeview), "row-activated",
						  GTK_SIGNAL_FUNC (signal_row_activated), NULL);
	}

	/* replace frame not yet created! */
	if (!frame_replace)
	{
		frame_replace = gtk_frame_new ("Replace");

		/* a box */
		GtkWidget *vbox = gtk_vbox_new (FALSE, 2);
		gtk_container_set_border_width (GTK_CONTAINER (vbox), 5);
		gtk_container_add (GTK_CONTAINER (frame_replace), vbox);

		/* create a text entry for replacement word */
		entry_replace = gtk_entry_new ();
		gtk_entry_set_activates_default (GTK_ENTRY (entry_replace), TRUE);
		gtk_box_pack_start (GTK_BOX (vbox), entry_replace, FALSE, FALSE, 0);

		/* a checkbox for replace_all */
		check_replace_all = gtk_check_button_new_with_mnemonic ("Replace _all");
		gtk_box_pack_start (GTK_BOX (vbox), check_replace_all, FALSE, FALSE, 0);
	}

	/* create the window if not done so already */
	if (!search_window)
	{
		/* new dialog window */
		search_window = gtk_dialog_new ();
		gtk_window_set_modal (GTK_WINDOW (search_window), TRUE);
		gtk_window_set_resizable (GTK_WINDOW (search_window), FALSE);

		/* new virtual box for the upper part of the dialog */
		GtkWidget *vbox = gtk_vbox_new (TRUE, 5);
		gtk_box_pack_start (GTK_BOX (GTK_DIALOG (search_window)->vbox), vbox,
							FALSE, FALSE, 0);

		/* window is modal for main window */
		gtk_window_set_modal (GTK_WINDOW (search_window), TRUE);
		gtk_window_set_transient_for (GTK_WINDOW (search_window),
									  GTK_WINDOW (gtk_widget_get_toplevel
												  (GTK_WIDGET (MainNotebook))));

		/* cancel button */
		GtkWidget *button = gtk_button_new_from_stock (GTK_STOCK_CANCEL);
		GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
		gtk_box_pack_start (GTK_BOX (GTK_DIALOG (search_window)->action_area),
							button, TRUE, TRUE, 0);

		/* find button */
		button_find = gtk_button_new ();

		/* register some signals. mostly the deletion signals
		 * for the window. but instead of killing the window, only
		 * hide it */
		g_signal_connect (G_OBJECT (search_window), "delete-event",
						  GTK_SIGNAL_FUNC (gtk_widget_hide_on_delete),
						  search_window);
		g_signal_connect (G_OBJECT (search_window), "close",
						  GTK_SIGNAL_FUNC (gtk_widget_hide), search_window);
		g_signal_connect (G_OBJECT (search_window), "response",
						  GTK_SIGNAL_FUNC (gtk_widget_hide), search_window);
		g_signal_connect_swapped (G_OBJECT (button), "clicked",
								  GTK_SIGNAL_FUNC (gtk_widget_hide),
								  search_window);
	}

	/* remove the widgets from the boxes before reattaching them */
	if (frame_search->parent == GTK_WIDGET (GTK_DIALOG (search_window)->vbox))
	{
		g_object_ref (frame_search);	/* attach reference on it or it will be deleted */
		gtk_container_remove (GTK_CONTAINER (GTK_DIALOG (search_window)->vbox),
							  frame_search);
	}

	if (frame_replace->parent == GTK_WIDGET (GTK_DIALOG (search_window)->vbox))
	{
		g_object_ref (frame_replace);	/* attach reference on it or it will be deleted */
		gtk_container_remove (GTK_CONTAINER (GTK_DIALOG (search_window)->vbox),
							  frame_replace);
	}

	if (frame_results->parent == GTK_WIDGET (GTK_DIALOG (search_window)->vbox))
	{
		g_object_ref (frame_results);	/* attach reference on it or it will be deleted */
		gtk_container_remove (GTK_CONTAINER (GTK_DIALOG (search_window)->vbox),
							  frame_results);
	}

	/* choose both frames and attach them to the boxes */
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (search_window)->vbox),
						frame_search, TRUE, TRUE, 0);
	if (replace)
	{
		gtk_window_set_title (GTK_WINDOW (search_window),
							  "Search and Replace Text");
		gtk_box_pack_start (GTK_BOX (GTK_DIALOG (search_window)->vbox),
							frame_replace, TRUE, TRUE, 0);

		/* create a find&replace button */
		gtk_widget_destroy (button_find);
		button_find = gtk_button_new_from_stock (GTK_STOCK_FIND_AND_REPLACE);
		GTK_WIDGET_SET_FLAGS (button_find, GTK_CAN_DEFAULT);
		gtk_box_pack_start (GTK_BOX (GTK_DIALOG (search_window)->action_area),
							button_find, TRUE, TRUE, 0);
		g_signal_connect_swapped (G_OBJECT (button_find), "clicked",
								  GTK_SIGNAL_FUNC (find_replace), NULL);
	}
	else
	{
		gtk_window_set_title (GTK_WINDOW (search_window), "Search Text");
		gtk_box_pack_start (GTK_BOX (GTK_DIALOG (search_window)->vbox),
							frame_results, TRUE, TRUE, 0);

		/* create a find button */
		gtk_widget_destroy (button_find);
		button_find = gtk_button_new_from_stock (GTK_STOCK_FIND);
		GTK_WIDGET_SET_FLAGS (button_find, GTK_CAN_DEFAULT);
		gtk_box_pack_start (GTK_BOX (GTK_DIALOG (search_window)->action_area),
							button_find, TRUE, TRUE, 0);
		g_signal_connect_swapped (G_OBJECT (button_find), "clicked",
								  GTK_SIGNAL_FUNC (find), NULL);
	}

	/* clear the text entry fields */
	gtk_entry_set_text (GTK_ENTRY (entry_search), "");
	gtk_entry_set_text (GTK_ENTRY (entry_replace), "");

	/* clear the list */
	gtk_tree_view_set_model (GTK_TREE_VIEW (results_treeview), NULL);

	/* show the window */
	gtk_widget_show_all (search_window);
}

/**
 * Display a dialog where the user can input the line they want
 * to jump to.
 */
void goto_line (void)
{
	/* create the window if not done so already */
	if (!goto_window)
	{
		/* new dialog window */
		goto_window = gtk_dialog_new ();
		gtk_window_set_modal (GTK_WINDOW (goto_window), TRUE);
		gtk_window_set_resizable (GTK_WINDOW (goto_window), FALSE);
		gtk_window_set_title (GTK_WINDOW (goto_window), "Goto Line ...");
		gtk_window_set_transient_for (GTK_WINDOW (goto_window),
									  GTK_WINDOW (gtk_widget_get_toplevel
												  (GTK_WIDGET (MainNotebook))));

		/* spin box */
		entry_line = gtk_spin_button_new_with_range (1, 999, 1);
		gtk_box_pack_start (GTK_BOX (GTK_DIALOG (goto_window)->vbox),
							entry_line, FALSE, FALSE, 0);

		/* cancel button */
		GtkWidget *button = gtk_button_new_from_stock (GTK_STOCK_CANCEL);
		GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
		gtk_box_pack_start (GTK_BOX (GTK_DIALOG (goto_window)->action_area),
							button, TRUE, TRUE, 0);
		g_signal_connect_swapped (G_OBJECT (button), "clicked",
								  GTK_SIGNAL_FUNC (gtk_widget_hide),
								  goto_window);

		/* goto button */
		button = gtk_button_new_from_stock (GTK_STOCK_JUMP_TO);
		GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
		gtk_box_pack_start (GTK_BOX (GTK_DIALOG (goto_window)->action_area),
							button, TRUE, TRUE, 0);
		g_signal_connect_swapped (G_OBJECT (button), "clicked",
								  GTK_SIGNAL_FUNC (jump_to), NULL);

		/* register some signals. mostly the deletion signals
		 * for the window. but instead of killing the window, only
		 * hide it */
		g_signal_connect (G_OBJECT (goto_window), "delete-event",
						  GTK_SIGNAL_FUNC (gtk_widget_hide_on_delete),
						  goto_window);
		g_signal_connect (G_OBJECT (goto_window), "close",
						  GTK_SIGNAL_FUNC (gtk_widget_hide), goto_window);
		g_signal_connect (G_OBJECT (goto_window), "response",
						  GTK_SIGNAL_FUNC (gtk_widget_hide), goto_window);
	}

	/* adjust range of spin box, according to text size */
	gint current_page =
		gtk_notebook_get_current_page (GTK_NOTEBOOK (MainNotebook));
	GtkTextBuffer *buffer = FPROPS (current_page, Buffer);
	gint lines = gtk_text_buffer_get_line_count (buffer);
	gtk_spin_button_set_range (GTK_SPIN_BUTTON (entry_line), 1, lines);

	/* show the window */
	gtk_widget_show_all (goto_window);
}
