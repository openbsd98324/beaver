
/*
** Beaver's an Early AdVanced EditoR
** (C) 1999-2000 Marc Bevand, Damien Terrier and Emmanuel Turquin, 2008 Tobias Heinzen, Double 12
**
** languages.c
**
** Author<s>:   Emmanuel Turquin (aka "Ender") <turqui_e@epita.fr>
**              Michael Terry <mterry@fastmail.fm>
**				Tobias Heinzen
** Description:   Beaver languages menus generator source
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/**
 * @file creates a menu from which the user can choose the
 *       language for which he want syntax highlighting
 */

#include <gtk/gtk.h>
#include "languages.h"
#include "interface.h"
#include "struct.h"
#include "editor.h"
#include "msgbar.h"

/* global variables */
extern GtkUIManager *MenuManager;	/* the MenuManager from interface.c */
extern gint OpenedFilesCnt;		/* from interface.c */
extern GtkWidget *MainNotebook;	/* from interface.c */
extern GArray *FileProperties;	/* from interface.c */
static GSList *languages_radio_group = NULL;	/* the group for all radio items */
static GtkActionGroup *languages_action_group = NULL;	/* a action group for all the languages
														   menues */

/**
 * callback function for language menu items.
 * 
 * @param   action          the action that was triggered
 * @param   data            unused
 */
static void language_callback (GtkAction * action, gpointer data)
{
	/* get the choice of the language */
	gint choice = -1;
	if (action != NULL)
	{
		choice = gtk_radio_action_get_current_value (GTK_RADIO_ACTION (action));
	}

	/* check if any files are opened */
	if (OpenedFilesCnt < 0)
		return;

	/* get the current page that is opened in the notebook */
	gint current_page =
		gtk_notebook_get_current_page (GTK_NOTEBOOK (MainNotebook));

	/* select right type of syhi and tell the
	 * editor to choose it */
	gchar *msg = NULL;
	if (choice >= 0)
	{
		refresh_editor (FPROPS (current_page, Text), choice);
		msg =
			g_strdup_printf ("\"%s\" mode activated...",
							 Prefs.L[choice].Description);
	}
	else
	{
		refresh_editor (FPROPS (current_page, Text), SYHI_DISABLE);
		msg = g_strdup_printf ("Language support disabled...");
	}
	print_msg (msg);

	/* free up space */
	g_free (msg);
}

/**
 * Adds a new Language to the language choosing menu. All
 * items are RadioMenuItems in the same group, so one can 
 * determine which language is now active. All languages have
 * an unique integer identifier.
 * 
 * @param   text    the label text of the new item
 * @param   id      the identifier of this language.
 */
static inline void add_language_to_menu (const gchar * text, gint id)
{
	/* create merge id */
	gint merge_id = gtk_ui_manager_new_merge_id (MenuManager);

	/* create action name, this is just <name>Action */
	gchar *name = g_strdup_printf ("Language%d", id);
	gchar *action_name = g_strconcat (name, "Action", NULL);

	/* create action entry */
	GtkRadioAction *action = gtk_radio_action_new (action_name,	/* name */
												   text,	/* label */
												   NULL,	/* tooltip */
												   NULL,	/* stock id */
												   id);
	gtk_action_group_add_action (languages_action_group, GTK_ACTION (action));

	/* make sure all radio buttons are on the same group */
	gtk_radio_action_set_group (action, languages_radio_group);
	languages_radio_group = gtk_radio_action_get_group (action);

	/* add the menu item */
	gtk_ui_manager_add_ui (MenuManager, merge_id,
						   "ui/MainMenu/ViewMenu/Languages/", name, action_name,
						   GTK_UI_MANAGER_MENUITEM, FALSE);

	/* register signals */
	g_signal_connect (G_OBJECT (action), "activate",
					  G_CALLBACK (language_callback), NULL);

	/* free up space */
	g_free (name);
	g_free (action_name);
}

/**
 * 
 */
void refresh_languages_menu (void)
{
	GtkAction *action =
		gtk_action_group_get_action (languages_action_group,
									 "Language-1Action");

	gint mode = -1;
	if (OpenedFilesCnt >= 0)
	{
		/* get the current page that is opened in the notebook */
		gint current_page =
			gtk_notebook_get_current_page (GTK_NOTEBOOK (MainNotebook));

		/* get the language of the current page */
		mode = FPROPS (current_page, WidgetInfo.Lg);
	}

	/* make sure the mode to set is safe */
	if (mode > MAX_LANG || !Prefs.L[mode].IsDefined || mode < 0)
	{
		mode = -1;
	}

	/* select the correct language in the menu */
	gtk_radio_action_set_current_value (GTK_RADIO_ACTION (action), mode);
}

/**
 * 
 */
void init_languages_menu (void)
{
	/* we add a language selection menu to the View menu
	 * of the interface. all languages can be selected
	 * via a submenu of a new menu item called "Syntax Highlighting".
	 * So first of all we create this new submenu */
	gint merge_id = gtk_ui_manager_new_merge_id (MenuManager);
	GtkActionEntry entries[] = {
		{"LanguagesAction",	/* action name */
		 NULL,	/* stock item */
		 "Syntax Highlighting",	/* label */
		 NULL,	/* accelerator */
		 NULL,	/* tooltip */
		 refresh_languages_menu	/* callback */
		 }
	};

	languages_action_group = gtk_action_group_new ("LanguageActionGroup");
	gtk_ui_manager_insert_action_group (MenuManager, languages_action_group, 0);

	gtk_action_group_add_actions (languages_action_group, entries, 1, NULL);
	gtk_ui_manager_add_ui (MenuManager, merge_id, "ui/MainMenu/ViewMenu/",
						   "Languages", "LanguagesAction", GTK_UI_MANAGER_MENU,
						   FALSE);

	/* first item in the languages menu is the none item
	 * which has special identifier -1 */
	add_language_to_menu ("None", -1);

	/* add all parsed languages into the languages menu */
	gint lang;
	for (lang = 0; lang < MAX_LANG; lang++)
	{
		if (Prefs.L[lang].IsDefined)
		{
			add_language_to_menu (Prefs.L[lang].Description, lang);
		}
	}
}
