
/*
** Beaver's an Early AdVanced EditoR
** (C) 1999-2000 Marc Bevand, Damien Terrier and Emmanuel Turquin, 2008 Tobias Heinzen, Double 12
**
** msgbar.c
**
** Author<s>:     Emmanuel Turquin (aka "Ender") <turqui_e@epita.fr>
**                Michael Terry <mterry@fastmail.fm>
**                Higor Eurípedes <heuripedes@gmail.com>
** Description:   Beaver message bar manipulation source
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/** @file msgbar.c
 * Message bar
 */

#include <gtk/gtk.h>
#include "struct.h"
#include "prefs.h"
#include "msgbar.h"

/* some external defined structures */
extern t_settings Settings;
extern GtkWidget *MainNotebook;
extern GArray *FileProperties;
extern gint OpenedFilesCnt;		/* how many opened files */

/* some statics */
static GtkWidget *__MsgBar = NULL;
static GtkWidget *__MsgBarInfoLabel = NULL;
static gint message_context = 0;
static gint __info_label_timeout_id = -1;
static gint __mesg_label_timeout_id = -1;

/**
 * Returns the number of characters in the text. 
 * 
 * @param   Text    the text to search into
 * @return  number of characters in the Text
 */
static inline gint get_num_characters (GtkTextView * Text)
{
	return gtk_text_buffer_get_char_count (gtk_text_view_get_buffer (Text));
}


/**
 * Returns the beginning of the current selection. 
 * 
 * @param   Text    the text to search into
 * @return  position (in characters) in text of the beginning of the selection
 */
static inline gint min_from_selection (GtkTextView * Text)
{
	GtkTextIter start;
	GtkTextBuffer *Buffer;

	Buffer = gtk_text_view_get_buffer (Text);
	gtk_text_buffer_get_selection_bounds (Buffer, &start, NULL);

	return gtk_text_iter_get_offset (&start);
}


/**
 * Returns the end of the current selection. 
 * 
 * @param   Text    the text to search into
 * @return  position (in characters) in text of the end of the selection
 */
static inline gint max_from_selection (GtkTextView * Text)
{
	GtkTextIter end;
	GtkTextBuffer *Buffer;

	Buffer = gtk_text_view_get_buffer (Text);
	gtk_text_buffer_get_selection_bounds (Buffer, NULL, &end);

	return gtk_text_iter_get_offset (&end);
}

/**
 * get's the active line number.
 * 
 * @return number of line that's active  
 */
static inline gint get_line (void)
{
	GtkWidget *Editor;
	GtkTextIter current;
	GtkTextBuffer *EditorBuffer;

	Editor =
		FPROPS (gtk_notebook_get_current_page (GTK_NOTEBOOK (MainNotebook)),
				Text);
	EditorBuffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (Editor));
	gtk_text_buffer_get_iter_at_mark (EditorBuffer, &current,
									  gtk_text_buffer_get_insert
									  (EditorBuffer));

	return gtk_text_iter_get_line (&current) + 1;
}

/**
 * get's active column number.
 * 
 * @return number of column that's active
 */
static inline gint get_column (void)
{
	GtkWidget *Editor;
	GtkTextIter current;
	GtkTextBuffer *EditorBuffer;

	Editor =
		FPROPS (gtk_notebook_get_current_page (GTK_NOTEBOOK (MainNotebook)),
				Text);
	EditorBuffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (Editor));
	gtk_text_buffer_get_iter_at_mark (EditorBuffer, &current,
									  gtk_text_buffer_get_insert
									  (EditorBuffer));

	return gtk_text_iter_get_line_offset (&current) + 1;
}

/**
 *
 */
static inline gint get_percent (void)
{
	GtkWidget *Editor;
	GtkTextIter current;
	GtkTextBuffer *EditorBuffer;
	gint i, j;

	Editor =
		FPROPS (gtk_notebook_get_current_page (GTK_NOTEBOOK (MainNotebook)),
				Text);
	EditorBuffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (Editor));
	gtk_text_buffer_get_iter_at_mark (EditorBuffer, &current,
									  gtk_text_buffer_get_insert
									  (EditorBuffer));

	i = 100 * gtk_text_iter_get_offset (&current);
	if ((j = get_num_characters (GTK_TEXT_VIEW (Editor))) == 0)
		return 0;
	else
		return i / j;
}

/**
 * get's the size of a selection.
 * 
 * @return size of selection in characters
 */
static inline gint get_selection_size (void)
{
	gint CurrentPage;
	gint rv;

	CurrentPage = gtk_notebook_get_current_page (GTK_NOTEBOOK (MainNotebook));
	rv = max_from_selection (GTK_TEXT_VIEW (FPROPS (CurrentPage, Text))) -
		min_from_selection (GTK_TEXT_VIEW (FPROPS (CurrentPage, Text)));

	return rv;
}

/**
 * ends printing the message in the message bar. this means that the label
 * is set to empty and the timeout is removed 
 * 
 * @param   data    unused
 * @retval  true    on success
 * @retval  false   otherwise
 */
static gboolean end_print_msg (gpointer data)
{
	gtk_statusbar_pop(GTK_STATUSBAR(__MsgBar), message_context);
	g_source_remove (__mesg_label_timeout_id);
	return TRUE;
}

/**
 * Update the Message Bar Info Label. In the Info Label there are the 
 * following information:\n
 *  - how many lines selected?\n
 *  - which line?\n
 *  - which column?\n
 *  - percentage in whole text?\n
 * The functions get's called by a timeout every 80 ms
 *
 * @param   data    unused
 * @retval  true    on success
 * @retval  false   otherwise
 */
static gboolean update_info_label (gpointer data)
{
	gchar *line_column;
	gint sel_size;

	/* if not visible then do not display anything */
	if (!MSGBAR_DISPLAY)
		return TRUE;

	/* if no files are open then show nothing on the info
	 * label */
	if (OpenedFilesCnt < 0)
	{
		gtk_label_set_text (GTK_LABEL (__MsgBarInfoLabel), "");

		return TRUE;
	}

	/* set label */
	sel_size = get_selection_size ();
	if (sel_size)
	{
		line_column =
			g_strdup_printf ("Selection %d Line %d   Column %d   %d%%",
							 sel_size, get_line (), get_column (),
							 get_percent ());
	}
	else
	{
		line_column =
			g_strdup_printf ("Line %d   Column %d   %d%%", get_line (),
							 get_column (), get_percent ());
	}
	gtk_label_set_text (GTK_LABEL (__MsgBarInfoLabel), line_column);
	g_free (line_column);

	return TRUE;
}

/**
 * shows the message box.
 */
void show_msgbar (void)
{
	gtk_widget_show (__MsgBar);

	/* reenable timer */
	if (__info_label_timeout_id == -1)
	{
		g_timeout_add (80, update_info_label, NULL);
	}
}

/**
 * hide the message box 
 */
void hide_msgbar (void)
{
	gtk_widget_hide (__MsgBar);

	/* remove the timer, so we don't get interrupted any
	 * 80ms */
	g_source_remove (__info_label_timeout_id);
	__info_label_timeout_id = -1;
}

/**
 * initsializing message bar. The message bar consists of two horinzontal
 * boxes which contain two labels. the first label is for printing messages,
 * where the second label is to display some information about the document
 * (ie. which line are we at? which column? etc...)
 * 
 * @param   ParentBox   where to insert the message bar
 */
void init_msgbar (GtkBox * ParentBox)
{

	/* create all objects */
	__MsgBar             = gtk_statusbar_new();
	__MsgBarInfoLabel    = gtk_label_new("");

	message_context = gtk_statusbar_get_context_id(GTK_STATUSBAR(__MsgBar), "Statusbar");

	/* pack the things together */
	gtk_box_pack_end (GTK_BOX (__MsgBar), __MsgBarInfoLabel, FALSE, FALSE, 5);
	gtk_box_pack_end (ParentBox, __MsgBar, FALSE, FALSE, 0);
	/* add a timeout, that happens every 80ms to update the info 
	 * label */
	if (MSGBAR_DISPLAY)
	{
		__info_label_timeout_id = g_timeout_add (80, update_info_label, NULL);
	}
}

/**
 * Set the message label to given message.
 * 
 * @param   Message message to display
 */
void print_msg (gchar * Message)
{
	/* if the message bar is not visible then do
	 * nothing */
	if (!MSGBAR_DISPLAY)
		return;

	/* set the message label */
	gtk_statusbar_pop(GTK_STATUSBAR(__MsgBar), message_context);
	gtk_statusbar_push(GTK_STATUSBAR(__MsgBar), message_context, Message);
	/* add a timeout for how long the message should be displayed */
	__mesg_label_timeout_id =
		g_timeout_add (MSGBAR_INTERVAL, end_print_msg, NULL);
}
