
/*
** Beaver Ascii art generator plugin
** (C) 2008 Double 12
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <stdio.h>

/* In this struct, characters of the art are stored in lines */
typedef struct
{
	const gchar *line1;
	const gchar *line2;
	const gchar *line3;
	const gchar *line4;
	const gchar *line5;
} RenderArt;



/*
 * Example:
 * line1:      ____      _____
 * line2:     / __ \     | __ \
 * line3:    / /__\ \    |    /
 * line4:   /  ____  \   | ___ \
 * line5:  /__/    \__\  |_____/

*/
