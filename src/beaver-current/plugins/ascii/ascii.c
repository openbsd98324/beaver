
/*
** Beaver Ascii art generator plugin
** (C) 2008 Double 12
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <stdio.h>

#include "beaver.h"
#include "ascii.h"


static gint menu_item_id = 0;
static gint tool_item_id = 0;


void draw_art (gchar * word)
{
	RenderArt art;
	art.line1 = " ";
	art.line2 = " ";
	art.line3 = " ";
	art.line4 = " ";
	art.line5 = " ";

	gchar *next = word;
	while (*next)
	{
		switch (*next)
		{
			printf ("%c", *next);
		case 'a':
			art.line1 = g_strconcat (art.line1, "    ____      ", NULL);
			art.line2 = g_strconcat (art.line2, "   / __ \\     ", NULL);
			art.line3 = g_strconcat (art.line3, "  / /__\\ \\    ", NULL);
			art.line4 = g_strconcat (art.line4, " / ______ \\   ", NULL);
			art.line5 = g_strconcat (art.line5, "/_/      \\_\\  ", NULL);
			break;
		case 'b':
			art.line1 = g_strconcat (art.line1, "_____    ", NULL);
			art.line2 = g_strconcat (art.line2, "| __ \\   ", NULL);
			art.line3 = g_strconcat (art.line3, "|    /   ", NULL);
			art.line4 = g_strconcat (art.line4, "| ___ \\  ", NULL);
			art.line5 = g_strconcat (art.line5, "|_____/  ", NULL);
			break;
		case 'c':
			art.line1 = g_strconcat (art.line1, "  ______    ", NULL);
			art.line2 = g_strconcat (art.line2, " /  ____|   ", NULL);
			art.line3 = g_strconcat (art.line3, "/  /        ", NULL);
			art.line4 = g_strconcat (art.line4, "\\  \\____    ", NULL);
			art.line5 = g_strconcat (art.line5, " \\______|   ", NULL);
			break;
		case 'd':
			art.line1 = g_strconcat (art.line1, "_____     ", NULL);
			art.line2 = g_strconcat (art.line2, "| __ \\    ", NULL);
			art.line3 = g_strconcat (art.line3, "| | \\ |   ", NULL);
			art.line4 = g_strconcat (art.line4, "| |_/ |   ", NULL);
			art.line5 = g_strconcat (art.line5, "|____/    ", NULL);

			break;
		case 'e':
			art.line1 = g_strconcat (art.line1, "_______    ", NULL);
			art.line2 = g_strconcat (art.line2, "|  ____|   ", NULL);
			art.line3 = g_strconcat (art.line3, "|  |___    ", NULL);
			art.line4 = g_strconcat (art.line4, "|  |___    ", NULL);
			art.line5 = g_strconcat (art.line5, "|______|   ", NULL);
			break;
		case 'f':
			art.line1 = g_strconcat (art.line1, "_______   ", NULL);
			art.line2 = g_strconcat (art.line2, "|  ____|  ", NULL);
			art.line3 = g_strconcat (art.line3, "|  |__    ", NULL);
			art.line4 = g_strconcat (art.line4, "|  |__|   ", NULL);
			art.line5 = g_strconcat (art.line5, "|__|      ", NULL);
			break;
		case 'g':
			art.line1 = g_strconcat (art.line1, "  ______   ", NULL);
			art.line2 = g_strconcat (art.line2, " /  ____|  ", NULL);
			art.line3 = g_strconcat (art.line3, "|  /  __   ", NULL);
			art.line4 = g_strconcat (art.line4, "|  \\__| |  ", NULL);
			art.line5 = g_strconcat (art.line5, " \\_____/   ", NULL);
			break;
		case 'h':
			art.line1 = g_strconcat (art.line1, " __    __   ", NULL);
			art.line2 = g_strconcat (art.line2, "|  |  |  |  ", NULL);
			art.line3 = g_strconcat (art.line3, "|  |/\\|  |  ", NULL);
			art.line4 = g_strconcat (art.line4, "|  |\\/|  |  ", NULL);
			art.line5 = g_strconcat (art.line5, "|__|  |__|  ", NULL);
			break;
		case 'i':
			art.line1 = g_strconcat (art.line1, " __   ", NULL);
			art.line2 = g_strconcat (art.line2, "|  |  ", NULL);
			art.line3 = g_strconcat (art.line3, "|  |  ", NULL);
			art.line4 = g_strconcat (art.line4, "|  |  ", NULL);
			art.line5 = g_strconcat (art.line5, "|__|  ", NULL);
			break;
		case 'j':
			art.line1 = g_strconcat (art.line1, "      __   ", NULL);
			art.line2 = g_strconcat (art.line2, "     |  |  ", NULL);
			art.line3 = g_strconcat (art.line3, "___  |  |  ", NULL);
			art.line4 = g_strconcat (art.line4, "\\  \\/  /   ", NULL);
			art.line5 = g_strconcat (art.line5, " \\_ __/    ", NULL);
			break;
		case 'k':
			art.line1 = g_strconcat (art.line1, " __  ___  ", NULL);
			art.line2 = g_strconcat (art.line2, "|  |/  /  ", NULL);
			art.line3 = g_strconcat (art.line3, "|     /   ", NULL);
			art.line4 = g_strconcat (art.line4, "|     \\   ", NULL);
			art.line5 = g_strconcat (art.line5, "|__|\\__\\  ", NULL);
			break;
		case 'l':
			art.line1 = g_strconcat (art.line1, " __      ", NULL);
			art.line2 = g_strconcat (art.line2, "|  |     ", NULL);
			art.line3 = g_strconcat (art.line3, "|  |     ", NULL);
			art.line4 = g_strconcat (art.line4, "|  |__   ", NULL);
			art.line5 = g_strconcat (art.line5, "|_____|  ", NULL);

			break;
		case 'm':
			art.line1 = g_strconcat (art.line1, " __    __   ", NULL);
			art.line2 = g_strconcat (art.line2, "|  \\  /  |  ", NULL);
			art.line3 = g_strconcat (art.line3, "| \\ \\/ / |  ", NULL);
			art.line4 = g_strconcat (art.line4, "| |\\__/| |  ", NULL);
			art.line5 = g_strconcat (art.line5, "|_|    |_|  ", NULL);
			break;
		case 'n':
			art.line1 = g_strconcat (art.line1, " __   _   ", NULL);
			art.line2 = g_strconcat (art.line2, "|  \\ | |  ", NULL);
			art.line3 = g_strconcat (art.line3, "| \\ \\| |  ", NULL);
			art.line4 = g_strconcat (art.line4, "| |\\ \\ |  ", NULL);
			art.line5 = g_strconcat (art.line5, "|_| \\__|  ", NULL);
			break;
		case 'o':
			art.line1 = g_strconcat (art.line1, "  ______    ", NULL);
			art.line2 = g_strconcat (art.line2, " /  __  \\   ", NULL);
			art.line3 = g_strconcat (art.line3, "|  /  \\  |  ", NULL);
			art.line4 = g_strconcat (art.line4, "|  \\__/  |  ", NULL);
			art.line5 = g_strconcat (art.line5, " \\______/   ", NULL);
			break;
		case 'p':
			art.line1 = g_strconcat (art.line1, " ______   ", NULL);
			art.line2 = g_strconcat (art.line2, "|  __  \\  ", NULL);
			art.line3 = g_strconcat (art.line3, "|   ___/  ", NULL);
			art.line4 = g_strconcat (art.line4, "|  |      ", NULL);
			art.line5 = g_strconcat (art.line5, "|__|      ", NULL);
			break;
		case 'q':
			art.line1 = g_strconcat (art.line1, "  ______     ", NULL);
			art.line2 = g_strconcat (art.line2, " /  __  \\    ", NULL);
			art.line3 = g_strconcat (art.line3, "|  /  \\  |   ", NULL);
			art.line4 = g_strconcat (art.line4, "|  \\__\\  \\   ", NULL);
			art.line5 = g_strconcat (art.line5, " \\____/\\__\\  ", NULL);
			break;
		case 'r':
			art.line1 = g_strconcat (art.line1, " ______    ", NULL);
			art.line2 = g_strconcat (art.line2, "|  __  \\   ", NULL);
			art.line3 = g_strconcat (art.line3, "|      /   ", NULL);
			art.line4 = g_strconcat (art.line4, "|  |\\  \\   ", NULL);
			art.line5 = g_strconcat (art.line5, "|__| \\__\\  ", NULL);
			break;
		case 's':
			art.line1 = g_strconcat (art.line1, "  ______  ", NULL);
			art.line2 = g_strconcat (art.line2, " /  ___/  ", NULL);
			art.line3 = g_strconcat (art.line3, " \\     \\  ", NULL);
			art.line4 = g_strconcat (art.line4, " /___  /  ", NULL);
			art.line5 = g_strconcat (art.line5, "\\_____/   ", NULL);
			break;
		case 't':
			art.line1 = g_strconcat (art.line1, " ________   ", NULL);
			art.line2 = g_strconcat (art.line2, "|__    __|  ", NULL);
			art.line3 = g_strconcat (art.line3, "   |  |     ", NULL);
			art.line4 = g_strconcat (art.line4, "   |  |     ", NULL);
			art.line5 = g_strconcat (art.line5, "   |__|     ", NULL);
			break;
		case 'u':
			art.line1 = g_strconcat (art.line1, " _   _   ", NULL);
			art.line2 = g_strconcat (art.line2, "| | | |  ", NULL);
			art.line3 = g_strconcat (art.line3, "| | | |  ", NULL);
			art.line4 = g_strconcat (art.line4, "| \\_/ |  ", NULL);
			art.line5 = g_strconcat (art.line5, " \\___/   ", NULL);
			break;
		case 'v':
			art.line1 = g_strconcat (art.line1, " _    _   ", NULL);
			art.line2 = g_strconcat (art.line2, "| |  | |  ", NULL);
			art.line3 = g_strconcat (art.line3, "| |  / |  ", NULL);
			art.line4 = g_strconcat (art.line4, " \\ \\/ /   ", NULL);
			art.line5 = g_strconcat (art.line5, "  \\__/    ", NULL);
			break;
		case 'w':
			art.line1 = g_strconcat (art.line1, " _    _   ", NULL);
			art.line2 = g_strconcat (art.line2, "| |  | |  ", NULL);
			art.line3 = g_strconcat (art.line3, "| |  | |  ", NULL);
			art.line4 = g_strconcat (art.line4, "| \\/\\/ |  ", NULL);
			art.line5 = g_strconcat (art.line5, " \\_/\\_/   ", NULL);
			break;
		case 'x':
			art.line1 = g_strconcat (art.line1, "_      _  ", NULL);
			art.line2 = g_strconcat (art.line2, "\\ \\  / /  ", NULL);
			art.line3 = g_strconcat (art.line3, " \\ \\/ /   ", NULL);
			art.line4 = g_strconcat (art.line4, " / /\\ \\   ", NULL);
			art.line5 = g_strconcat (art.line5, "/_/  \\_\\  ", NULL);
			break;
		case 'y':
			art.line1 = g_strconcat (art.line1, "__  __  ", NULL);
			art.line2 = g_strconcat (art.line2, "\\ \\/ /  ", NULL);
			art.line3 = g_strconcat (art.line3, " \\  /   ", NULL);
			art.line4 = g_strconcat (art.line4, " / /    ", NULL);
			art.line5 = g_strconcat (art.line5, "/_/     ", NULL);
			break;
		case 'z':
			art.line1 = g_strconcat (art.line1, " ____   ", NULL);
			art.line2 = g_strconcat (art.line2, "|__  |  ", NULL);
			art.line3 = g_strconcat (art.line3, "  / /   ", NULL);
			art.line4 = g_strconcat (art.line4, " / /_   ", NULL);
			art.line5 = g_strconcat (art.line5, "/____|  ", NULL);
			break;
		case ' ':
			art.line1 = g_strconcat (art.line1, "     ", NULL);
			art.line2 = g_strconcat (art.line2, "     ", NULL);
			art.line3 = g_strconcat (art.line3, "     ", NULL);
			art.line4 = g_strconcat (art.line4, "     ", NULL);
			art.line5 = g_strconcat (art.line5, "     ", NULL);
			break;

		default:;
			gchar *unsupported_char = g_strnfill (1, *next);
			gchar *boxmsg =
				g_strconcat ("Unsupported character: ", unsupported_char, ".",
							 NULL);
			beaver_box_message (boxmsg);
			g_free (boxmsg);
			g_free (unsupported_char);
		}

		next++;

	}

	/*Add the lines of the characters to the document */
	beaver_text_insert_string ("\n");
	beaver_text_insert_string (art.line1);
	beaver_text_insert_string ("\n");
	beaver_text_insert_string (art.line2);
	beaver_text_insert_string ("\n");
	beaver_text_insert_string (art.line3);
	beaver_text_insert_string ("\n");
	beaver_text_insert_string (art.line4);
	beaver_text_insert_string ("\n");
	beaver_text_insert_string (art.line5);
	beaver_text_insert_string ("\n");



}

static void item_clicked (void)
{
	gchar *word = beaver_box_prompt ("Text to insert as Ascii art:"); // show prompt 
	if (word) // only continue if given string != NULL
	{
		word = g_ascii_strdown (word, -1);	/* convert to lower case */
		draw_art (word);
	}
	g_free (word);
}

static void init (void)
{
	/* add a toolbar item with handler */
	menu_item_id =
		beaver_ui_item_add (BEAVER_SECTION_MENU_TOOLS, "AsciiMenuItem",
							GTK_STOCK_UNDERLINE, "Insert _Ascii art",
							item_clicked);
	tool_item_id =
		beaver_ui_item_add (BEAVER_SECTION_TOOLBAR, "AsciiToolItem",
							GTK_STOCK_UNDERLINE, "Insert Ascii art",
							item_clicked);
}

static void cleanup (void)
{
	/* remove items */
	beaver_ui_item_remove (menu_item_id, "AsciiMenuItem");
	beaver_ui_item_remove (tool_item_id, "AsciiToolItem");
}

PLUGIN_NAME ("Ascii art generator")
PLUGIN_DESCRIPTION
("Generates an ascii art of the given word in the current document.") PLUGIN_AUTHOR ("Double 12")
PLUGIN_VERSION ("1") PLUGIN_INIT (init) PLUGIN_CLEANUP (cleanup)
