
/*
** (C) 2008 Tobias Heinzen
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <stdio.h>

#include "beaver.h"


static gint menu_id = 0;

static void sample_clicked (void)
{
	gchar *word = beaver_box_prompt ("Give a shout:");

	//beaver_text_insert_string ("Hallo Welt!");
	/*gchar* selection = beaver_text_selection_get ();
	   if (selection != NULL) {
	   beaver_box_message (selection);
	   g_free (selection);
	   } */
	//beaver_text_selection_set ("Hello World!");
}

static void init (void)
{
	/* show a simple message box */
	//beaver_box_message ("Hello World!");

	/* show an error box */
	//beaver_box_error ("Hello World!");

	/* show a question box (yes/no) */
	//gint test = beaver_box_question ("Hello World?");
	//printf ("return value = %d\n", test); 

	/* add a menu item with handler */
	menu_id =
		beaver_ui_item_add (BEAVER_SECTION_TOOLBAR, "Sample", GTK_STOCK_CDROM,
							"_Sample Entry", sample_clicked);
}

static void cleanup (void)
{
	/* remove menu item */
	beaver_ui_item_remove (menu_id, "Sample");
}

PLUGIN_NAME ("Sample")
PLUGIN_DESCRIPTION ("A small simple plugin.")
PLUGIN_AUTHOR ("Tobias Heinzen")
PLUGIN_VERSION ("1") PLUGIN_INIT (init) PLUGIN_CLEANUP (cleanup)
