

/*
** (C) 2008 Tobias Heinzen
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef __TOOLS_H_
#define __TOOLS_H_

/* PROTOTYPES FOR MENU CALLBACKS */
void base_converter (void);
void insert_time_string (void);

void to_upper_case (void);
void to_lower_case (void);
void capitalize (void);
void invert_case (void);

void color_picker (void);

void convert_this_to_dos (void);
void convert_this_to_mac (void);
void convert_this_to_unix (void);

#endif							/* __TOOLS_H_ */
