
/*
** (C) 2008 Tobias Heinzen
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "beaver.h"
#include "tools.h"

/** @file tools.c
 * Main File of tools plugin.
 *  
 * This plugin provides different tools 
 * like colorpicker or baseconverter.
 */

/* to save the id's of all created menu entries */
#define num_menues 13
static gint menues[num_menues];

/**
 * plugin initsialisation function
 */
static void init (void)
{
	menues[0] =
		beaver_ui_item_submenu_add (BEAVER_SECTION_MENU_TOOLS, "format",
									"Format");
	menues[1] =
		beaver_ui_item_add (BEAVER_SECTION_MENU_TOOLS, "format_thistodos",
							GTK_STOCK_CONVERT, "format/Convert This to _DOS",
							convert_this_to_dos);
	menues[2] =
		beaver_ui_item_add (BEAVER_SECTION_MENU_TOOLS, "format_thistomac",
							GTK_STOCK_CONVERT, "format/Convert This to _MAC",
							convert_this_to_mac);
	menues[3] =
		beaver_ui_item_add (BEAVER_SECTION_MENU_TOOLS, "format_thistounix",
							GTK_STOCK_CONVERT, "format/Convert This to _UNIX",
							convert_this_to_unix);
	menues[4] = beaver_ui_item_seperator_add (BEAVER_SECTION_MENU_TOOLS);

	menues[5] =
		beaver_ui_item_add (BEAVER_SECTION_MENU_TOOLS, "converter",
							GTK_STOCK_CONVERT, "_Base Converter",
							base_converter);
	menues[6] =
		beaver_ui_item_add (BEAVER_SECTION_MENU_TOOLS, "colorpicker",
							GTK_STOCK_SELECT_COLOR, "_Color Picker",
							color_picker);
	menues[7] =
		beaver_ui_item_add (BEAVER_SECTION_MENU_TOOLS, "inserttime", NULL,
							"Insert _Time", insert_time_string);
	menues[8] = beaver_ui_item_seperator_add (BEAVER_SECTION_MENU_TOOLS);
	menues[9] =
		beaver_ui_item_add (BEAVER_SECTION_MENU_TOOLS, "touppercase", NULL,
							"To _Upper Case", to_upper_case);
	menues[10] =
		beaver_ui_item_add (BEAVER_SECTION_MENU_TOOLS, "tolowercase", NULL,
							"To _Lower Case", to_lower_case);
	menues[11] =
		beaver_ui_item_add (BEAVER_SECTION_MENU_TOOLS, "capitalize", NULL,
							"Ca_pitalize", capitalize);
	menues[12] =
		beaver_ui_item_add (BEAVER_SECTION_MENU_TOOLS, "invertcase", NULL,
							"In_vert Case", invert_case);
}

/**
 * plugin cleanup function
 */
static void cleanup (void)
{
	/* remove all menu items */
	beaver_ui_item_remove (menues[0], "format");
	beaver_ui_item_remove (menues[1], "format_thistodos");
	beaver_ui_item_remove (menues[2], "format_thistomac");
	beaver_ui_item_remove (menues[3], "format_thistounix");
	beaver_ui_item_remove (menues[4], NULL);

	beaver_ui_item_remove (menues[5], "converter");
	beaver_ui_item_remove (menues[6], "colorpicker");
	beaver_ui_item_remove (menues[7], "inserttime");
	beaver_ui_item_remove (menues[8], NULL);
	beaver_ui_item_remove (menues[9], "touppercase");
	beaver_ui_item_remove (menues[10], "tolowercase");
	beaver_ui_item_remove (menues[11], "capitalize");
	beaver_ui_item_remove (menues[12], "invertcase");
}

/* plugin meta information */
PLUGIN_NAME ("Tools")
PLUGIN_DESCRIPTION
("Provides some useful tools (eg. Colorpicker, Baseconverter, etc...)")
PLUGIN_AUTHOR ("Beaver Developers") PLUGIN_VERSION ("1") PLUGIN_INIT (init) PLUGIN_CLEANUP (cleanup)
