

/*
** (C) 2008 Tobias Heinzen
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "beaver.h"

/** @file convert.c
 * Converts different text formats
 */

static inline void conv_unix_to_dos (void)
{
	beaver_text_replace ("\n", "\r\n", FALSE);
	//print_msg ("File converted from UNIX to DOS Format...");
}

static inline void conv_dos_to_unix (void)
{
	beaver_text_replace ("\r\n", "\n", FALSE);
	//print_msg ("File converted from DOS to UNIX Format...");
}

static inline void conv_dos_to_mac (void)
{
	beaver_text_replace ("\r\n", "\r", FALSE);
	//print_msg ("File converted from DOS to MAC Format...");
}

static inline void conv_mac_to_unix (void)
{
	beaver_text_replace ("\r", "\n", FALSE);
	//print_msg ("File converted from MAC to UNIX Format...");
}

static inline void conv_unix_to_mac (void)
{
	beaver_text_replace ("\n", "\r", FALSE);
	//print_msg ("File converted from UNIX to MAC Format...");
}

static inline void conv_mac_to_dos (void)
{
	beaver_text_replace ("\r", "\r\n", FALSE);
	//print_msg ("File converted from MAC to DOS Format...");
}

/**
 * Convert text to dos format
 */
void convert_this_to_dos (void)
{
	switch (beaver_text_format ())
	{
	case BEAVER_FORMAT_UNIX:
		conv_unix_to_dos ();
		break;
	case BEAVER_FORMAT_MAC:
		conv_mac_to_dos ();
		break;
	default:
		break;
	}
}

/**
 * Convert text to mac format
 */
void convert_this_to_mac (void)
{
	switch (beaver_text_format ())
	{
	case BEAVER_FORMAT_UNIX:
		conv_unix_to_mac ();
		break;
	case BEAVER_FORMAT_DOS:
		conv_dos_to_mac ();
		break;
	default:
		break;
	}
}

/**
 * Convert text to unix format
 */
void convert_this_to_unix (void)
{
	switch (beaver_text_format ())
	{
	case BEAVER_FORMAT_MAC:
		conv_mac_to_unix ();
		break;
	case BEAVER_FORMAT_DOS:
		conv_dos_to_unix ();
		break;
	default:
		break;
	}
}
