

/*
** (C) 2008 Tobias Heinzen
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "beaver.h"

/** @file picker.c
 * Display a colorpicker and insert selected color into text
 */

/**
 * Handler for ColorPicker Window. Inserts selected color by the
 * ColorPicker into the text.
 * 
 * @param csd   Selected Color in the ColorPicker Window
 */
static void insert_color (GtkColorSelection * csd)
{
	gdouble Color[3];
	gchar *ColorString;

	/* get the color */
	gtk_color_selection_get_color (csd, Color);

	/* convert the integer values into a hexvalues */
	ColorString =
		g_strdup_printf ("%02x%02x%02x", (guint) (255 * Color[0]),
						 (guint) (255 * Color[1]), (guint) (255 * Color[2]));

	/* insert the color */
	beaver_text_selection_set (ColorString);

	/* free up space */
	g_free (ColorString);

	//print_msg ("Insert Color...");
}

/**
 * Display ColorPicker
 */
void color_picker (void)
{
	/* create new Color Picker */
	GtkColorSelectionDialog *ColorWindow;
	ColorWindow =
		(GtkColorSelectionDialog *)
		gtk_color_selection_dialog_new ("Color Picker");

	/* setting the window modal means, that it stays on top and that the 
	 * other windows can't be used, therefore avoiding having to 
	 * ColorPickers at the same time */
	gtk_window_set_modal (GTK_WINDOW (ColorWindow), TRUE);

	/* connect the window */
	g_signal_connect_swapped (G_OBJECT (ColorWindow), "delete_event",
							  (GtkSignalFunc) gtk_widget_destroy,
							  G_OBJECT (ColorWindow));

	/* rename the "Ok" button to "Insert" and connect it */
	gtk_button_set_label (GTK_BUTTON (ColorWindow->ok_button), "Insert");
	g_signal_connect_swapped (G_OBJECT (ColorWindow->ok_button), "clicked",
							  (GtkSignalFunc) insert_color,
							  G_OBJECT (ColorWindow->colorsel));

	/* connect "Cancel" button */
	g_signal_connect_swapped (G_OBJECT (ColorWindow->cancel_button),
							  "clicked", (GtkSignalFunc) gtk_widget_destroy,
							  G_OBJECT (ColorWindow));

	/* we dont' want the "Help" button */
	gtk_widget_hide (ColorWindow->help_button);

	/* Show the ColorPicker and show a message */
	gtk_widget_show (GTK_WIDGET (ColorWindow));
	//print_msg ("Display Color Picker...");
}
