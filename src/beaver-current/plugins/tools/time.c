

/*
** (C) 2008 Tobias Heinzen
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <time.h>
#include <string.h>
#include "beaver.h"

/** @file time.c
 */

/**
 * Callback Handler.
 * 
 * Insert a string representing the current time into
 * the opened file. The string is formated according to the
 * locale settings.
 */
void insert_time_string (void)
{
	time_t now;
	gchar *buffer;

	/* format time */
	time (&now);
	buffer = ctime (&now);

	/* get rid of a little bug */
	buffer[strlen (buffer) - 1] = '\0';

	/* insert into text */
	beaver_text_insert_string (buffer);
}
