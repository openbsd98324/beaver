

/*
** (C) 2008 Tobias Heinzen
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "beaver.h"

/** @file base.c
 * base converter for tools plugins
 */

/* converter window (singleton) */
static GtkWidget *window = NULL;

/* the text fields in which the user can insert
 * the number to convert */
static GtkWidget *entries[4];

/* the different bases */
#define BASE_BINARY 2
#define BASE_OCTAL  8
#define BASE_HEX    16

/**
 * converts binary to decimal.
 * 
 * All conversion were done by using the horner scheme.
 * 
 * @param   number  the string representation of a binary number
 * @returns the integer value represented by given binary number
 */
static inline guint binary_to_decimal (const gchar * number)
{
	guint dec = 0;

	while (*number)
	{
		if (*number != '1' && *number != '0')
			return 0;
		dec = dec * BASE_BINARY + (*number) - '0';
		number++;
	}

	return dec;
}

/**
 * converts octal to decimal.
 * 
 * All conversion were done by using the horner scheme.
 * 
 * @param   number  the string representation of an octal number
 * @returns the integer value represented by given octal number
 */
static inline guint octal_to_decimal (const gchar * number)
{
	guint dec = 0;

	while (*number)
	{
		if ((*number) - '0' > 7)
			return 0;
		dec = dec * BASE_OCTAL + (*number) - '0';
		number++;
	}

	return dec;
}

/**
 * converts hexadecimal to decimal.
 * 
 * All conversion were done by using the horner scheme.
 * 
 * @param   number  the string representation of a hexadecimal number
 * @returns the integer value represented by given hexadecimal number
 */
static inline guint hexadecimal_to_decimal (const gchar * number)
{
	guint dec = 0;

	while (*number)
	{

		if ((((*number) - '0') >= 0 && ((*number) - '0') <= 9))
		{
			dec = dec * BASE_HEX + (*number) - '0';
		}
		else if ((((*number) - 'a') >= 0 && ((*number) - 'a') <= 5))
		{
			dec = dec * BASE_HEX + (*number) - 'a' + 10;
		}

		number++;
	}

	return dec;
}

/**
 * converts string to decimal.
 * 
 * All conversion were done by using the horner scheme.
 * 
 * @param   number  the string representation of a decimal number
 * @returns the integer value represented by given decimal number
 */
static inline guint string_to_decimal (const gchar * number)
{
	guint dec = 0;

	while (*number)
	{

		if ((((*number) - '0') >= 0 && ((*number) - '0') <= 9))
		{

			/* check if we overflow when we do the action */
			if (((dec * 10) / 10) + (*number) - '0' == dec + (*number) - '0')
			{
				dec = dec * 10 + (*number) - '0';
			}
			else
			{
				return G_MAXUINT32;
			}

		}

		number++;
	}

	return dec;
}

/**
 * Converts a user given into different bases.
 */
static void signal_clicked (GtkButton * button, gpointer user_data)
{
	gint id = GPOINTER_TO_INT (user_data);
	guint decimal = 0;			/* everything is unsigned */
	gchar *text = NULL;

	/* first of all we have to compute the decimal value, 
	 * from there on, everything else is pretty easy to 
	 * compute (just do an printf). so we check which case 
	 * we got and convert into decimal */
	switch (id)
	{
	case 0:	/* string to decimal */
		decimal =
			string_to_decimal (gtk_entry_get_text (GTK_ENTRY (entries[0])));
		break;
	case 1:	/* binary to decimal */
		decimal =
			binary_to_decimal (gtk_entry_get_text (GTK_ENTRY (entries[1])));
		break;
	case 2:	/* octal to decimal */
		decimal =
			octal_to_decimal (gtk_entry_get_text (GTK_ENTRY (entries[2])));
		break;
	case 3:	/* hexadecimal to decimal */
		decimal =
			hexadecimal_to_decimal (gtk_entry_get_text
									(GTK_ENTRY (entries[3])));
		break;
	}

	/* set all text fields with their right values */
	text = g_strdup_printf ("%u", decimal);
	gtk_entry_set_text (GTK_ENTRY (entries[0]), text);
	g_free (text);

	text = g_strdup_printf ("%o", decimal);
	gtk_entry_set_text (GTK_ENTRY (entries[2]), text);
	g_free (text);

	text = g_strdup_printf ("%x", decimal);
	gtk_entry_set_text (GTK_ENTRY (entries[3]), text);
	g_free (text);

	gtk_entry_set_text (GTK_ENTRY (entries[1]), "");
	while (decimal)
	{
		text = g_strdup_printf ("%d", decimal & 0x1);
		gtk_entry_prepend_text (GTK_ENTRY (entries[1]), text);
		g_free (text);
		decimal = decimal / 2;
	}
}

/**
 * Callback Handler.
 * 
 * Creates a Window with 4 boxes to convert between
 * Hexadecimal, Decimal, Octal and Binary.
 */
void base_converter (void)
{
	/* if the converter window was not created yet,
	 * create it as singleton */
	if (!window)
	{
		GtkWidget *table;
		GtkWidget *label;
		GtkWidget *button;

		/* create new dialog */
		window = gtk_dialog_new ();
		gtk_window_set_title (GTK_WINDOW (window), "Base Converter");
		gtk_window_set_modal (GTK_WINDOW (window), TRUE);
		gtk_window_set_policy (GTK_WINDOW (window), FALSE, FALSE, FALSE);	/* not resizable */

		/* creating a 4x3 table. the first column
		 * contains labels, the second text boxes and
		 * the third buttons. the 4 rows are for the 
		 * different bases: hexadecimal, decimal, binary
		 * and octal */
		table = gtk_table_new (4, 3, FALSE);
		gtk_table_set_row_spacings (GTK_TABLE (table), 5);
		gtk_table_set_col_spacings (GTK_TABLE (table), 5);
		gtk_container_set_border_width (GTK_CONTAINER (table), 10);

		/* but the table into the upper vertical box of
		 * the dialog */
		gtk_box_pack_start (GTK_BOX (GTK_DIALOG (window)->vbox), table, FALSE,
							FALSE, 0);

		/* create labels and put them into first column
		 * of the table */
		label = gtk_label_new ("Decimal :");
		gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);
		gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, 0, 1);

		label = gtk_label_new ("Binary :");
		gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);
		gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, 1, 2);

		label = gtk_label_new ("Octal :");
		gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);
		gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, 2, 3);

		label = gtk_label_new ("Hexa :");
		gtk_misc_set_alignment (GTK_MISC (label), 0, 0.5);
		gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, 3, 4);

		/* create the text fields and put them into second column
		 * of the table */
		entries[0] = gtk_entry_new ();
		gtk_entry_set_max_length (GTK_ENTRY (entries[0]), 10);
		gtk_table_attach_defaults (GTK_TABLE (table), entries[0], 1, 2, 0, 1);

		entries[1] = gtk_entry_new ();
		gtk_entry_set_max_length (GTK_ENTRY (entries[1]), 32);
		gtk_table_attach_defaults (GTK_TABLE (table), entries[1], 1, 2, 1, 2);

		entries[2] = gtk_entry_new ();
		gtk_entry_set_max_length (GTK_ENTRY (entries[2]), 11);
		gtk_table_attach_defaults (GTK_TABLE (table), entries[2], 1, 2, 2, 3);

		entries[3] = gtk_entry_new ();
		gtk_entry_set_max_length (GTK_ENTRY (entries[3]), 8);
		gtk_table_attach_defaults (GTK_TABLE (table), entries[3], 1, 2, 3, 4);

		/* create the buttons and put them into third column
		 * of the table */
		gint i;
		for (i = 0; i < 4; i++)
		{
			button = gtk_button_new_from_stock (GTK_STOCK_CONVERT);
			gtk_table_attach_defaults (GTK_TABLE (table), button, 2, 3, i,
									   i + 1);
			g_signal_connect (G_OBJECT (button), "clicked",
							  G_CALLBACK (signal_clicked), GINT_TO_POINTER (i));
		}

		/* create a close button and add it in the bottom vertical
		 * box of the dialog */
		button =
			gtk_dialog_add_button (GTK_DIALOG (window), GTK_STOCK_CLOSE, 1);
		gtk_button_set_use_stock (GTK_BUTTON (button), TRUE);

		/* connect some signals */
		g_signal_connect (G_OBJECT (button), "clicked",
						  GTK_SIGNAL_FUNC (gtk_widget_hide), window);
		g_signal_connect (G_OBJECT (window), "delete-event",
						  GTK_SIGNAL_FUNC (gtk_widget_hide_on_delete), window);
		g_signal_connect (G_OBJECT (window), "close",
						  GTK_SIGNAL_FUNC (gtk_widget_hide), window);
		g_signal_connect (G_OBJECT (window), "response",
						  GTK_SIGNAL_FUNC (gtk_widget_hide), window);
	}

	gtk_widget_show_all (window);
}
