

/*
** (C) 2008 Tobias Heinzen
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "beaver.h"

/** @file case.c
 * Case manipulation of selected text.
 */

/**
 * Set all letters in selected text to uppercase 
 */
void to_upper_case (void)
{
	gchar *selection = beaver_text_selection_get ();
	if (selection)
	{
		gchar *upper = g_utf8_strup (selection, -1);

		beaver_text_selection_set (upper);

		g_free (selection);
		g_free (upper);
	}
}

/**
 * Set all letters in selected text to lowercase 
 */
void to_lower_case (void)
{
	gchar *selection = beaver_text_selection_get ();
	if (selection)
	{
		gchar *lower = g_utf8_strdown (selection, -1);

		beaver_text_selection_set (lower);

		g_free (selection);
		g_free (lower);
	}
}

/**
 * Convert the first letter in the selected text to upper case
 */
void capitalize (void)
{
	gchar *selection = beaver_text_selection_get ();
	if (selection)
	{
		selection[0] = g_unichar_toupper (selection[0]);

		beaver_text_selection_set (selection);

		g_free (selection);
	}
}

/**
 * Inverts the case of the letters in the selected text
 */
void invert_case (void)
{
	gchar *selection = beaver_text_selection_get ();
	if (selection)
	{
		gchar *iter = selection;

		while (*iter)
		{
			if (g_unichar_islower (*iter))
			{
				*iter = g_unichar_toupper (*iter);
			}
			else
			{
				*iter = g_unichar_tolower (*iter);
			}

			iter++;
		}

		beaver_text_selection_set (selection);

		g_free (selection);
	}
}
